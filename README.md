# Rulebox

## About
Rulebox is a simple CLI tool to monitor, measure and improve data quality via a constraint based approach.
It's main features are:

* Assessment and improvement of data residing in either csv files or a Postgres database.

* A simple text-based format to encode several types of constraints like selection rules, functional dependencies, inclusion dependencies and regular expressions.

## Usage
Rulebox requires [Java 17](https://java.com/en/download/help/download_options.html) or higher to run.
The latest version can be found in the Package folder of the ledc group.
Once downloaded, use `java -jar rulebox.jar` on the console to see all available commands.
For more detailed information, have a look at the [documentation](docs).
