# Management of data binders

Rulebox uses data binders to interact with data sources.
A data binder is basically a description of where data can be found.
Each data binder is assigned a unique name.
All binder information is collected in the file `binder-pool.json`.

## Binder types

Currently, there are two types of binders:

- **csv binders** A binder to data that resides in a csv file.

- **jdbd binders** A binder to data that resides in a relational database. Currently, only PostgreSQL is supported.

## Commands for manipulation

Strictly speaking it is possible to manipulate the file `binder-pool.json` but this is **not** recommended as a simple typo might make the file corrupt.
Rulebox offers commands to manipulate binders.

```console
binder add csv
binder add jdbc
binder delete
binder list
binder temporal
```
The command `binder temporal` allows to declare a binder as "temporal", meaning that it binds to a temporal dataset.
It allows to store additional information about the timestamp attribute in the binder configuration.
This is necessary as some commands require input binders to be temporal.
When adding binders the requested parameters depend on the type of binding.
Below, some more information on creation of binders is given per type.

### Creating csv binders

The command `binder add csv` creates a binding to a dataset that resides in a csv file.
The complete list of parameters and flags in shown below.
```console
csv: Adds a CSV-based binder to the binder pool.

Parameters:
name            --n     (required) A unique name for the binder.
encoding        --e     (optional) The encoding of the connection. Default value is UTF-8
file            --fi    (required) The path to the csv file.
comment-symbol  --cs    (optional) The comment symbol to be used. Lines starting with this symbol are considered as comment.
null-symbols    --ns    (optional) Symbols that represent null-values (comma-separated list). A default null symbol is registered in the config file.
quote-symbol    --qs    (optional) The quote symbol to be used. By default, no quote symbols are used.
separator       --s     (required) The separator symbol to be used.

Flags:
force           --f     Overwrite the binder if it already exists.
no-header       --nh    Use this flag if the first line in the csv file is not a header line.
type-inference  --ti    Use this flag if you want datatypes to be recognized in the data.
```
Some points of attention are the following:

* **Type inference** When reading data from a csv file, the default datatype of an attribute is a [string](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md#contractors).
If the `type-inference` flag is actived, rulebox will try to assign datatypes to attributes.
To that extent, a sample of the data is inspected.
The size of this sample can be modified in the [config](config.md) file.

* **Null symbols** In csv files, there is no standard way of encoding null values (i.e., missing data). It is possible to pass a (comma-separated) list of strings that represent a null value in your csv file.
When not specified, the default string that encodes a null value will be used.
This default can be modified in the [config](config.md) file.

### Creating jdbc binders

The command `binder add jdbc` creates a binding to data that resides in a relational database.
The complete list of parameters and flags in shown below.
```console
jdbc: Adds a JDBC-based binder to the binder pool.

Parameters:
database        --db    (required) The name of the database to connect with.
host            --h     (required) The name of the host where the database is located.
name            --n     (required) A unique name for the binder.
user            --u     (required) The name of the user that will connect to the database.
encoding        --e     (optional) The encoding of the connection. Default value is UTF-8
password        --pw    (optional) The password of the user that will connect to the database. When specified, the password is stored in the binder pool file. If not given, the password will be requested at connection time.
port            --p     (optional) The port number to which the database can be reached. Default value is 5432.
schema          --s     (optional) The name of the schema to connect with. Default value is public
sql-statement   --sql   (optional) The SQL statement that will be used to load data. Use this if the data to work with is fixed.
table           --t     (optional) When specified, data will be loaded via a 'select * from <table>' statement. This parameter is ignored when sql-statement is specified.
vendor          --v     (optional) The name of the DBMS. Default value is POSTGRESQL

Flags:
force           --f     Overwrite the binder if it already exists.
all-tables      --at    When activated, a binder entry is added for every table in the specified schema. The names of these binders are a concatenation of the given binder name the name of the table. This parameter is ignored when either sql-statement or table are specified.
```

Some points of attention are the following:

* The parameter `password` is optional.
When provided, it will be stored in the file `binder-pool.json` in plain text.
If this is unacceptable, you can choose not to specifcy the password.
In that case, the password will be prompted for each time a connection to the database must be made.

* It is possible to provide a *fixed SQL statement* for a jdbc binder by use of parameter `sql-statement`.
If this is the case, the output of the SQL query is considered as the dataset to work with.

* If the dataset is a table in the database, you can specify by using the parameter `table`.

* If you want to create a binder for each table in a database schema, you can use the `all-tables` flag.
The binder names are then the given name, concatenated with the name of the table.

* When specifying the data, the priority of evaluation is `sql-statement`, `table`, `all-tables`.
Rulebox will only account for the parameter/flag with highest priority that is specified.
Other parameters/flags are **ignored**.
If none of these parameters is specified, the binder binds to the entire schema.
This can be useful to draw diagrams with `explore schema`.
Note that in this case, a SQL statement will be promted for each time data must be read from the database.

* When using rulebox on data stored in a relational database, it is advisable that the database administrator provides a user with limited access.
Advisable is a user with select access on the database of interest and the information schema if schemas are desirable.
When repairing data, it is advised to never execute it immediately on the dirty data.
Write repairs to a csv file or a separate table for inspection (e.g. by using `repair analyse`).
