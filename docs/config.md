# Config file

The behavior of rulebox can be tweaked via simple text-based config file that is named `ledc.config` and must be placed in the same folder as the .jar file.
Below, the parameters that can be set in the current version of rulebox are shown together with their default values.

```config
#The size of the sample to conduct type inference for CSV-based data
TYPE_INFERENCE_SAMPLE_SIZE = 100000

#Name of the local registry folder
LOCAL_REGISTRY = registry

#Name of the file in which binder data is collected
BINDER_POOL = binder-pool.json

#Default representation of NULL
NULL_SYMBOL = 

#Usage of short names for cost functions in the .rbx file
COST_FUNCTION_SHORT_NAMES = true
```
