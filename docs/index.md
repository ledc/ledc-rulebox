# Working with rulebox

Rulebox is a simple CLI-tool to get insights in the data quality of datasets.
To use rulebox, you should:

* make sure Java (version 8 or higher) is installed.
* download the latest build in the [package registry](https://gitlab.com/ledc/ledc-rulebox/-/packages)

Once downloaded, open a shell, go to the folder where you've downloaded the executable jar file and run `java -jar rulebox.jar` to get an overview of the commands.

To run a command, pass the command name as an argument when you run the .jar file.
For example, `java -jar rulebox.jar repair parker` calls the command `repair parker`.
If any required parameters are missing, a message will be printed to notify what is missing.
In any case, you can always add the `--help` flag after the command name to see the options for that command.

## Further topics

To further help you in using rulebox, some guidelines and hints on usage of rulebox are provided below.
Please select your topic of interest.

* [Basic usage](parameters.md)
* [Management of data binders](binding.md)
* [Management of constraints](rbx.md)
* [Configuration](config.md)
* [Report generation](reporting.md)
