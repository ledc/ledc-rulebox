# Using rulebox

### Commands

Rulebox offers a set of commands that are organized *hierarchically* in a tree structure.
The leaf nodes of this structure are executable commands that perform some operation while the intermediary nodes of the structure simply group these executable commands.

When rulebox is called with an **non executable command** (e.g., `java -jar rulebox.jar explore`), rulebox will report all the child commands that are available for that command.

When rulebox is called with an **executable command** (e.g., `java -jar rulebox.jar explore schema`) the command will be executed.
For some commands, it might be necessary to first specify some required [parameters](#parameters) that offer necessary input for the command.
Information about what a command does and how parameters need to be interpreted can be obtained by putting `--help` after an executable command name.
For example, running `java -jar rulebox.jar explore schema --help` will yield the following output:

```console
schema: draws a diagram of the schema of a JDBC data source using mermaid.

Parameters:
data-binder             --d     (required) Name of the data binder. Use 'binder list' for an overview.
output-file             --of    (required) File to which output is written.
name-filter             --nf    (optional) Only tables with a name that matches this regex pattern will be shown in the diagram

Flags:
check-constraints       --cc    Adds information about check constraints to the diagram
invert-filter           --if    Inverts the selection of tables made by the name-filter parameter
row-count               --rc    Show table sizes (row counts) for each table
exclude-views           --ev    Exclude views from the diagram
```
This output first specifies the purpose of the command (i.e., making a diagram) and then prints a list of parameters and flags for which an explanation of usage is shown.
 
### Parameters

The behaviour of executable commands (in the following just called 'commands') can be steered by using *parameters*.
A parameter has a full name (e.g., `data-binder`) a short name or alias (e.g., `--d`).
In order to specify a parameter, type either the full or short name of the parameter followed by its value.

Some parameters are attached to a series of **tests** (e.g., for an output file there might be a check on the file extension).
If any of those tests fails on the given value, execution of the command stops and an error is reported.

Parameters are either **required** or **optional**.
This is indicated in the output of `--help` (see example above).
For required parameters, a value must be specified when you call the command.
If this is not the case, execution will halt and an error message will be printed.

In most cases, a parameter name can be specified only once.
However, there are some cases where parameters can be repeated multiple times.
In that case, a *set* of parameter values can be passed to rulebox for being further processed.
If a parameter accepts multiple values by repetition, this is indicated in the output of `--help`.

### Flags

Flags are special parameters that are always optional and that are Boolean valued.
Therefore, flags do not require a value to be passed: if you pass the flag name, you pass a value `true`.
Else, you implicitly pass value `false`.
