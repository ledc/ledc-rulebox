# Management of constraints

The central tool in rulebox to perform data quality checks, is that of a **constraint**.
Constraints are collected and described in a special file called the **rubix** file with extension `.rbx`.
A rubix file is basically a text-based file in which different types of constraints can be declared in so-called **sections**.
Moreover, some sections can be used to provide meta-information.
The table below provides an overview of the different sections.

| **Section name**                    | **Description**                                              | **Section type** |
|-------------------------------------|--------------------------------------------------------------|------------------|
| [#datatypes](#contractors)            | Specification of contractors for attributes                  | Meta             |
| [#selection-rules](#selection-rules)| Definition of selection rules that must be satisfied         | Constraint       |
| [#functional-dependencies](#functional-dependencies) | Definition of functional dependencies that must be satisfied |Constraint |
| [#inclusion-dependencies](#inclusion-dependencies)  | Definition of inclusion dependencies that must be satisfied  | Constraint |
| [#property-assertions](#assertions)     | Definition of property assertions | Constraint       |
| [#linkage-rules](#linkage-rules)     | Definition of linkage rules | Constraint       |
| [#repair-cost-models](#cost-models)      | Cost models used in repairing of selection rules             | Repair             |

# Contractors

To support the correct usage of selection rules and inclusion dependencies, it is possible to specify [contractors](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md#contractors) for the different columns in your dataset.
In simple words, a contractor is the combination of a datatype with a number of standard operations on that datatype.
Examples of operations are the next value and the previous value.
For some datatypes (e.g., integer numbers) these operations are trivial.
For other datatypes, it might be useful (e.g. timestamps) or even necessary (e.g., real numbers) to specify these operations.
This often boils down to the specification of a **unit** or **granularity** for the datatype.
For example, the contractor `datetime_in_minutes` implies that the datatype is a timestamp (datetime) and the elementary unit is a minute.
Values are then truncated to this unit and operations like 'next' or 'previous' are defined in terms of this unit.


**Format** In the section `#datatypes` each line is of the format `@<attribute>:<contractor>`.
That is, each line start with the name of an attribute prefixed with `@`, followed by a colon `:` and finally the name of a valid contractor.
A full overview of the available contractors can be found [here](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md#contractors).

**Relevant commands** Contractors are necessary when using [selection rules](#selection-rules) and can be useful when using [inclusion dependencies](#inclusion-dependencies).

# Selection rules

A selection rule or a [sigma rule](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md) is basically a statement that describes when a row in your dataset is invalid.
It thus describes cases in which a row contains an error.
Note that in order to correctly interpret a selection rule, each attribute that appears in the definition of a selection rule must occur in the `#datatypes` section too.

**Format** In the section `#selection-rules` you can basically encode selection rules by using the file format as explained and exemplified [here](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md#file-format), with the exception that attribute contracts are listed separately in the `#datatypes` section.

**Relevant commands** If your dataset features some selection rules that must be satisfied, here are some relevant commands in rulebox:

```console
reason fcf

detect

repair selection condsequential
repair selection joint
repair selection sequential
repair parker

explore rules association
explore rules lift
explore rules ordinal
```
# Inclusion dependencies

*To be completed.*

# Functional dependencies

**Format** In the section `#functional-dependencies` a set of functional dependencies (FDs) can be encoded.
Functional dependencies can be communicated in their usual notation `LHS -> RHS` where both `LHS`(left-hand side) and `RHS` (right-hand side) are comma-separated lists of attribute names.

**Relevant commands** If FDs are relevant for the dataset you're working with, the following commands in rulebox are relevant:

```console
detect

explore rules tane

reason mincover

reason normalisation 3nf
reason normalisation bcnf
reason normalisation candkeys
reason normalisation v2nf
reason normalisation v3nf
reason normalisation vbcnf

repair parker
repair swipe
```

# Assertions

**Format** In the section `#property-assertions`, statements can be made that link individual attributes to standardized properties by using the syntax described [here](https://gitlab.com/ledc/ledc-pi/-/blob/master/docs/assertions.md#assertions).
Standardized properties come with several utilities such as quality measures, convertors and scanners.
They are a tool to verify and improve the *representational consistency* of data.

**Relevant commands** If assertions are relevant for the dataset you're working with, the following commands in rulebox are relevant:

```console
server fetch
server list

property grex
property init
property instance

detect

transform
repair assertions
```
To use assertions, properties must be available.
All information about such properties is collected in a local directory called `registry`.
To feed this local directory, there are two options.

* The `property` command helps in constructing a local file in which knowledge about a property is collected.

* The `server` command allows to obtain pre-configured files from a ledc server.
The `server list` command provides information about properties and their components that resides on a ledc server.
A public server is running on `ledc.ugent.be:8828`.
The `server fetch` command can be used download all necessary properties that appear in your .rbx file to the local registry.

# Linkage rules

In the section `#linkage-rules`, a linkage rule can be specified that allows to deduplicate a dataset.
A linkage rule is basically composed of two things: a set of attribute matchers and a belief model that links observed matches to a belief that two tuples are duplicates.
Rulebox basically allows two types of linkage rules: (i) a **Fellegi-Sunter** linkage rule and (ii) a **minimax** linkage rule.
More details on these types of rules are given in separate sections below.

**Format** The general structure of passing on a linkage rule is to first provide a mapping of attributes to matchers.
This is done by passing, one separate lines, information of the format `@<attribute>:<matcher>` where `<matcher>` describes the desired matcher.
After passing the matchers, the belief structure is described.
The format of this depends on the type of linkage rule.
For more info, see the sections below.
Note however, that in a single .rbx file, **at most one** linkage rule can be described.

**Relevant commands** The following commands are relevant:

```console
dedupe

explore rules linkage optimal
explore rules linkage em
```

The later two commands allow to train prior and partial match weights of a Fellegi Sunter model (under the Naive Bayes assumption) in a supervised or unsupervised way.
The unsupervised learning uses an EM algorithm to estimate weights.

### Specifying attribute matchers
Currently, the following matchers can be used:

* **EqualsMatcher.** Returns `1` if two values are equal and `0` otherwise.

* **StringEqualsMatcher.** Returns `1` if two strings are equal and `0` otherwise.

* **SubstringMatcher.** Returns `1` if one string is a substring of the other and `0` otherwise.

* **RatioDistanceMatcher** For two strings `s` and `t`, returns the ratio `(m-d)/m` where `m:=max(|s|,|t|)` and `d` is the edit distance between `s` and `t`.

* **SoundexMatcher** Returns `1` if the soundex code of two strings is equal and `0` otherwise.

* **JaccardMatcher** Returns the Jaccard index of the `n`-gram sets computed from two strings.

* **HybridMatcher** Computes a similarity by hybrid matching. Two strings are first tokenized and tokens are mutually compared with an internal character-based similarity. 

The behavior of the matchers can be controlled with the following options:

* **transform:** a [grex](https://gitlab.com/ledc/ledc-pi/-/blob/master/docs/grexes.md?ref_type=heads) expression that indicates how strings are transformed prior to comparison.
* **splitter:** for token-based methods, this option informs how to split strings. Available options: `wordsplitter` or `ngramsplitter (n=.)` where the dot can be filled in with a natural number larger than zero.
* **metric:** an edit distance. Available options: `Levenshtein` and `Damerau`.
* **equality pattern:** For hybrid matching, this option specifies a regex to identify tokens that must be compared on pure equality.
* **strict:** For hybrid matching, a boolean that indicate strict matching. When set to `true`, all tokens in both strings must have a matching counterpart. When set to `false` this requirement is loosened and comparison basically boils down to subset inclusion of tokens.

The following table shows which options are available for which matcher.

|                      | transform: | metric: | splitter: | equality pattern: | strict: |
|----------------------|:------------:|:---------:|:-----------:|:-------------------:|:---------:|
| EqualsMatcher        |            |         |           |                   |         |
| StringEqualsMatcher  |      X     |         |           |                   |         |
| SubstringMatcher     |      X     |         |           |                   |         |
| RatioDistanceMatcher |      X     |    X    |           |                   |         |
| SoundexMatcher       |      X     |         |           |                   |         |
| JaccardMatcher       |      X     |         |     X     |                   |         |
| HybridMatcher        |      X     |         |           |         X         |    X    |


### Fellegi-Sunter linkage

With the Fellegi-Sunter linkage rule, the outcome of comparing two attributes is restricted to four cases:

- **Equal** (E): two attributes values are not null and equal.
- **Similar** (S): two attributes values are not null, not equal but their similarity lies above a threshold.
- **Unknown** (U): at least one of two attributes is null.
- **Different** (D): any other case.

To compute similarity, the attribute matcher of each attribute must be accompanied with a cutoff to decide when similarity is high enough.
This means that each attribute matcher specified, must be a CutoffMatcher that follows the following general pattern:

```console
@attribute: CutoffMatcher with embedded <matcher> with cutoff <threshold>
```
Here, `<matcher>` can be [any attribute matcher](#specifying-attribute-matchers) that compares attributes and produces a similarity score in the unit interval and threshold is a [0,1]-valued cutoff.
When two attributes are not equal and not null, the similarity of the values will computed and compared to given threshold.
If the similarity is higher than or equal to the threshold, the outcome of comparing the two attribute values is `S`.

The belief model of the Fellegi-Sunter linkage rule, consists of two things: a **prior** weight and a **partial match** weight for each attribute and each comparison outcome.
These weights are typically not hand-crafted, but learned from data.

### Minimax linkage

With the Minimax linkage rule, conditional belief for matching attributes is encoded by means of a **capacity** function and the actual computation of the linkage score is done with a Sugeno integral.

The capacity function can be specified (partially in most cases) by providing *capacity rules*.
Each capacity rule is written on a separate line and is of the form `<attribute1>,...,<attributeN>: belief` where `belief` is a real number in the unit interval that represents the capacity of the given set of attributes.
This capacity represents the belief we assign to two tuples being a match whenever all attributes in the set are matching.

The whole of capacity rules must adhere to the definition of a capacity.
In particular, this means that:

* the empty set has capacity `0`,
* the set of all matching attributes has capacity `1`,
* capacity is monotonic with respect to the subset-relation.

If the capacity function is specified only partially (i.e., by given the capacity of just a few sets), the capacity function is completed by assigning the smallest possible capacities that still satisfy monotonicity.

For two tuples `t` and `t`, the linkage score is computed by:

$`\max_{B\subseteq A} \min\left(C(B), \min_{b\in B} \operatorname{sim}\left(t[b],t'[b]\right)\right)`$

where `A` is the set of matching attributes, `C` is the capacity function and `sim` is the designated similarity function for attribute `b`.

The main difference with the Fellegi-Sunter model is that usage of minimax rules do not require an a priori choice of a cutoff value on the indivual matches.

# Cost models

When repairing selection rules, the search for repairs is based on an assignment of *cost* to each modification.
Cost models allow to indicate how this cost must be computed for each attribute.
In the section `#repair-cost-models`, it is possible to add lines of the format `@<attribute>:<cost-model>`.
The table below provides an overview of the available cost models.

| **Cost model**                    | **Description**                                           | **Requirements** | **Parameters**|
|-------------------------------------|--------------------------------------------------------------|:------------------:|:-------:|
|constant (default) | The cost is a constant integer number (default 1) for each attribute | None | Fixed cost |
|distance| The cost of a change is proportionate to the *magnitude* of the change| Ordinal data only | None |
|error|The cost of a change is lower if the change can be explained by means of an error mechanism| None | Error mechanisms to account for and default cost|
|preference|The cost is induced by a preference order among values|None| (Partial) preference order |
|levenstein|The cost is equal to the Levenstein distance|String-data only|None|
|damerau|The cost is equal to the Damerau distance|String-data only|None|

Below, some more details about cost models and their syntax is given.

- **constant** With a constant cost model, the cost to make a change to attribute `a` is a fixed integer number.
In particular, this means the cost does not depend on the values involved in the change.
To encode a specific cost, put the cost between parenthesis after the cost model name.

- **distance** With a distance cost model, the cost of a change is not constant, but depends on the magnitude of the change.
That is, the difference between the original and repaired values determines the size of the cost.
Difference is hereby computed as the number of units between two values, as dictated by the [contractor](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md#contractors).

- **error** With an error cost model, the cost of a change equals 1 if the change can be explained by one of a set of given [error mechanisms](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/repair.md#non-constant-cost-functions).
Else, the cost is equal to some fixed value, higher than one.
To define an error cost model, use the syntax: `error (<default-cost>, [error-mechanism-name|error-mechanism-shortname, ...])`.
A list of names and shortnames of existing error mechanisms can be found [here](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/repair.md#non-constant-cost-functions).

- **preference** With a preference cost model, you specify a preference order on values from the domain of an attribute.
This can be done partially, which means that not all possible values must be listed.
Those not specified, are assumed equally non-preferable and are put last.
To define a preference cost model, list constant values in a comma-separated list in order of preference.
Preference cost models are especially useful to encode risk-avoiding strategies.

- **levenstein** For text data only, the cost is computed as the Levenstein distance between the original value and the new value.

- **damerau** For text data only, the cost is computed as the Damerau distance between the original value and the new value.
An example of a cost model section is shown below.
```txt
#repair-cost-models
@city:levenstein
@zip:constant(2)
@age:distance
@ssn: error (3, [SingleDigitError, PhoneticError])
```
**Compatibility note** As of version 1.3, the Swipe repair algorithm is modified to also work with a cost models.
Choice functions can thus no longer be specified, but they have a cost-function counter part (e.g. voting becomes constant cost function).

