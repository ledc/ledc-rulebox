# Reporting

In order to provide appealing output, rulebox extensively relies on HTML documents.
Below are commands that can show output in HTML with a brief indication of what the report does.

```console
#Generates a report on violations of constraints in a dataset
detect

#Generates a report on basic characteristics of a dataset
explore analyse

#Generates a report with outliers found in a dataset
explore outliers [...]

#Generates a Mermaid ER diagram of a database
explore schema

#Generates a report on the differences between dirty data and repaired data
repair analyse
```
The output varies from command to command, but in most cases, rulebox creates an XML file which is then processed by means of an XSLT transformation that is embedded in the .jar file of rulebox.
Note: it is possible to write the raw data to an XML file and transform it with your own XSLT file.

An example report, created with the command `explore analyse` on a selection of attributes from the [adult](https://archive.ics.uci.edu/ml/datasets/adult) dataset can be found [here](https://antoonbronselaer.gitlab.io/ledc-use-cases/examples/adult-stats.html).
