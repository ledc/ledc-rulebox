package be.ugent.ledc.rulebox;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.pi.registry.Registry;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;

public class Config
{
    //Configurable int constants
    public static int TYPE_INFERENCE_SAMPLE_SIZE = 100000;
    
    //Configurable string constants
    public static String LOCAL_REGISTRY = "registry";
    public static String BINDER_POOL = "binder-pool.json";
    public static String NULL_SYMBOL = "";
    
    //Configurable boolean constants
    public static boolean COST_FUNCTION_SHORT_NAMES = true;
    
    //Non configurable constants
    public static final String CONTACT_EMAIL = "ledc@ugent.be";
    public static final String DOCUMENTATION = "https://gitlab.com/ledc/ledc-rulebox/-/tree/main/docs?ref_type=heads";
    public static final String COMMENT = "#";
    
    public static void readConfig() throws FileNotFoundException, IOException
    {    
        File cfgFile = new File("ledc.cfg");
        
        try(InputStream in = new FileInputStream(cfgFile))
        {
            Properties props = new Properties();
            
            props.load(in);
            
            for(String name : props.stringPropertyNames())
            {
                switch(name)
                {
                    case "TYPE_INFERENCE_SAMPLE_SIZE":
                        TYPE_INFERENCE_SAMPLE_SIZE = Integer
                            .parseInt(props.getProperty("TYPE_INFERENCE_SAMPLE_SIZE").trim());
                        break;
                    case "NULL_SYMBOL":
                        NULL_SYMBOL = props.getProperty("NULL_SYMBOL");
                        break;
                    case "LOCAL_REGISTRY":
                        LOCAL_REGISTRY = props.getProperty("LOCAL_REGISTRY");
                        break;
                    case "COST_FUNCTION_SHORT_NAMES":
                        COST_FUNCTION_SHORT_NAMES = Boolean
                            .parseBoolean(props.getProperty("COST_FUNCTION_SHORT_NAMES").trim());
                        break;
                }
            }
        }

    
    }

    public static void loadRegistry() throws FileNotFoundException, LedcException, URISyntaxException
    {
        File[] jsonFiles = new File(Config.LOCAL_REGISTRY)
            .listFiles((f) -> f.getName().endsWith(".json"));
        
        for(File jsonFile: jsonFiles)
        {
            Registry.getInstance().restore(jsonFile);
        }
    }
}
