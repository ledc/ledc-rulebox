package be.ugent.ledc.rulebox;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.fundy.algorithms.normalization.NormalForm;
import be.ugent.ledc.rulebox.handler.DocHandler;
import be.ugent.ledc.rulebox.handler.Router;
import be.ugent.ledc.rulebox.handler.Handler;
import be.ugent.ledc.rulebox.handler.RootHandler;
import be.ugent.ledc.rulebox.handler.binder.BinderListHandler;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.handler.binder.BinderRemoveHandler;
import be.ugent.ledc.rulebox.handler.binder.BinderTemporalHandler;
import be.ugent.ledc.rulebox.handler.binder.CsvAddHandler;
import be.ugent.ledc.rulebox.handler.binder.JdbcAddHandler;
import be.ugent.ledc.rulebox.handler.detection.DedupeHandler;
import be.ugent.ledc.rulebox.handler.explore.DiagramHandler;
import be.ugent.ledc.rulebox.handler.explore.outliers.DBScanHandler;
import be.ugent.ledc.rulebox.handler.explore.outliers.HBOSHandler;
import be.ugent.ledc.rulebox.handler.explore.outliers.IsolationForestHandler;
import be.ugent.ledc.rulebox.handler.explore.outliers.NearestNeighbourHandler;
import be.ugent.ledc.rulebox.handler.explore.outliers.SodaHandler;
import be.ugent.ledc.rulebox.handler.explore.outliers.ZScoreHandler;
import be.ugent.ledc.rulebox.handler.explore.rules.AssociationHandler;
import be.ugent.ledc.rulebox.handler.explore.rules.LiftMiningHandler;
import be.ugent.ledc.rulebox.handler.explore.rules.OrdinalMiningHandler;
import be.ugent.ledc.rulebox.handler.explore.rules.TaneHandler;
import be.ugent.ledc.rulebox.handler.explore.stats.AnalysisHandler;
import be.ugent.ledc.rulebox.handler.detection.DetectionHandler;
import be.ugent.ledc.rulebox.handler.explore.dedupe.ExpMaxFSHandler;
import be.ugent.ledc.rulebox.handler.explore.dedupe.OptimalFSHandler;
import be.ugent.ledc.rulebox.handler.explore.outliers.LOFHandler;
import be.ugent.ledc.rulebox.handler.explore.timeseries.ACFHandler;
import be.ugent.ledc.rulebox.handler.explore.timeseries.CusumDetectionHandler;
import be.ugent.ledc.rulebox.handler.explore.timeseries.FFTHandler;
import be.ugent.ledc.rulebox.handler.explore.timeseries.MovingAverageChangeDetectorHandler;
import be.ugent.ledc.rulebox.handler.explore.timeseries.SeasonalityHandler;
import be.ugent.ledc.rulebox.handler.explore.timeseries.TimeSeriesPlotHandler;
import be.ugent.ledc.rulebox.handler.reasoning.BCNFHandler;
import be.ugent.ledc.rulebox.handler.reasoning.CandidateKeyHandler;
import be.ugent.ledc.rulebox.handler.reasoning.MinimalCoverHandler;
import be.ugent.ledc.rulebox.handler.reasoning.NormalFormChecker;
import be.ugent.ledc.rulebox.handler.reasoning.SufficientSetHandler;
import be.ugent.ledc.rulebox.handler.reasoning.SynthesisHandler;
import be.ugent.ledc.rulebox.handler.repair.selection.ConditionalSequentialRepairHandler;
import be.ugent.ledc.rulebox.handler.repair.selection.JointRepairHandler;
import be.ugent.ledc.rulebox.handler.repair.ParkerRepairHandler;
import be.ugent.ledc.rulebox.handler.repair.AssertionsRepairHandler;
import be.ugent.ledc.rulebox.handler.repair.INDRepairHandler;
import be.ugent.ledc.rulebox.handler.repair.selection.SequentialRepairHandler;
import be.ugent.ledc.rulebox.handler.repair.SwipeRepairHandler;
import be.ugent.ledc.rulebox.handler.transform.GrexTransformHandler;
import be.ugent.ledc.rulebox.handler.repair.meta.AnalyseRepairHandler;
import be.ugent.ledc.rulebox.handler.server.FetchHandler;
import be.ugent.ledc.rulebox.handler.server.ListHandler;
import be.ugent.ledc.rulebox.handler.transform.CusumChangesTransformHandler;
import be.ugent.ledc.rulebox.handler.transform.RemoveAttributeHandler;
import be.ugent.ledc.rulebox.handler.transform.SeasonalityTransformHandler;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MainApp
{  
    public static final String VERSION = "1.3";
    
    public static void main(String[] args) throws IOException
    {              
        if(new File("ledc.cfg").exists())
            Config.readConfig();
        
        init();
        
        deploy().handle(args);
    }
    
    public static void init()
    {
        File localRegistry = new File(Config.LOCAL_REGISTRY);
        
        if(localRegistry.exists() && !localRegistry.isDirectory())
            throw new RuntimeException("Config file error: local registry is not a directory");
        
        if(!localRegistry.exists())
        {
            System.out.println("Initializing local registry: " + localRegistry.getAbsolutePath());
            localRegistry.mkdirs();
        }
        
        try
        {
            //Load binder pool
            BinderPool
                .getInstance()
                .load();
        }
        catch (BinderPoolException | ParseException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
            System.exit(-1);
        }
    }
    
    public static Handler deploy()
    {
        RootHandler root = new RootHandler();

        //The binder command
        Router binderHandler = new Router("binder", "management of the binder pool");
       
        Router binderAddHandler = new Router("add", "add a binder to the binder pool");
        
        binderAddHandler.register(new CsvAddHandler());
        binderAddHandler.register(new JdbcAddHandler());
        
        binderHandler.register(binderAddHandler);
        binderHandler.register(new BinderTemporalHandler());
        binderHandler.register(new BinderListHandler());
        binderHandler.register(new BinderRemoveHandler());
        
        root.register(binderHandler);
        
        //The reason command
        Router reasonHandler = new Router("reason", "allows reasoning about constraints");
        
        Router normalisation = new Router("normalisation", "allows reasoning about normal forms");

        normalisation.register(new CandidateKeyHandler());
        normalisation.register(new NormalFormChecker("v2nf", "verifies if a relation is in 2NF", NormalForm._2NF));
        normalisation.register(new NormalFormChecker("v3nf", "verifies if a relation is in 3NF", NormalForm._3NF));
        normalisation.register(new NormalFormChecker("vbcnf", "verifies if a relation is in BCNF", NormalForm.BCNF));
        normalisation.register(new SynthesisHandler());
        normalisation.register(new BCNFHandler());
        
        reasonHandler.register(normalisation);
        reasonHandler.register(new SufficientSetHandler());
        reasonHandler.register(new MinimalCoverHandler());
        
        root.register(reasonHandler);
        
        //The explore command
        Router exploreHandler = new Router("explore", "explore a dataset");

        Router timesHandler = new Router("times", "explore time series data present in a dataset");
        timesHandler.register(new TimeSeriesPlotHandler());
        timesHandler.register(new SeasonalityHandler());
        timesHandler.register(new FFTHandler());
        timesHandler.register(new ACFHandler());
        
        Router changeHandler = new Router("change", "find change points in time series data");
        changeHandler.register(new MovingAverageChangeDetectorHandler());
        changeHandler.register(new CusumDetectionHandler());
        
        timesHandler.register(changeHandler);
        
        exploreHandler.register(timesHandler);
        exploreHandler.register(new AnalysisHandler());
        exploreHandler.register(new DiagramHandler());
        
        Router outlierHandler = new Router("outliers", "check if a dataset has potential outliers");
        
        outlierHandler.register(new DBScanHandler());
        outlierHandler.register(new ZScoreHandler());
        outlierHandler.register(new IsolationForestHandler());
        outlierHandler.register(new HBOSHandler());
        outlierHandler.register(new SodaHandler());
        outlierHandler.register(new NearestNeighbourHandler());
        outlierHandler.register(new LOFHandler());
        
        exploreHandler.register(outlierHandler);
        
        Router miningHandler = new Router("rules", "find constraints that can be enforced on a dataset");
        
        miningHandler.register(new AssociationHandler());
        miningHandler.register(new OrdinalMiningHandler());
        miningHandler.register(new LiftMiningHandler());
        miningHandler.register(new TaneHandler());
        
        Router linkageHandler = new Router("linkage", "learn linkage rules from data");
        
        linkageHandler.register(new OptimalFSHandler());
        linkageHandler.register(new ExpMaxFSHandler());
        
        miningHandler.register(linkageHandler);
        
        exploreHandler.register(miningHandler);
        
        root.register(exploreHandler);
        
        //The server command
        Router serverHandler = new Router("server", "allows interaction with a ledc server to fetch information about properties");
        
        serverHandler.register(new ListHandler());
        serverHandler.register(new FetchHandler());
        
        root.register(serverHandler);
        
        //The detect command
        root.register(new DetectionHandler());
        
        //The dedupe command
        root.register(new DedupeHandler());
        
        //The repair command
        Router repairHandler = new Router("repair", "repairs errors in a dataset by searching for constraint violations");
        
        repairHandler.register(new AssertionsRepairHandler());
        
        Router sigmaRepairHandler = new Router("selection", "dedicated repair engines for selection rules");
        
        sigmaRepairHandler.register(new SequentialRepairHandler());
        sigmaRepairHandler.register(new ConditionalSequentialRepairHandler());
        sigmaRepairHandler.register(new JointRepairHandler());
        
        repairHandler.register(sigmaRepairHandler);
        
        repairHandler.register(new SwipeRepairHandler());
        repairHandler.register(new INDRepairHandler());
        repairHandler.register(new ParkerRepairHandler());
        
        //Meta
        repairHandler.register(new AnalyseRepairHandler());
        
        root.register(repairHandler);
        
        Router transformHandler = new Router("transform", "transform a dataset");
        
        Router transformAttributeHandler = new Router("attribute", "make a change to an attribute in a dataset");
        
        Router transformAddAttributeHandler = new Router("add", "add a new attribute to a dataset with values based on an existing attribute");
        
        transformAddAttributeHandler.register(new GrexTransformHandler());
        transformAddAttributeHandler.register(new CusumChangesTransformHandler());
        transformAddAttributeHandler.register(new SeasonalityTransformHandler());
        
        transformAttributeHandler.register(new RemoveAttributeHandler());
        transformAttributeHandler.register(transformAddAttributeHandler);
        
        transformHandler.register(transformAttributeHandler);
        root.register(transformHandler);
        
        root.register(new DocHandler());
        
        return root;
    }
}
