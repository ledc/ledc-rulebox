package be.ugent.ledc.rulebox;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.Contract.ContractBuilder;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.indy.algorithms.verification.InMemoryReferenceManager;
import be.ugent.ledc.indy.algorithms.verification.ReferenceManager;
import be.ugent.ledc.indy.algorithms.verification.StreamingReferenceManager;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.match.matcher.dataobject.DataObjectMatcher;
import be.ugent.ledc.pi.assertion.AbstractAssertor;
import be.ugent.ledc.pi.assertion.PropertyAssertions;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.rulebox.handler.binder.BinderBox;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.SqlBox;
import be.ugent.ledc.rulebox.handler.binder.TableBox;
import be.ugent.ledc.rulebox.io.rubix.RubixFileConstants;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A container structure to hold several types of constraints
 * @author abronsel
 */
public class Rubix
{
    public static final String SEPARATOR = ".";
    
    /**
     * A set of sigma rules with contractors
     */
    private final SigmaRuleset sigmaRules;
    
    /**
     * A set of FDs
     */
    private final RuleSet<Dataset, FD> functionalDependencies;
    
    /**
     * Assertions in terms of standardized properties
     */
    private final PropertyAssertions propertyAssertions;
    
    /**
     * A collection of inclusion dependencies
     */
    private final INDRuleset inclusionDependencies;
    
    /**
     * Cost functions that can be used in the repair of sigma rules
     */
    private final Map<String, CostFunction<?>> costFunctions;
    
    /**
     * A matcher that can be used for deduplication.
     */
    private final DataObjectMatcher<?,?> matcher;

    private Rubix(SigmaRuleset sr, RuleSet<Dataset, FD> fd, PropertyAssertions pa, INDRuleset inds, Map<String, CostFunction<?>> scf,DataObjectMatcher<?,?> matcher)
    {
        this.sigmaRules = sr;
        this.functionalDependencies = fd;
        this.propertyAssertions = pa;
        this.inclusionDependencies = inds;
        this.costFunctions = scf;
        this.matcher = matcher;
    }

    public SigmaRuleset getAllSigmaRules()
    {
        return sigmaRules;
    }
    
    public SigmaRuleset getSigmaRules(String binder)
    {
        if(!BinderPool.getInstance().exists(binder))
            throw new RuntimeException("Unknown binder " + binder);
        
        Set<String> accepted = sigmaRules
            .getContractors()
            .keySet()
            .stream()
            .filter(a -> a.startsWith(binder.concat(SEPARATOR))
                    || !a.contains(SEPARATOR))
            .collect(Collectors.toSet());
        
        SigmaRuleset filtered = sigmaRules.project(accepted);
        
        Map<String, SigmaContractor<?>> strippedContractors = filtered
            .getContractors()
            .entrySet()
            .stream()
            .collect(Collectors.toMap(
                    e -> stripBinder(e.getKey()),
                    e -> e.getValue()));
        
        return new SigmaRuleset(
            strippedContractors,
            filtered
                .getRules()
                .stream()
                .map(sr -> new SigmaRule(sr
                    .getAtoms()
                    .stream()
                    .map(aa -> aa.nameTransform(n -> stripBinder(n)))
                    .collect(Collectors.toSet())
                ))
                .collect(Collectors.toSet())
            );
    }

    public RuleSet<Dataset, FD> getAllFunctionalDependencies()
    {
        return functionalDependencies;
    }
    
    public RuleSet<Dataset, FD> getFunctionalDependencies(String binder)
    {
        if(!BinderPool.getInstance().exists(binder))
            throw new RuntimeException("Unknown binder " + binder);
        
        Set<String> accepted = functionalDependencies
            .stream()
            .flatMap(fd -> fd.getInvolvedAttributes().stream())
            .filter(a -> a.startsWith(binder.concat(SEPARATOR))
                    || !a.contains(SEPARATOR))
            .collect(Collectors.toSet());
        
        return new RuleSet<>(functionalDependencies
            .project(accepted)
            .stream()
            .map(fd -> new FD(
                fd.getLeftHandSide()
                    .stream()
                    .map(a -> stripBinder(a))
                    .collect(Collectors.toSet()),
                fd.getRightHandSide()
                    .stream()
                    .map(a -> stripBinder(a))
                    .collect(Collectors.toSet())))
            .collect(Collectors.toSet()));
    }

    public PropertyAssertions getAllPropertyAssertions()
    {
        return propertyAssertions;
    }
    
    public PropertyAssertions getPropertyAssertions(String binder) throws DependencyGraphException, PropertyParseException
    {
        if(!BinderPool.getInstance().exists(binder))
            throw new RuntimeException("Unknown binder " + binder);
        
        Set<AbstractAssertor> aaList = new HashSet<>();
        
        for(AbstractAssertor aa: propertyAssertions.getRules())
        {
            if(!aa.getAttribute().contains(SEPARATOR))
                aaList.add(aa);
            else if(aa.getAttribute().startsWith(binder.concat(SEPARATOR)))
                aaList.add(aa.renameTo(stripBinder(aa.getAttribute())));
        }

        return new PropertyAssertions(aaList);
    }

    public Map<String, CostFunction<?>> getCostFunctions()
    {
        return costFunctions;
    }

    public INDRuleset getAllInclusionDependencies()
    {
        return inclusionDependencies;
    }
    
    public INDRuleset getINDRuleset(String binderName, boolean streaming) throws FileNotFoundException, LedcException
    {
        Map<IND, ReferenceManager> referenceMapping = new HashMap<>();
        
        for(IND ind: inclusionDependencies)
        {
            validate(ind);
            
            String testName = ind.getAttributesToTest()
                .stream()
                .map(a -> Rubix.binderName(a))
                .filter(a -> a != null)
                .findFirst()
                .get();
            
            if(!testName.equals(binderName))
                continue;
            
            String bName = ind.getReferenceAttributes()
                .stream()
                .map(a -> Rubix.binderName(a))
                .filter(a -> a != null)
                .findFirst()
                .get();
            
            BinderBox<?> binderBox = BinderPool
                .getInstance()
                .getBinderBox(bName);
            
            ContractBuilder builder = new Contract.ContractBuilder();
                
            ind
            .getLeftContractorMap()
            .entrySet()
            .stream()
            .forEach(e -> builder.addContractor(stripBinder(e.getKey()), e.getValue()));

            TreeMap<String,String> pMap = ind
                .getInclusionMapping()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                    e -> stripBinder(e.getKey()),
                    e -> stripBinder(e.getValue()),
                    (v1,v2) -> v2,
                    () -> new TreeMap<>())
                );

            CPF pCondition = new CPF(
                ind
                .getCondition()
                .getAtoms()
                .stream()
                .map(aa -> aa.nameTransform(n -> stripBinder(n)))
                .collect(Collectors.toSet()));

            Map<String, SigmaContractor<?>> pLeftContractors = ind
                .getLeftContractorMap()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                    e -> stripBinder(e.getKey()),
                    e -> e.getValue()));
            
            //Can we stream?
            if(streaming && (binderBox instanceof SqlBox || binderBox instanceof TableBox))
            {
                if(binderBox instanceof SqlBox)
                {
                    referenceMapping.put(
                        new IND(pMap, pLeftContractors, pCondition),
                        new StreamingReferenceManager(
                            ((SqlBox)binderBox).bind(),
                            ((SqlBox)binderBox).getSql(),
                            builder.build())
                    );
                }
                if(binderBox instanceof TableBox)
                {
                    JDBCBinder binder = ((TableBox)binderBox).bind();
                    
                    referenceMapping.put(
                        new IND(pMap, pLeftContractors, pCondition),
                        new StreamingReferenceManager(
                            binder,
                            "select * from " + binder
                                .getRdb()
                                .getVendor()
                                .getAgent()
                                .escaping()
                                .apply(((TableBox)binderBox).getTable())
                                + ";",
                            builder.build())
                    );
                }
            }
            else
            {
                referenceMapping.put(
                    new IND(pMap, pLeftContractors, pCondition),
                    new InMemoryReferenceManager(BinderPool
                        .getInstance()
                        .data(bName)));
            }
        }
        
        return new INDRuleset();
        
    }

    public DataObjectMatcher<?, ?> getMatcher() {
        return matcher;
    }
    
    public Set<String> allAttributes()
    {
        return Stream.concat(
                Stream.concat(
                    Stream.concat(
                        functionalDependencies
                            .stream()
                            .flatMap(fd -> fd.getInvolvedAttributes().stream()),
                        inclusionDependencies.stream().flatMap(ind -> ind.getAttributesToTest().stream())
                    ),
                    propertyAssertions.getRules().stream().map(a -> a.getAttribute())
                ),
                sigmaRules.getContractors().keySet().stream())
        .collect(Collectors.toSet());
    }

    public static class RubixBuilder
    {
        private SigmaRuleset sigmaRules;
        private RuleSet<Dataset,FD> functionalDependencies;
        private PropertyAssertions propertyAssertions;
        private INDRuleset inclusionDependencies;
        private Map<String, CostFunction<?>> sigmaCostFunctions;
        private DataObjectMatcher<?,?> matcher;

        public RubixBuilder(){}
        
        public RubixBuilder withSigmaRules(SigmaRuleset sigmaRules)
        {
            this.sigmaRules = sigmaRules;
            return this;
        }
        
        public RubixBuilder withFunctionalDependencies(RuleSet<Dataset,FD> functionalDependencies)
        {
            this.functionalDependencies = functionalDependencies;
            return this;
        }
        
        public RubixBuilder withPropertyAssertions(PropertyAssertions propertyAssertions)
        {
            this.propertyAssertions = propertyAssertions;
            return this;
        }
        
        public RubixBuilder withInclusionDependencies(INDRuleset inclusionDependencies)
        {
            this.inclusionDependencies = inclusionDependencies;
            return this;
        }
        
        public RubixBuilder withSigmaCostFunctions(Map<String, CostFunction<?>> sigmaCostFunctions)
        {
            this.sigmaCostFunctions = sigmaCostFunctions;
            return this;
        }
        
        public RubixBuilder withMatcher(DataObjectMatcher<?,?> matcher)
        {
            this.matcher = matcher;
            return this;
        }
        
        public Rubix build()
        {
            return new Rubix(
                this.sigmaRules != null
                    ? this.sigmaRules
                    : new SigmaRuleset(new HashMap<>(), new HashSet<>()),
                this.functionalDependencies != null
                    ? this.functionalDependencies
                    : new RuleSet<>(),
                this.propertyAssertions != null
                    ? this.propertyAssertions
                    : new PropertyAssertions(new HashSet<>()),
                this.inclusionDependencies != null
                    ? this.inclusionDependencies
                    : new INDRuleset(),
                this.sigmaCostFunctions != null
                    ? this.sigmaCostFunctions
                    : this.sigmaRules != null
                        ? sigmaRules //Default sigma cost functions
                            .getContractors()
                            .keySet()
                            .stream()
                            .collect(Collectors
                                .toMap(
                                    a -> a,
                                    a -> new ConstantCostFunction()))
                        : new HashMap<>(),
                this.matcher
            );
        }
        
        
    } 
    
    public static boolean valid(String a, boolean requirePrefix)
    {
        return (!a.contains(Rubix.SEPARATOR) && !requirePrefix)//Valid if a has no separator and we do not require one
            || BinderPool                   //Or the prefix is a valid binder
                .getInstance()
                .exists(a.substring(0, a.lastIndexOf(Rubix.SEPARATOR)));
    }
    
    public static boolean valid(String a)
    {
        return valid(a, false);
    }
    
    private static String binderName(String a)
    {
        return !a.contains(Rubix.SEPARATOR)
            ? null
            : a.substring(0, a.lastIndexOf(Rubix.SEPARATOR));
    }
    
    private static String stripBinder(String a)
    {
        return !a.contains(Rubix.SEPARATOR)
            ? a
            : a.substring(a.lastIndexOf(Rubix.SEPARATOR)+1);
    }
    
    public static void validate(IND ind) throws ParseException
    {
        for(String a: ind.getAttributesToTest())
        {
            if(!Rubix.valid(a, true))
                throw new ParseException("Invalid attribute name in section "
            + RubixFileConstants.IND_SECTION
            + ": " + a + ". Each attribute to test must have an existing binder as prefix.");
        }

        long binderNameToTestCount = ind.getAttributesToTest()
            .stream()
            .map(a -> Rubix.binderName(a))
            .filter(a -> a != null)
            .count();

        if(binderNameToTestCount != 1L)
        {
            throw new ParseException("Parse error in section "
            + RubixFileConstants.IND_SECTION
            + ": attributes to test must have the same binder prefix.");
        }

        for(String a: ind.getReferenceAttributes())
        {
            if(!Rubix.valid(a, true))
                throw new ParseException("Invalid attribute name in section "
            + RubixFileConstants.IND_SECTION
            + ": " + a + ". Each referred attribute must have an existing binder as prefix.");
        }

        long binderNameToCheckCount = ind.getReferenceAttributes()
            .stream()
            .map(a -> Rubix.binderName(a))
            .filter(a -> a != null)
            .count();

        if(binderNameToCheckCount != 1L)
        {
            throw new ParseException("Parse error in section "
            + RubixFileConstants.IND_SECTION
            + ": referred attributes must have the same binder prefix.");
        }

        String testBinder = ind.getAttributesToTest()
            .stream()
            .map(a -> Rubix.binderName(a))
            .filter(a -> a != null)
            .findFirst()
            .get();

        String refBinder = ind.getReferenceAttributes()
            .stream()
            .map(a -> Rubix.binderName(a))
            .filter(a -> a != null)
            .findFirst()
            .get();

        for(String a: ind.getCondition().getAttributes())
        {
            if(!Rubix.valid(a, true))
                throw new ParseException("Invalid attribute name in section "
                    + RubixFileConstants.IND_SECTION
                    + ": " + a + ". Each attribute in the WHERE condition must "
                    + "have an existing binder as prefix.");

            if(!testBinder.equals(Rubix.binderName(a))
            && !refBinder.equals(Rubix.binderName(a)))
                throw new ParseException("Invalid attribute name in section "
                    + RubixFileConstants.IND_SECTION
                    + ": " + a + ". Each attribute in the WHERE condition must "
                    + "have a binder prefix that appears in the either"
                    + "the left-hand or right-hand side of the inclusion formula.");
        }
    }
    
    public static void validate(FD fd) throws ParseException
    {
        for(String a: fd.getInvolvedAttributes())
        {
            if(!Rubix.valid(a))
                throw new ParseException("Invalid attribute name in section "
            + RubixFileConstants.FD_SECTION
            + ": " + a + ". Prefix is not an existing binder.");
        }
    }
    
    public static void validate(AbstractAssertor aa) throws ParseException
    {
        if(!Rubix.valid(aa.getAttribute()))
            throw new ParseException("Invalid attribute name in section "
            + RubixFileConstants.PROPERTY_SECTION
            + ": " + aa.getAttribute()
            + ". Prefix is not an existing binder.");
    }
    
}
