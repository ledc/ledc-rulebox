package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.rulebox.handler.util.StringUtil;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.util.SetOperations;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public abstract class AbstractHandler implements Handler
{       
    public static final String HELP = "--help";
    
    private final String command;
    
    private final String description;
    
    private final List<Parameter<?>> parameters;
    
    private final List<Flag> flags;

    public AbstractHandler(String command, String description)
    {
        this.command = command;
        this.description = description;
        this.parameters = new ArrayList<>();
        this.flags = new ArrayList<>();
    }

    public String getCommand()
    {
        return command;
    }

    public String getDescription()
    {
        return description;
    }

    public List<Parameter<?>> getParameters()
    {
        return parameters;
    }

    public List<Flag> getFlags()
    {
        return flags;
    }
    
    public final void addParameter(Parameter<?> parameter)
    {
        if(parameter.getName().equals(HELP))
            throw new RuntimeException("Parameter name '" + parameter.getName() +"' is reserved.");
        
        if(parameter.getShortName().equals(HELP))
            throw new RuntimeException("Parameter short name '" + parameter.getShortName() +"' is reserved.");
        
        if(parameters.stream().anyMatch(p -> p.getName().equals(parameter.getName())))
            throw new RuntimeException("Parameter name '" + parameter.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(parameters.stream().anyMatch(p -> p.getName().equals(parameter.getShortName())))
            throw new RuntimeException("Parameter name '" + parameter.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(parameters.stream().anyMatch(p -> p.getShortName().equals(parameter.getName())))
            throw new RuntimeException("Parameter short name '" + parameter.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        if(parameters.stream().anyMatch(p -> p.getShortName().equals(parameter.getShortName())))
            throw new RuntimeException("Parameter short name '" + parameter.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getName().equals(parameter.getName())))
            throw new RuntimeException("Parameter name '" + parameter.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getName().equals(parameter.getShortName())))
            throw new RuntimeException("Parameter name '" + parameter.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getShortName().equals(parameter.getName())))
            throw new RuntimeException("Parameter short name '" + parameter.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getShortName().equals(parameter.getShortName())))
            throw new RuntimeException("Parameter short name '" + parameter.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        this.parameters.add(parameter);
    }
    
    public final void addFlag(Flag flag)
    {
        if(flag.getName().equals(HELP))
            throw new RuntimeException("Flag name '" + flag.getName() +"' is reserved.");
        
        if(flag.getShortName().equals(HELP))
            throw new RuntimeException("Flag short name '" + flag.getShortName() +"' is reserved.");
        
        if(parameters.stream().anyMatch(p -> p.getName().equals(flag.getName())))
            throw new RuntimeException("Flag name '" + flag.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(parameters.stream().anyMatch(p -> p.getName().equals(flag.getShortName())))
            throw new RuntimeException("Flag name '" + flag.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(parameters.stream().anyMatch(p -> p.getShortName().equals(flag.getName())))
            throw new RuntimeException("Flag short name '" + flag.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        if(parameters.stream().anyMatch(p -> p.getShortName().equals(flag.getShortName())))
            throw new RuntimeException("Flag short name '" + flag.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getName().equals(flag.getName())))
            throw new RuntimeException("Flag name '" + flag.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getName().equals(flag.getShortName())))
            throw new RuntimeException("Flag name '" + flag.getName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getShortName().equals(flag.getName())))
            throw new RuntimeException("Flag short name '" + flag.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        if(flags.stream().anyMatch(f -> f.getShortName().equals(flag.getShortName())))
            throw new RuntimeException("Flag short name '" + flag.getShortName() +"' is invalid. Names and shortnames must be unique.");
        
        this.flags.add(flag);
    }

    @Override
    public void handle(String[] args)
    {   
        Set<String> argSet = SetOperations.set(args);
           
        //Check parameters
        if(Stream.of(args).anyMatch(arg -> arg.equals(HELP)))
        {
            printUsage();
            System.exit(0);
        }
        
        //Check parameters
        if(parameters
            .stream()
            .filter(p -> p.isRequired())
            .anyMatch(p -> !argSet.contains(p.getName()) && !argSet.contains(p.getShortName())))
        {
            error("Required parameters missing.");
        }
        
        //Check for unknown arguments
        String unknownArg = ParameterTools.unknownArgumentCheck(args, parameters, flags);

        if(unknownArg != null)
            error("Unknown argument: " + unknownArg);
        
        //Check validity of parameters
        for(Parameter<?> parameter: parameters)
        {
            try
            {
                ParameterTools.extractParameter(parameter, args);
            }
            catch (ParameterException ex)
            {
                error(ex.getMessage());
            }
        }
    }
    
    public final void error(String message)
    {
        System.err.println(message);
        printUsage();
        System.exit(-1);
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.command);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final AbstractHandler other = (AbstractHandler) obj;
        if (!Objects.equals(this.command, other.command))
        {
            return false;
        }
        return true;
    }
    
    public void printParamaters()
    {
        int w = Stream.concat(
            parameters.stream().map(p->p.getName()),
            flags.stream().map(p->p.getName())
        )
        .mapToInt(n -> n.length())
        .max()
        .getAsInt();
        
        if(!parameters.isEmpty())
        {
            System.out.println("\nParameters:");
            
            parameters
            .stream()
            .sorted(Comparator
                .<Parameter, Boolean>comparing(Parameter::isRequired)
                .reversed()
                .thenComparing(Parameter::getName))
            .forEach(p ->
                System.out.println(StringUtil.pad(p.getName(), w)+ "\t"
                    + p.getShortName() + "\t"
                    + (p.isRequired() ? "(required) " : "(optional) ")
                    + (p.isRepeating()? "(multi) " : "")
                    + p.getDescription()
                ));
        }
    }
    
    public void printFlags()
    {
        int w = Stream.concat(
            parameters.stream().map(p->p.getName()),
            flags.stream().map(p->p.getName())
        )
        .mapToInt(n -> n.length())
        .max()
        .getAsInt();
        
        if(!flags.isEmpty())
        {
            System.out.println("\nFlags:");
            for (Flag f : flags)
            {
                System.out.println(StringUtil.pad(f.getName(),w)+ "\t"
                    + f.getShortName() + "\t"
                    + f.getDescription()
                );
            }
        }
    }
}
