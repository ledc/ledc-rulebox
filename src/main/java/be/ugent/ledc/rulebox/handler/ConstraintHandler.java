package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.core.config.parameter.Parameter;
import java.io.File;

public abstract class ConstraintHandler extends LeafHandler
{
    private final Parameter<File> constraintFileParameter = ParameterFactory.createConstraintFileParameter(true);
    
    public ConstraintHandler(String command, String description)
    {
        super(command, description);
        addParameter(constraintFileParameter);
    }

    public Parameter<File> getConstraintFileParameter()
    {
        return constraintFileParameter;
    }    
}
