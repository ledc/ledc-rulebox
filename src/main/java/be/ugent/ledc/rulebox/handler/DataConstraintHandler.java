package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.core.config.parameter.Parameter;
import java.io.File;

public abstract class DataConstraintHandler extends DataHandler
{
    private final Parameter<File> constraintFileParameter = ParameterFactory.createConstraintFileParameter(true);
    
    public DataConstraintHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(constraintFileParameter);
    }

    public Parameter<File> getConstraintFileParameter()
    {
        return constraintFileParameter;
    }
}
