package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.util.stream.Stream;

public abstract class DataHandler extends LeafHandler
{
    private final Parameter<String> binderNameParameter = ParameterFactory.createBinderNameParameter(true);
    
    public DataHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(binderNameParameter);
    }

    public Parameter<String> getBinderNameParameter()
    {
        return binderNameParameter;
    }
    
    public static final boolean isNumeric(TypeContractor<?> contractor)
    {
        return Stream.of(
            TypeContractorFactory.BIGDECIMAL.name(),
            TypeContractorFactory.DOUBLE.name(),
            TypeContractorFactory.FLOAT.name(),
            TypeContractorFactory.INTEGER.name(),
            TypeContractorFactory.LONG.name()
        ).anyMatch(n -> n.equals(contractor.name()));
    }
    
    public static final boolean isInteger(TypeContractor<?> contractor)
    {
        return Stream.of(
            TypeContractorFactory.INTEGER.name(),
            TypeContractorFactory.LONG.name()
        ).anyMatch(n -> n.equals(contractor.name()));
    }
    
    public static final ContractedDataset toSigmaContractor(ContractedDataset data, String a)
    {
        TypeContractor<?> ac = data
            .getContract()
            .getAttributeContract(a);
        
        if(ac.equals(TypeContractorFactory.INTEGER))
        {
            data = data.asInteger(a, SigmaContractorFactory.INTEGER);
        }
        else if(ac.equals(TypeContractorFactory.LONG))
            data = data.asLong(a, SigmaContractorFactory.LONG);
        else if(ac.equals(TypeContractorFactory.DOUBLE))
            data = data.asBigDecimal(a, SigmaContractorFactory.BIGDECIMAL_THREE_DECIMALS);
        else if(ac.equals(TypeContractorFactory.FLOAT))
            data = data.asBigDecimal(a, SigmaContractorFactory.BIGDECIMAL_THREE_DECIMALS);
        else if(ac.equals(TypeContractorFactory.BIGDECIMAL))
        {
            int minScale = data
            .stream()
            .filter(o -> o.get(a) != null)
            .mapToInt(o -> o.getBigDecimal(a).scale())
            .min()
            .getAsInt();

            switch (minScale)
            {
                case 3:
                    data = data.asBigDecimal(a, SigmaContractorFactory.BIGDECIMAL_THREE_DECIMALS);
                    break;
                case 2:
                    data = data.asBigDecimal(a, SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);
                    break;
                case 1:
                    data = data.asBigDecimal(a, SigmaContractorFactory.BIGDECIMAL_ONE_DECIMAL);
                    break;
                case 0:
                    data = data.asBigDecimal(a, SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS);
                    break;
                default:
                    break;
            }
        }
        else if(ac.equals(TypeContractorFactory.DATETIME))
            data = data.asDateTime(a, SigmaContractorFactory.DATETIME_SECONDS);
        
        return data;
    }
    
}
