package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.binding.csv.CSVDataWriter;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.agents.DMLGenerator;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.rulebox.handler.binder.BinderBox;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.handler.binder.CsvBox;
import be.ugent.ledc.rulebox.handler.binder.TableBox;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface DataModifier
{
    public final Parameter<String> repairBinderParameter = ParameterFactory
       .createRepairBinderParameter(true);
    
    public final Flag softRepairFlag = ParameterFactory
        .createSoftRepairFlag();
     
    public default void removeRows(ContractedDataset original, String inputBinder, ContractedDataset toRemove, String outputBinder, boolean soft) throws BinderPoolException, DataWriteException, BindingException, SQLException
    {
        String binderType = BinderPool.getInstance().getBinderType(outputBinder);
        
        if(binderType.equals(BinderBox.TYPE_CSV))
        {
            CsvBox csvBox = (CsvBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            CSVDataWriter csvDataWriter = new CSVDataWriter(csvBox.bind());
            
            toRemove
                .stream()
                .forEach(o -> original.removeDataObject(o));
            
            csvDataWriter.writeData(original);
            
            csvDataWriter.close();
        }
        else if(binderType.equals(BinderBox.TYPE_JDBC_TABLE))
        {
            TableBox tableBox = (TableBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            //Get binder
            JDBCBinder binder = tableBox.bind();
            
            //Get schema signature for table
            DatabaseSchema schema = binder.getSchema();
            
            if(!schema.containsKey(tableBox.getTable()))
                throw new DataWriteException("Cannot remove rows in binder "
                    + outputBinder
                    + ". Cause: table "
                    + tableBox.getTable()
                    + " does not exist.");
            
            TableSchema tableSchema = schema.get(tableBox.getTable());
            
            if(!original.getContract().equals(toRemove.getContract()))
                throw new DataWriteException("Cannot remove rows in binder "
                    + outputBinder
                    + ". Cause: contracts of original data and rows to remove differ.");
            
            if(!tableSchema.getAttributeNames().equals(original.getContract().getAttributes()))
                throw new DataWriteException("Cannot remove rows in binder "
                    + outputBinder
                    + ". Cause: attributes of table " + tableBox.getTable()
                    + " differ from attributes in rows to remove.");
            
            Key primaryKey = tableSchema.getPrimaryKey();

            if(primaryKey == null || primaryKey.isEmpty())
                throw new DataWriteException("Cannot remove rows in binder "
                    + outputBinder
                    + ". Cause: no primary key defined in table " + tableBox.getTable());
            
            if(!toRemove.getContract().getAttributes().containsAll(primaryKey))
                throw new DataWriteException("Cannot remove rows in binder "
                    + outputBinder
                    + ". Cause: primary key "
                    + primaryKey
                    + " is not present.");
            
            List<String> script = new ArrayList<>();
            
            //Function for name escaping
            Function<String, String> escaping = binder
                .getRdb()
                .getVendor()
                .getAgent()
                .escaping();
            
            //DML Generator
            DMLGenerator generator = new DMLGenerator(escaping);
                
            //Must dump in other binder
            if(!inputBinder.equals(outputBinder))
            {
                //Remove all rows that are present
                script.add("delete from "
                    + escaping.apply(tableBox.getTable())
                    + ";");
                
                HashSet<DataObject> keysToRemove = new HashSet<>(toRemove.project(primaryKey).getDataObjects());
                
                //Add rows that must not be removed
                script.addAll(original
                    .select(o -> !keysToRemove.contains(o.project(primaryKey)))
                    .stream()
                    .map(o -> generator.generateInsertCode(
                        o,
                        tableBox.getTable(),
                        binder.getRdb().getSchemaName())
                    )
                    .collect(Collectors.toList()));
            }
            else
            {
                //Delete rows to be removed
                script.addAll(toRemove
                    .stream()
                    .map(o -> generator
                        .generateDeleteCode(
                            o,
                            tableSchema,
                            binder.getRdb().getSchemaName()))
                    .collect(Collectors.toList()));
            }
            
            //Decide what to do with script
            if(soft)
                script.stream().forEach(System.out::println);
            else
                executeSqlScript(script, binder);
        }
    }
    
    public default void updateRows(ContractedDataset original, String inputBinder, ContractedDataset modified, String outputBinder, boolean soft) throws BinderPoolException, DataWriteException, BindingException, SQLException
    {
        String binderType = BinderPool.getInstance().getBinderType(outputBinder);
        
        if(binderType.equals(BinderBox.TYPE_CSV))
        {
            CsvBox csvBox = (CsvBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            CSVDataWriter csvDataWriter = new CSVDataWriter(csvBox.bind());
           
            csvDataWriter.writeData(modified);
            
            csvDataWriter.close();
        }
        else if(binderType.equals(BinderBox.TYPE_JDBC_TABLE))
        {
            TableBox tableBox = (TableBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            //Get binder
            JDBCBinder binder = tableBox.bind();
            
            //Get schema signature for table
            DatabaseSchema schema = binder.getSchema();
            
            if(!schema.containsKey(tableBox.getTable()))
                throw new DataWriteException("Cannot update rows in binder "
                    + outputBinder
                    + ". Cause: table "
                    + tableBox.getTable()
                    + " does not exist.");
            
            TableSchema tableSchema = schema.get(tableBox.getTable());
            
            if(!original.getContract().equals(modified.getContract()))
                throw new DataWriteException("Cannot update rows in binder "
                    + outputBinder
                    + ". Cause: contracts of original data and updated data differ.");
            
            if(!tableSchema.getAttributeNames().containsAll(modified.getContract().getAttributes()))
                throw new DataWriteException("Cannot update rows in binder "
                    + outputBinder
                    + ". Cause: attributes of table " + tableBox.getTable()
                    + " differ from attributes in new rows.");
            
            Key primaryKey = tableSchema.getPrimaryKey();

            if(primaryKey.isEmpty())
                throw new DataWriteException("Cannot update rows in binder "
                    + outputBinder
                    + ". Cause: table '" + tableBox.getTable() + "' "
                    + " has no primary key set.");
            
            if(!modified.getContract().getAttributes().containsAll(primaryKey))
                throw new DataWriteException("Cannot update rows in binder "
                    + outputBinder
                    + ". Cause: primary key "
                    + primaryKey
                    + " is not present.");
            
            List<String> script = new ArrayList<>();
            
            //Function for name escaping
            Function<String, String> escaping = binder
                .getRdb()
                .getVendor()
                .getAgent()
                .escaping();
            
            //DML Generator
            DMLGenerator generator = new DMLGenerator(escaping);

            //Must dump in other binder
            if(!inputBinder.equals(outputBinder))
            {
                //Remove all rows that are present
                script.add("delete from " + escaping.apply(tableBox.getTable()));
                
                //Add modified rows
                script.addAll(modified
                    .stream()
                    .map(o -> generator.generateInsertCode(
                        o,
                        tableBox.getTable(),
                        binder.getRdb().getSchemaName())
                    )
                    .collect(Collectors.toList()));
            }
            else
            {
                List<DataObject> diff = diff(original, modified, new ArrayList<>(primaryKey));
                
                script.addAll(diff
                .stream()
                .map(o -> generator
                    .generateUpdateCode(
                        o,
                        tableBox.getTable(),
                        o.project(primaryKey),
                        binder.getRdb().getSchemaName()))
                .collect(Collectors.toList()));
            }
            
            if(soft)
                script.stream().forEach(System.out::println);
            else
                executeSqlScript(script, binder);
        }
    }
    
    public default void removeAttribute(String attributeToRemove, String outputBinder, boolean soft) throws BinderPoolException, BindingException, DataWriteException, SQLException
    {
        String binderType = BinderPool.getInstance().getBinderType(outputBinder);
        
        if(binderType.equals(BinderBox.TYPE_JDBC_TABLE))
        {
            TableBox tableBox = (TableBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            //Get binder
            JDBCBinder binder = tableBox.bind();
            
            //Get schema signature for table
            DatabaseSchema schema = binder.getSchema();
            
            if(!schema.containsKey(tableBox.getTable()))
                throw new DataWriteException("Cannot remove attribute in binder "
                    + outputBinder
                    + ". Cause: table "
                    + tableBox.getTable()
                    + " does not exist.");
            
            TableSchema tableSchema = schema.get(tableBox.getTable());
            
            if(tableSchema.getAttributeNames().contains(attributeToRemove))
            {
                List<String> script = new ArrayList<>();
                
                 //Function for name escaping
                Function<String, String> escaping = binder
                    .getRdb()
                    .getVendor()
                    .getAgent()
                    .escaping();
                
                //Remove all rows that are present
                script.add("alter table " + escaping.apply(tableBox.getTable())
                    + " drop column " + escaping.apply(attributeToRemove)
                );

                //Decide what to do with script
                if(soft)
                    script.stream().forEach(System.out::println);
                else
                    executeSqlScript(script, binder);
            }
        }
    }
    
    public default void removeAttribute(ContractedDataset original, String attributeToRemove, String outputBinder, boolean soft) throws BinderPoolException, DataWriteException, BindingException, SQLException
    {
        String binderType = BinderPool.getInstance().getBinderType(outputBinder);
        
        if(binderType.equals(BinderBox.TYPE_CSV))
        {
            CsvBox csvBox = (CsvBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            CSVDataWriter csvDataWriter = new CSVDataWriter(csvBox.bind());
            csvDataWriter.writeData(original.inverseProject(attributeToRemove));
            csvDataWriter.close();
        }
        else if(binderType.equals(BinderBox.TYPE_JDBC_TABLE))
        {
            removeAttribute(attributeToRemove, outputBinder, soft);
        }
    }

    public default void addAttribute(ContractedDataset extended, String attribute, String original, String outputBinder, boolean soft, boolean removeOriginal) throws BinderPoolException, DataWriteException, BindingException, SQLException
    {
       String binderType = BinderPool.getInstance().getBinderType(outputBinder);
        
        if(binderType.equals(BinderBox.TYPE_CSV))
        {
            CsvBox csvBox = (CsvBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            CSVDataWriter csvDataWriter = new CSVDataWriter(csvBox.bind());
            
            if(removeOriginal)
                csvDataWriter.writeData(extended.inverseProject(original));
            else
                csvDataWriter.writeData(extended);
            
            csvDataWriter.close();
        }
        else if(binderType.equals(BinderBox.TYPE_JDBC_TABLE))
        {
            TableBox tableBox = (TableBox) BinderPool
                .getInstance()
                .getBinderBox(outputBinder);
            
            //Get binder
            JDBCBinder binder = tableBox.bind();
            
            //Get schema signature for table
            DatabaseSchema schema = binder.getSchema();
            
            if(!schema.containsKey(tableBox.getTable()))
                throw new DataWriteException("Cannot add attribute in binder "
                    + outputBinder
                    + ". Cause: table "
                    + tableBox.getTable()
                    + " does not exist.");
            
            TableSchema tableSchema = schema.get(tableBox.getTable());
            
            Key primaryKey = tableSchema.getPrimaryKey();

            if(!extended.getContract().getAttributes().containsAll(primaryKey))
                throw new DataWriteException("Cannot add attribute in binder "
                    + outputBinder
                    + ". Cause: primary key "
                    + primaryKey
                    + " is not present.");
            
            List<String> script = new ArrayList<>();
            
            //Function for name escaping
            Function<String, String> escaping = binder
                .getRdb()
                .getVendor()
                .getAgent()
                .escaping();
            
            //DML Generator
            DMLGenerator generator = new DMLGenerator(escaping);
            
            if(tableSchema.getAttributeNames().contains(attribute))
            {
                //Remove the attribute first
                script.add("alter table "
                    + escaping.apply(tableBox.getTable())
                    + " drop column "
                    + escaping.apply(attribute) + ";"
                );
            }
            
            //Add it again
            script.add("alter table "
                    + escaping.apply(tableBox.getTable())
                    + " add column "
                    + escaping.apply(attribute)
                    + extended
                        .getContract()
                        .getAttributeContract(attribute)
                        .toPersistentDatatype(true)
                    + ";"
                );
            
            //Add update statements
            extended.stream().map(o -> generator.generateUpdateCode(
                    o.project(attribute),
                    tableBox.getTable(),
                    o.project(primaryKey),
                    binder.getRdb().getSchemaName()));
           
            if(removeOriginal)
            {
                //Remove the original
                script.add("alter table "
                    + escaping.apply(tableBox.getTable())
                    + " drop column "
                    + escaping.apply(original) + ";"
                );
            }
           
            //Decide what to do with script
            if(soft)
                script.stream().forEach(System.out::println);
            else
                executeSqlScript(script, binder);
        }
    }
    
    private void executeSqlScript(List<String> sqlStatements, JDBCBinder binder) throws SQLException, BindingException
    {
        try (Connection connection = binder.connect(); Statement statement = connection.createStatement())
        {
            for (String sqlStatement : sqlStatements)
            {
                statement.addBatch(sqlStatement);
            }
            
            //Execute batch
            statement.executeBatch();
        }
    }

    /**
     * Returns a list of DataObjects from the repair dataset that differ from the original dataset
     * with the given attributes in place as the primary key for both datasets
     * @param original
     * @param repair
     * @param key
     * @return
     * @throws DataWriteException 
     */
    private List<DataObject> diff(ContractedDataset original, ContractedDataset repair, List<String> key) throws DataWriteException
    {
        List<DataObject> oSorted = original
            .sort(DataObject.getProjectionComparator(key))
            .getDataObjects();
        
        List<DataObject> rSorted = repair
            .sort(DataObject.getProjectionComparator(key))
            .getDataObjects();
             
        //Sanity check 1: require equal amount of objects
        if(oSorted.size() != rSorted.size())
            throw new DataWriteException("Cannot update rows. "
                    + "Cause: original and repaired data have different amount of rows.");
        
        List<DataObject> diffObjects = new ArrayList<>();
        
        for(int i=0; i<oSorted.size(); i++)
        {
            DataObject o = oSorted.get(i);
            DataObject r = rSorted.get(i);
                       
            //Check if original has non-null key attributes
            if(key.stream().anyMatch(a -> o.get(a)== null))
                throw new DataWriteException("Cannot update rows. "
                    + "Cause: original object has null values for key.\n"
                    + "Object: " + o + "\n"
                    + "Key: " + key
                );
            //Check if repair has non-null key attributes
            if(key.stream().anyMatch(a -> r.get(a)== null))
                throw new DataWriteException("Cannot update rows. "
                    + "Cause: repair object has null values for key.\n"
                    + "Object: " + r + "\n"
                    + "Key: " + key
                );
            
            //Since the key is unique and the amount of objects is the same
            //we expect, after sorting, keys are aligned.
            if(!o.project(key).equals(r.project(key)))
                throw new DataWriteException("Cannot update rows. "
                    + "Cause: original and repair have key mismatches.\n"
                    + "Original: " + o + "\n"
                    + "Repair: " + r
                );
            
            //If there are differences, add the repair object
            if(!o.diff(r).isEmpty())
                diffObjects.add(r);
        }
        
        System.out.println("Number of objects with changes: " + diffObjects.size());
        return diffObjects;
    }
    
}