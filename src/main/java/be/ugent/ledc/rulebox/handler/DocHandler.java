package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.rulebox.Config;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class DocHandler extends LeafHandler
{
    public DocHandler()
    {
        super("man", "Show online documentation for rulebox");
    }

    @Override
    public void handle(String[] args)
    {
        if (Desktop.isDesktopSupported()
        &&  Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
        {
            try
            {
                Desktop.getDesktop().browse(new URI(Config.DOCUMENTATION));
            }
            catch (URISyntaxException | IOException ex)
            {
                super.error(ex.getMessage());
            }
        }
        else
        {
            System.out.println("Cannot open default browser...");
            System.out.println("See " + Config.DOCUMENTATION + " for online documentation.");
        }
    }
    
    
    
}
