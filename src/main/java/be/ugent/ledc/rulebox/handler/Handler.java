package be.ugent.ledc.rulebox.handler;

/**
 * Handles a command by consuming parameters and doing something with them.
 **/
public interface Handler
{
    public void handle(String[] args);
    
    public void printUsage();
}
