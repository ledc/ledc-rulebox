package be.ugent.ledc.rulebox.handler;

public abstract class LeafHandler extends AbstractHandler
{
    public LeafHandler(String command, String description)
    {
        super(command, description);
    }
    
    @Override
    public void printUsage()
    {
        System.out.println(getCommand() + ": " + getDescription());
        printParamaters();
        printFlags();
    }
    
}
