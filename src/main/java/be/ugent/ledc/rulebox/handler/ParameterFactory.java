package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.rulebox.handler.binder.BinderBox;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import java.io.File;

public class ParameterFactory
{
    public static Parameter<File> createConstraintFileParameter(boolean required)
    {
        return new Parameter<>(
            "constraints",
            "--c",
            required,
            (s) -> new File(s),
            ".rbx file that holds relevant constraints."
        )
        .addVerifier(f -> f.exists(), "Constraint file does not exist.")
        .addVerifier(f -> f.getName().endsWith(".rbx"), "Constraint file extension should be .rbx.");
    }
    
    public static Parameter<String> createBinderNameParameter(boolean required)
    {
        return new Parameter<>(
            "data-binder",
            "--d",
            required,
            (s) -> s,
            "Name of the data binder. Use 'binder list' for an overview."
        )
        .addVerifier(
            n -> BinderPool.getInstance().exists(n),
            "Unknown data binder name."
        );
    }
    
    public static Parameter<String> createRepairBinderParameter(boolean required)
    {
        return new Parameter<>(
            "repair-binder",
            "--r",
            required, //Repair is optional
            (s) -> s,
            "Name of the binder to which the repair must be written."
        )
        .addVerifier(
            (b) -> BinderPool.getInstance().exists(b),
            "Unknown repair binder."
        )
        .addVerifier(
            (b) ->
                {
                    try
                    {
                        return SetOperations
                            .set(
                                BinderBox.TYPE_CSV,
                                BinderBox.TYPE_JDBC_TABLE)
                            .contains(BinderPool
                                .getInstance()
                                .getBinderType(b));
                    }
                    catch (BinderPoolException ex)
                    {
                        return false;
                    }
                },
            "Repair binder type must be of type "
                + BinderBox.TYPE_CSV
                + " or "
                + BinderBox.TYPE_JDBC_TABLE
        );
    }
    
    public static Flag createSoftRepairFlag()
    {
        return new Flag(
        "soft-repair",
        "--sr",
        "When enabled, SQL statements are not executed, but printed to the console");
    }
    
    public static Parameter<File> createOutputFileParameter(boolean required)
    {
        return new Parameter<>(
            "output-file",
            "--of",
            required,
            (s) -> new File(s),
            "File to which output is written."
        );
    }
    
}
