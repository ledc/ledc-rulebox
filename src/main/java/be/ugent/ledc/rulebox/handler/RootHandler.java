package be.ugent.ledc.rulebox.handler;

import be.ugent.ledc.rulebox.MainApp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RootHandler extends Router
{
    public RootHandler()
    {
        super(null, null);
    }

    @Override
    public void printUsage()
    {
        System.out.println("This is rulebox version " + MainApp.VERSION);
        System.out.println("Usage: java -jar rulebox.jar <command sequence> <options>");
        
        System.out.println("Available command sequences:");
        
        List<String> sequences = new ArrayList<>();
        
        build("", getSubHandlerMap(), sequences);
        
        sequences
        .stream()
        .sorted()
        .forEach(System.out::println);
    }
    
    private void build(String prefix, Map<String, Handler> handlerMap, List<String> sequences)
    {
        for(String subCommand: handlerMap.keySet())
        {
            if(handlerMap.get(subCommand) instanceof Router)
                build(
                    prefix + " " + subCommand,
                    ((Router)handlerMap.get(subCommand)).getSubHandlerMap(),
                    sequences
                );
            else
            {
                sequences.add((prefix + " " + subCommand).trim());
            }
        }
    }
}
