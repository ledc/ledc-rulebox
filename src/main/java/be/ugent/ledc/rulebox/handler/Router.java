package be.ugent.ledc.rulebox.handler;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A special type of handler that reroutes commands to sub-handlers
 */
public class Router extends AbstractHandler
{   
    private final Map<String, Handler> subHandlerMap = new LinkedHashMap<>();

    public Router(String command, String description)
    {
        super(command, description);
    }

    @Override
    public void handle(String[] args)
    {
        if(args.length == 0 || !subHandlerMap.keySet().contains(args[0]))
        {
            printUsage();
        }
        else
        {
            //Pass the remaining arguments to the sub handler
            subHandlerMap
                .get(args[0])
                .handle(Arrays.copyOfRange(args, 1, args.length));
        }
        
    }

    @Override
    public void printUsage()
    {
        System.out.println(getCommand() + ": " + getDescription());
        
        if(super.getCommand() == null)
            System.out.print("Available commands: ");
        else
            System.out.print("Available sub-commands: ");
        
        System.out.println(
            subHandlerMap
            .keySet()
            .stream()
            .collect(Collectors.joining(","))
        );
        
    }
    
    public void register(AbstractHandler handler)
    {
        this.subHandlerMap.put(handler.getCommand(), handler);
    }

    protected Map<String, Handler> getSubHandlerMap()
    {
        return subHandlerMap;
    }
}
