package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import java.nio.charset.StandardCharsets;

public abstract class BinderAddHandler extends BinderHandler
{
    private final Flag forceFlag = new Flag
    (
        "force",
        "--f",
        "Overwrite the binder if it already exists."
    );
    
    private final Parameter<String> encodingParameter = new OptionalParameter<>(
        "encoding",
        "--e",
        (s) -> s,
        "The encoding of the connection. Default value is " + StandardCharsets.UTF_8.name(),
        StandardCharsets.UTF_8.name()
    );
    
    public BinderAddHandler(String command, String description)
    {
        super(command, description);
        addParameter(encodingParameter);
        addFlag(forceFlag);
    }

    public String getEncoding(String[] args) throws ParameterException
    {
        return ParameterTools.extractParameter(encodingParameter, args);
    }
    
    public boolean isForced(String[] args) throws ParameterException
    {
        return ParameterTools.extractFlag(forceFlag, args);
    }

    public Flag getForceFlag()
    {
        return forceFlag;
    }
}
