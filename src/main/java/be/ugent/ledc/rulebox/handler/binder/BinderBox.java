package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.datastructures.TemporalDataset;
import be.ugent.ledc.core.DataException;
import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.binding.jdbc.schema.DiagrammerOptions;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Set;

public abstract class BinderBox<B> implements Persistable
{
    public static final String TYPE_CSV         = "csv";
    public static final String TYPE_JDBC        = "jdbc";
    public static final String TYPE_JDBC_SQL    = "jdbc-sql";
    public static final String TYPE_JDBC_TABLE  = "jdbc-table";
    
    public static final String BINDER = "Binder";
    public static final String TYPE   = "BinderType";
    public static final String NAME   = "BinderName";
    
    public static final String TEMPORAL = "Temporal";
    
    public static final Set<String> TYPES = SetOperations.set(
        TYPE_CSV,
        TYPE_JDBC,
        TYPE_JDBC_SQL,
        TYPE_JDBC_TABLE);
    
    /**
     * A unique name for the box
     */
    private final String name;
    
    /**
     * The type of the box
     */
    private final String type;
    
    private Temporal<?> temporalInfo;

    public BinderBox(String name, String type, Temporal<?> temporalInfo)
    {
        this.name = name;
        this.type = type;
        this.temporalInfo = temporalInfo;
        
        if(!TYPES.contains(type))
        {
            throw new DataException("Unknown binder type '" + type + "'.");
        }
    }
    
    public BinderBox(String name, String type)
    {
        this(name, type, null);
    }

    public String getName()
    {
        return name;
    }

    public String getType()
    {
        return type;
    }

    public Temporal<?> getTemporalInfo()
    {
        return temporalInfo;
    }

    public void setTemporalInfo(Temporal<?> temporalInfo)
    {
        this.temporalInfo = temporalInfo;
    }
    
    public boolean isTemporal()
    {
        return this.temporalInfo != null;
    }
    
    public boolean isTemporalPartitioned()
    {
        return isTemporal() && this.temporalInfo.isPartioned();
    }
    
    public TemporalDataset<?> temporalData() throws FileNotFoundException, DataReadException, ChronosException
    {
        if(!isTemporal())
            throw new DataReadException("Binder '" + this.name + "' is not a temporal dataset.");
        
        if(this.temporalInfo.isPartioned())
            throw new DataReadException("Cannot read a single temporal dataset from '"
                + this.name
                + "'. "
                + "Cause: dataset has partition attribute "
                + this.temporalInfo.getPartitionAttribute() + "."
            );
        
        return TemporalDataset.create(
            data(),
            this.temporalInfo.getTimeAttribute(),
            this.temporalInfo.getContractor()
            );
    }
    
    public <X extends Comparable<? super X>> Map<String, TemporalDataset<X>> temporalPartitionedData() throws FileNotFoundException, DataReadException, ChronosException
    {
        if(!isTemporal())
            throw new DataReadException("Binder '" + this.name + "' is not a temporal dataset.");
        
        if(!this.temporalInfo.isPartioned())
            throw new DataReadException("Cannot read a multiple temporal datasets from '"
                + this.name
                + "'. "
                + "Cause: dataset has no partition attribute."
            );
        
        return TemporalDataset.<X>create(data(),
            this.temporalInfo.getTimeAttribute(),
            (OrdinalContractor<X>) this.temporalInfo.getContractor(),
            this.temporalInfo.getPartitionAttribute()
            );
    }
    
    public abstract ContractedDataset data() throws FileNotFoundException, DataReadException;
    
    public abstract DatabaseSchema schema(DiagrammerOptions options) throws BindingException;
    
    public abstract B bind();
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        FeatureMap fm = new FeatureMap()
            .addFeature(TYPE, type)
            .addFeature(NAME, name);
        
        if(temporalInfo != null)
            fm.addFeature(TEMPORAL, this
                .temporalInfo
                .buildFeatureMap()
                .getFeatures());
        
        return fm;
    }

    @Override
    public String toString()
    {
        return name
        + "\n -> Type: " + getType()
        + (getTemporalInfo() == null
            ? ""
            : "\n -> Temporal on time attribute "
                + getTemporalInfo().getTimeAttribute()
                + " with contract " + getTemporalInfo().getContractor().name()
                + (getTemporalInfo().getPartitionAttribute() == null
                    ? ""
                    : " partitioned by " + getTemporalInfo().getPartitionAttribute()));
    }
    
    
}
