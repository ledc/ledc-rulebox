package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.rulebox.handler.LeafHandler;

public abstract class BinderHandler extends LeafHandler
{
    private final Parameter<String> binderNameParameter = new Parameter<>(
        "name",
        "--n",    
        true,
        (s) -> s,
        "A unique name for the binder."
    );
    
    public BinderHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(binderNameParameter);
    }

    public final Parameter<String> getBinderNameParameter()
    {
        return binderNameParameter;
    }
}
