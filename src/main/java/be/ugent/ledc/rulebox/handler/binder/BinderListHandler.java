package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.rulebox.handler.AbstractHandler;
import java.util.Set;

public class BinderListHandler extends AbstractHandler
{
    private final Flag verboseFlag = new Flag
    (
        "verbose",
        "--v",
        "Verbose print. Show all details of the binders."
    );
    
    private final Parameter<String> filterParameter = new OptionalParameter<>(
        "filter",
        "--f",    
        (s) -> s,
        "A regex that is used to filter the binders that must be printed. By default, all binders are listed.",
        ".+"
    );

    public BinderListHandler()
    {
        super("list", "List information on binders available in the pool.");
        
        addParameter(filterParameter);
        addFlag(verboseFlag);
    }

    @Override
    public void printUsage()
    {
        System.out.println(getCommand() + ": " + getDescription());
        printParamaters();
        printFlags();
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            Set<String> names = BinderPool
                .getInstance()
                .list(ParameterTools.extractParameter(filterParameter, args));
            
            if(names.isEmpty())
            {
                System.out.println("No binders found.");
                return;
            }

            boolean verbose = ParameterTools.extractFlag(verboseFlag, args);
          
            names
            .stream()
            .sorted()
            .map(name -> verbose
                ? BinderPool.getInstance().print(name)
                : name + (BinderPool.getInstance().isTemporal(name) ? " (temporal)" : ""))
            .forEach(System.out::println);
        }
        catch (LedcException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
