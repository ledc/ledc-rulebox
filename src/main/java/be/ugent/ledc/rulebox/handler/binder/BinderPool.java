package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.csv.CSVBinder;
import be.ugent.ledc.core.binding.csv.CSVBindingFactory;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.JDBCBinderFactory;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.binding.jdbc.schema.DiagrammerOptions;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.rulebox.Config;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorUtil;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Provides a simple management of binders, SQL statements, aliases ... for their combinations.
 * @author abronsel
 */
public class BinderPool
{
    private final Map<String, BinderBox> binderBoxes;
    
    private final static BinderPool INSTANCE = new BinderPool();

    private BinderPool()
    {
        this.binderBoxes = new HashMap<>();
    }
    
    public static BinderPool getInstance()
    {
        return INSTANCE;
    }
    
    /**
     * Load the data for the source with this name
     * @param name
     * @return
     * @throws be.ugent.ledc.rulebox.handler.binder.BinderPoolException
     * @throws IOException 
     * @throws java.io.FileNotFoundException 
     * @throws be.ugent.ledc.core.binding.DataReadException 
     */
    public ContractedDataset data(String name) throws BinderPoolException, FileNotFoundException, DataReadException
    {
        if(binderBoxes.get(name) == null)
            throw new BinderPoolException("Unknown data binder name '" + name + "'");
        
        return binderBoxes.get(name).data();
    }
    
    public DatabaseSchema schema(String name, DiagrammerOptions options) throws BinderPoolException, BindingException
    {
        if(binderBoxes.get(name) == null)
            throw new BinderPoolException("Unknown data binder name '" + name + "'");
        
        return binderBoxes.get(name).schema(options);
    }
    
    public BinderBox<?> getBinderBox(String name) throws BinderPoolException
    {
        if(binderBoxes.get(name) == null)
            throw new BinderPoolException("Unknown data source name '" + name + "'.");

        return binderBoxes.get(name);
    }
    
    public String getBinderType(String name) throws BinderPoolException
    {
        if(binderBoxes.get(name) == null)
            throw new BinderPoolException("Unknown data source name '" + name + "'.");

        return binderBoxes.get(name).getType();
    }
    
    public boolean exists(String name)
    {
        return binderBoxes.get(name) != null;
    }
    
    public boolean isTemporal(String name)
    {
        return binderBoxes.get(name) != null && binderBoxes.get(name).isTemporal();
    }
    
    public Set<String> list(String regex)
    {
        return binderBoxes
            .keySet()
            .stream()
            .filter(n -> n.matches(regex))
            .collect(Collectors.toSet());
    }
    
    public String print(String name)
    {
        return binderBoxes.get(name) == null
            ? ""
            : binderBoxes.get(name).toString();
    }
    
    public void register(BinderBox box) throws BinderPoolException
    {
        if(binderBoxes.get(box.getName()) != null)
            throw new BinderPoolException("Data source with name '" + box.getName() + "' already exists.");
        
        binderBoxes.put(box.getName(), box);
    }
    
    public void delete(String name) throws BinderPoolException
    {       
        binderBoxes.remove(name);
    }
    
    public void overwrite(BinderBox box)
    {        
        binderBoxes.put(box.getName(), box);
    }
    
    public Set<String> listNames()
    {
        return new HashSet<>(binderBoxes.keySet());
    }
    
    public void dump() throws IOException
    {
        File poolFile = new File(Config.BINDER_POOL);
        
        if(!poolFile.exists())
            poolFile.createNewFile();
        
        //Create JSON Array
        JSONArray mainArray = new JSONArray();
        
        for(BinderBox box: binderBoxes.values())
        {
            mainArray.put(
                new JSONObject(
                    box.buildFeatureMap().getFeatures()));
        }
        
        try (BufferedWriter writer = new BufferedWriter(
            new OutputStreamWriter(
                new FileOutputStream(poolFile),
                StandardCharsets.UTF_8)
        ))
        {
            writer.write(mainArray.toString(4));
            writer.flush();
        }
    }
    
    public void load() throws BinderPoolException, FileNotFoundException, ParseException
    {
        File poolFile = new File(Config.BINDER_POOL);
        
        if(!poolFile.exists())
            return;
        
        //Load the array with binders
        JSONArray boxArray = new JSONArray(
            new JSONTokener(
                new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream(poolFile),
                        StandardCharsets.UTF_8))));
        
        for(int i=0; i<boxArray.length(); i++)
        {
            if(!(boxArray.get(i) instanceof JSONObject))
                throw new BinderPoolException("Binder file '"
                    + Config.BINDER_POOL
                    + "' is not correctly formatted.\n"
                    + "Expected: JSONArray of JSONObjects."
                );
            
            //Read the next object
            JSONObject boxObject = boxArray.getJSONObject(i);
            
            //Verify
            if(!boxObject.has(BinderBox.TYPE))
                throw new BinderPoolException("A data binder requires the binder type in a field '" 
                    + BinderBox.TYPE + "'.");
        
            if(boxObject.get(BinderBox.TYPE) == null)
                throw new BinderPoolException("Binder type is null.");
            
            if(!boxObject.has(BinderBox.NAME))
                throw new BinderPoolException("A data binder must have a unique name in a field '"
                    + BinderBox.NAME + "'.");
        
            if(boxObject.get(BinderBox.NAME) == null)
                throw new BinderPoolException("Data binder name is null.");
            
            if(!(boxObject.get(BinderBox.NAME) instanceof String))
                throw new BinderPoolException("Data binder name must be a string.");
            
            if(!boxObject.has(BinderBox.BINDER))
                throw new BinderPoolException("A data binder requires a field '"
                    + BinderBox.BINDER + "'.");

            if(boxObject.get(BinderBox.BINDER) == null)
                throw new BinderPoolException("Binder is null.");

            if(!(boxObject.get(BinderBox.BINDER) instanceof JSONObject))
                throw new BinderPoolException("Binder must be a JSONObject.");
            
            String name = boxObject.getString(BinderBox.NAME);
            
            BinderBox bb = null;
            
            switch(boxObject.getString(BinderBox.TYPE))
            {
                case BinderBox.TYPE_CSV:
                    bb = parseCsvBox(name, boxObject);
                    break;
                case BinderBox.TYPE_JDBC:
                case BinderBox.TYPE_JDBC_SQL:
                case BinderBox.TYPE_JDBC_TABLE:
                    bb = parseJdbcBox(name, boxObject);
                    break;
            }
            
            //Is this dataset a temporal dataset?
            if(boxObject.has(BinderBox.TEMPORAL) && boxObject.get(BinderBox.TEMPORAL) != null && bb != null)
            {
                bb.setTemporalInfo(
                    parseTemporalInfo(boxObject.getJSONObject(BinderBox.TEMPORAL))
                );
            }
            
            register(bb);
        }
    }
    
    private BinderBox parseCsvBox(String name, JSONObject boxObject) throws BinderPoolException, ParseException
    {
        //Check for type inference
        boolean typeInference = boxObject.has(CsvBox.TYPE_INFERENCE)
            && boxObject.get(CsvBox.TYPE_INFERENCE) != null
            && boxObject.get(CsvBox.TYPE_INFERENCE) instanceof Boolean
            && boxObject.getBoolean(CsvBox.TYPE_INFERENCE);
        
        return new CsvBox(
            name,
            parseCSVBinder(boxObject.getJSONObject(BinderBox.BINDER)),
            typeInference);
    }

    private BinderBox parseJdbcBox(String name, JSONObject boxObject) throws BinderPoolException, ParseException
    {
        boolean withTable = boxObject.has(TableBox.TABLE)
            && boxObject.get(TableBox.TABLE) != null
            && boxObject.get(TableBox.TABLE) instanceof String;
        
        boolean withSql = boxObject.has(SqlBox.SQL)
            && boxObject.get(SqlBox.SQL) != null
            && boxObject.get(SqlBox.SQL) instanceof String;
        
        if(withTable && withSql)
            throw new BinderPoolException("Formatting error for binder '" + name 
                + "'. A JDBC binder cannot have both " 
                + TableBox.TABLE 
                + " and " 
                + SqlBox.SQL + " fields.");
        
        JDBCBinder binder = getJDBCBinder(boxObject.getJSONObject(BinderBox.BINDER));
        
        if(withSql)
            return new SqlBox(
                name,
                binder,
                boxObject.getString(SqlBox.SQL));
        else if(withTable)
            return new TableBox(
                name,
                binder,
                boxObject.getString(TableBox.TABLE));
        else
            return new JdbcBox(
                name,
                binder);
    }
    
    private CSVBinder parseCSVBinder(JSONObject binderObject) throws ParseException
    {
        return new CSVBindingFactory()
        .buildFromFeatures(
            convertToFeatureMap(binderObject.toMap()
        ));
    }
            
    private JDBCBinder getJDBCBinder(JSONObject binderObject) throws ParseException
    {
        JDBCBinder jdbcBinder = new JDBCBinderFactory()
            .buildFromFeatures(
                convertToFeatureMap(binderObject.toMap())
            );

        return jdbcBinder;
    }
    
    private FeatureMap convertToFeatureMap(Map<String, Object> mapping)
    {
        FeatureMap fm = new FeatureMap();
        
        for(String key: mapping.keySet())
        {
            if(mapping.get(key) instanceof Map)
                fm.addFeature(key, convertToFeatureMap((Map) mapping.get(key)));
            else if (mapping.get(key) instanceof List)
                fm.addFeature(key, convertList((List) mapping.get(key)));
            else
                fm.addFeature(key, mapping.get(key));
        }
        
        return fm;
    }
    
    private List<FeatureMap> convertList(List list)
    {
        List featureList = new ArrayList();
        
        for(int i=0; i<list.size(); i++)
        {
            if(list.get(i) instanceof Map)
                featureList.add(convertToFeatureMap((Map) list.get(i)));
            else if (list.get(i) instanceof List)
                featureList.add(convertList((List) list.get(i)));
            else
                featureList.add(list.get(i));
        }
        
        return featureList;
    }   

    private Temporal parseTemporalInfo(JSONObject tObject) throws ParseException 
    {
        String tAttribute   = tObject.getString(Temporal.TIME);
        String tContract    = tObject.getString(Temporal.CONTRACTOR);
        String pAttribute   = tObject.has(Temporal.PARTITION)
            ? tObject.getString(Temporal.PARTITION)
            : null;
        
        if(tAttribute == null)
            throw new ParseException("Temporal info has no time attribute.");
        
        if(tContract == null)
            throw new ParseException("Temporal info has no time contractor.");
        
        if(!SigmaContractorUtil
            .getOrdinalContractors()
            .anyMatch(oc -> oc.name().equals(tContract)))
            throw new ParseException("Unknown contractor name '" + tContract + "'.");
        
        return new Temporal(
            tAttribute,
            SigmaContractorUtil
                .getOrdinalContractors()
                .filter(oc -> oc
                    .name()
                    .equals(tContract))
                .findFirst()
                .get(),
            pAttribute);
    }
}
