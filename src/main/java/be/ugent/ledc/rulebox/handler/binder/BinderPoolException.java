package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.LedcException;

public class BinderPoolException extends LedcException
{
    public BinderPoolException(){}

    public BinderPoolException(String message)
    {
        super(message);
    }

    public BinderPoolException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BinderPoolException(Throwable cause)
    {
        super(cause);
    }

    public BinderPoolException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
