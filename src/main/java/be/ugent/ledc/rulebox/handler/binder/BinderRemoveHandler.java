package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import java.io.IOException;

public class BinderRemoveHandler extends BinderHandler
{
    private final Flag matchFlag = new Flag
    (
        "match",
        "--m",
        "When activated, each binder of which the name contains the given name, will be removed."
    );
    
    public BinderRemoveHandler()
    {
        super("delete", "Removes the binder with the given name from the binder pool.");
        addFlag(matchFlag);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            String name = ParameterTools.extractParameter(getBinderNameParameter(), args);
            boolean match = ParameterTools.extractFlag(matchFlag, args);
            
            BinderPool pool = BinderPool.getInstance();
            
            int count = 0;
            
            for(String n: pool.listNames())
            {
                if(n.equals(name) || (match && n.contains(name)))
                {
                    System.out.println("Removing binder " + n);
                    BinderPool.getInstance().delete(n);
                    count++;
                }
            }
            
            if(count == 0)
            {
                System.out.println("Could not remove any binders");
            }
            else
            {
                System.out.println("Removed " + count + " binders.");
                BinderPool.getInstance().dump();
            }
        }
        catch (LedcException | IOException ex)
        {
            System.err.println(ex.getMessage());
        }
    }

}
