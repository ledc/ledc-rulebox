package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorUtil;
import java.io.IOException;

public class BinderTemporalHandler extends BinderHandler
{
    private final Parameter<String> timeAttributeParameter = new Parameter<>(
        "time",
        "--t",
        true,    
        (s) -> s,
        "Attribute that contains the time variable."
    );
    
    private final Parameter<String> timeContractorParameter = new Parameter<>(
        "time-contractor",
        "--tc",
        true,    
        (s) -> s,
        "Name of the contractor to be used for the time attribute."
    )
    .addVerifier(
        name -> SigmaContractorUtil
            .getOrdinalContractors()
            .anyMatch(oc -> oc.name().equals(name)),
        "Unknown contractor name given for parameter index-contractor."
        );
    
    private final Parameter<String> partitionAttributeParameter = new Parameter<>(
        "partition-by",
        "--pb",
        false,    
        (s) -> s,
        "Attribute to use to make partitions."
    );
    
    
    public BinderTemporalHandler()
    {
        super("temporal", "Declare this binder to point to a temporal dataset.");
        
        addParameter(timeAttributeParameter);
        addParameter(timeContractorParameter);
        addParameter(partitionAttributeParameter);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            String name = ParameterTools.extractParameter(getBinderNameParameter(), args);
            String time = ParameterTools.extractParameter(timeAttributeParameter, args);
            String part = ParameterTools.extractParameter(partitionAttributeParameter, args);
            String cont = ParameterTools.extractParameter(timeContractorParameter, args);
            
            if(part != null && time.equals(part))
            {
                System.err.println("Time and partition attribute cannot be equal.");
            }
            
            if(!BinderPool.getInstance().exists(name))
            {
                System.err.println("Binder '" + name + "' does not exist. "
                    + " Create a binder first and then make it temporal.");
            }
            else
            {
                Temporal t = new Temporal(
                    time,
                    SigmaContractorUtil
                    .getOrdinalContractors()
                    .filter(oc -> oc
                        .name()
                        .equals(cont))
                    .findFirst()
                    .get(), 
                    part);
                
                BinderPool
                    .getInstance()
                    .getBinderBox(name)
                    .setTemporalInfo(t);
                
                BinderPool
                    .getInstance()
                    .dump();
            }
        }
        catch (LedcException | IOException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
