package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.csv.CSVBinder;
import be.ugent.ledc.core.binding.csv.CSVProperties;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.rulebox.Config;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CsvAddHandler extends BinderAddHandler
{
    private final Parameter<File> fileParameter = new Parameter<>(
        "file",
        "--fi",
        true,
        (s) -> new File(s),
        "The path to the csv file."
    )
    .addVerifier(
        f -> f.getName().toLowerCase().endsWith(".csv"),
        "Must bind to a csv file.");
    
    private final Parameter<String> separatorParameter = new Parameter<>(
        "separator",
        "--s",
        true,
        (s) -> s,
        "The separator symbol to be used."
    );
    
    private final Parameter<Character> quoteParameter = new Parameter<>(
        "quote-symbol",
        "--qs",
        false,
        (s) -> s == null || s.isEmpty() ? null : s.charAt(0),
        "The quote symbol to be used. By default, no quote symbols are used."
    );
    
    private final Parameter<String> commentParameter = new Parameter<>(
        "comment-symbol",
        "--cs",
        false,
        (s) -> s,
        "The comment symbol to be used. Lines starting with this symbol are considered as comment."
    );
    
    private final Flag noHeaderFlag = new Flag(
        "no-header",
        "--nh",
        "Use this flag if the first line in the csv file is not a header line."
    );
    
    private final Flag typeInferenceFlag = new Flag(
        "type-inference",
        "--ti",
        "Use this flag if you want datatypes to be recognized in the data."
    );
    
    private final Parameter<Set<String>> nullSymbolsParameter = new OptionalParameter<>(
        "null-symbols",
        "--ns",
        (s) -> Stream
            .of(s.split(","))
            .map(String::trim)
            .collect(Collectors.toSet()),
        "Symbols that represent null-values (comma-separated list). A default null symbol is registered in the config file.",
        SetOperations.set(Config.NULL_SYMBOL)
    );
    
    public CsvAddHandler()
    {
        super("csv", "Adds a CSV-based binder to the binder pool.");
        
        addParameter(fileParameter);
        addParameter(commentParameter);
        addParameter(nullSymbolsParameter);
        addParameter(quoteParameter);
        addParameter(separatorParameter);
        addFlag(noHeaderFlag);
        addFlag(typeInferenceFlag);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            String name   = ParameterTools.extractParameter(getBinderNameParameter(), args);
            boolean force = isForced(args);
            
            if(BinderPool.getInstance().exists(name) && !force)
            {
                System.out.println("Binder '" + name + "' already exists. Use "
                    + getForceFlag().getName()
                    + " to overwrite.");
            }
            else
            {
                CsvBox box = new CsvBox(
                    ParameterTools.extractParameter(getBinderNameParameter(), args),
                    new CSVBinder(
                        new CSVProperties(
                            !ParameterTools.extractFlag(noHeaderFlag, args),
                            ParameterTools.extractParameter(separatorParameter, args),
                            ParameterTools.extractParameter(quoteParameter, args),
                            ParameterTools.extractParameter(nullSymbolsParameter, args),
                            ParameterTools.extractParameter(commentParameter, args),
                            Charset.forName(getEncoding(args))),
                        ParameterTools.extractParameter(fileParameter, args)),
                    ParameterTools.extractFlag(typeInferenceFlag, args)
                );
                
                BinderPool
                    .getInstance()
                    .overwrite(box);
                
                BinderPool
                    .getInstance()
                    .dump();
            }
        }
        catch (LedcException | IOException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
