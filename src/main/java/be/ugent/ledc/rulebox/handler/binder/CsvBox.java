package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.csv.CSVBinder;
import be.ugent.ledc.core.binding.csv.CSVBindingFactory;
import be.ugent.ledc.core.binding.csv.CSVDataReader;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.binding.jdbc.schema.DiagrammerOptions;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.rulebox.Config;
import static be.ugent.ledc.rulebox.handler.binder.BinderBox.BINDER;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CsvBox extends BinderBox<CSVBinder>
{
    public static final String TYPE_INFERENCE = "TypeInference";
    
    private final CSVBinder binder;
    
    private final boolean typeInference;
    
    public CsvBox(String name, CSVBinder binder, boolean typeInference)
    {
        super(name, BinderBox.TYPE_CSV);
        this.binder = binder;
        this.typeInference = typeInference;
    }
    
    public CsvBox(String alias, CSVBinder binder)
    {
        this(alias, binder, false);
    }

    @Override
    public ContractedDataset data() throws FileNotFoundException, DataReadException
    {       
        CSVDataReader reader = new CSVDataReader(binder);
        
        ContractedDataset data = typeInference
            ? reader.readDataWithTypeInference(Config.TYPE_INFERENCE_SAMPLE_SIZE)
            : reader.readData();
        
        try
        {
            reader.close();
        }
        catch (IOException ex)
        {
            throw new DataReadException(ex);
        }
        
        return data;
    }

    @Override
    public DatabaseSchema schema(DiagrammerOptions options) throws BindingException
    {
        throw new BindingException("Database schema are currently only supported for JDBC-binded data sources.");
    }
    
    @Override
    public CSVBinder bind()
    {
        return binder;
    }
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        return super
            .buildFeatureMap()
            .addFeature(TYPE_INFERENCE, typeInference)
            .addFeature(BINDER, new CSVBindingFactory()
                .buildFeatureMap(binder)
                .getFeatures()
            );
    }
    
    @Override
    public String toString()
    {
        return super.toString()
            + "\n -> " + binder.getCsvFile().getAbsolutePath()
            + " ["
            + "header=" + binder.getCsvProperties().isFirstLineIsHeader() + "|"
            + "separator=" + binder.getCsvProperties().getColumnSeparator() + "|"
            + "quote=" + binder.getCsvProperties().getQuoteCharacter()+ "|"
            + "null=" + binder.getCsvProperties().getNullSymbols()+ "|"
            + "comment=" + binder.getCsvProperties().getCommentSymbol()+ "|"
            + "typing=" + typeInference + "|"
            + "encoding=" + binder.getCsvProperties().getEncoding().name()
            + "]";
    }
}
