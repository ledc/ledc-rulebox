package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.jdbc.DBMS;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.RelationalDB;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.io.ConsoleUtil;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

public class JdbcAddHandler extends BinderAddHandler
{
    private final Parameter<String> hostParameter = new Parameter<>(
        "host",
        "--h",
        true,
        (s) -> s,
        "The name of the host where the database is located."
    );
    
    private final Parameter<String> userParameter = new Parameter<>(
        "user",
        "--u",
        true,
        (s) -> s,
        "The name of the user that will connect to the database."
    );
    
    private final Parameter<String> dbParameter = new Parameter<>(
        "database",
        "--db",
        true,
        (s) -> s,
        "The name of the database to connect with."
    );
    
    private final Parameter<Integer> portParameter = new OptionalParameter<>(
        "port",
        "--p",
        (s) -> Integer.valueOf(s),
        "The port number to which the database can be reached. Default value is 5432.",
        5432
    );
    
    private final Parameter<String> schemaParameter = new OptionalParameter<>(
        "schema",
        "--s",
        (s) -> s,
        "The name of the schema to connect with. Default value is public",
        "public"
    );
    
    private final Parameter<String> vendorParameter = new OptionalParameter<>(
        "vendor",
        "--v",
        (s) -> s,
        "The name of the DBMS. Default value is " + DBMS.POSTGRESQL.name(),
        DBMS.POSTGRESQL.name()
    );
    
    private final Parameter<String> passwordParameter = new Parameter<>(
        "password",
        "--pw",
        false,
        (s) -> s,
        "The password of the user that will connect to the database. "
        + "When specified, the password is stored in the binder pool file. "
        + "If not given, the password will be requested at connection time."
    );
    
    private final Parameter<String> sqlParameter = new Parameter<>(
        "sql-statement",
        "--sql",
        false,
        (s) -> s,
        "The SQL statement that will be used to load data. Use this if the data to work with is fixed."
    );
    
    private final Parameter<String> tableParameter = new Parameter<>(
        "table",
        "--t",
        false,
        (s) -> s,
        "When specified, data will be loaded via a 'select * from <table>' statement. "
        + "This parameter is ignored when "
        + sqlParameter.getName()
        + " is specified."
    );
    
    private final Flag allTablesFlag = new Flag(
        "all-tables",
        "--at",
        "When activated, a binder entry is added for every table in the specified schema. "
        + "The names of these binders are a concatenation of the given binder name the name of the table. "
        + "This parameter is ignored when either "
        + sqlParameter.getName()
        + " or "
        + tableParameter.getName()
        + " are specified."
    );
    
    public JdbcAddHandler()
    {
        super("jdbc", "Adds a JDBC-based binder to the binder pool.");
        
        addParameter(dbParameter);
        addParameter(hostParameter);
        addParameter(userParameter);
        addParameter(portParameter);
        addParameter(schemaParameter);
        addParameter(vendorParameter);
        addParameter(passwordParameter);
        addParameter(sqlParameter);
        addParameter(tableParameter);
        addFlag(allTablesFlag);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            String name   = ParameterTools.extractParameter(getBinderNameParameter(), args);
            boolean force = isForced(args);
            
            List<BinderBox> boxes = create(args);
            
            for(BinderBox box: boxes)
            {
                if(BinderPool.getInstance().exists(name) && !force)
                {
                    System.out.println("Binder '" + name + "' already exists. Use "
                        + getForceFlag().getName()
                        + " to overwrite.");
                }
                else
                {
                    BinderPool
                        .getInstance()
                        .overwrite(box);
                }
            }
            
            BinderPool.getInstance().dump();
        }
        catch (IOException | LedcException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    public List<BinderBox> create(String[] args) throws ParameterException, BindingException
    {
        String sql  = ParameterTools.extractParameter(sqlParameter, args);
        String table = ParameterTools.extractParameter(tableParameter, args);
        boolean allTables = ParameterTools.extractFlag(allTablesFlag, args);

        RelationalDB rdb = new RelationalDB(
            DBMS.valueOf(ParameterTools.extractParameter(vendorParameter, args)),
            ParameterTools.extractParameter(hostParameter, args),
            ParameterTools.extractParameter(portParameter, args).toString(),
            ParameterTools.extractParameter(userParameter, args),
            ParameterTools.extractParameter(passwordParameter, args),
            ParameterTools.extractParameter(dbParameter, args),
            ParameterTools.extractParameter(schemaParameter, args)
        );

        Charset encoding = Charset.forName(getEncoding(args));

        String name = ParameterTools.extractParameter(getBinderNameParameter(), args);

        //If sql statement given, return a sql box.
        if(sql != null)
        {
            return ListOperations.list(
                new SqlBox(name, new JDBCBinder(rdb,encoding), sql)
            );
        }
        //Else if a table name is given, return a table box
        else if(table != null)
        {
            return ListOperations.list(
                new TableBox(name, new JDBCBinder(rdb,encoding), table)
            );
        }
        //Else, if the all tables flag is on, make a binder for each table
        else if(allTables)
        {
            //Check if password is provided
            if (rdb.getPassword() == null)
            {
                System.out.println("Provide binder password:");
                rdb.setPassword(ConsoleUtil.readPassword());
            }
            
            return new JDBCBinder(rdb,encoding)
                .getSchema()
                .keySet()
                .stream()
                .map(t -> new TableBox(
                    name.concat(Rubix.SEPARATOR).concat(t),
                    new JDBCBinder(rdb,encoding),
                    t))
                .collect(Collectors.toList());
        }
        else
        {
            return ListOperations.list(
                new JdbcBox(name, new JDBCBinder(rdb,encoding))
            );
        }
    }
    
}
