package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.JDBCBinderFactory;
import be.ugent.ledc.core.binding.jdbc.JDBCDataReader;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.binding.jdbc.schema.DiagrammerOptions;
import be.ugent.ledc.core.binding.jdbc.schema.generator.Schematizer;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.rulebox.io.ConsoleUtil;
import java.io.FileNotFoundException;
import java.sql.SQLException;

public class JdbcBox extends BinderBox<JDBCBinder>
{
    private final JDBCBinder binder;
    
    public JdbcBox(String name, JDBCBinder binder)
    {
        super(name, BinderBox.TYPE_JDBC);
        this.binder = binder;
    }
    
    protected JdbcBox(String name, JDBCBinder binder, String type)
    {
        super(name, type);
        this.binder = binder;
    }

    @Override
    public JDBCBinder bind()
    {
        return binder;
    }

    @Override
    public ContractedDataset data() throws FileNotFoundException, DataReadException
    {
        if (binder.getRdb().getPassword() == null)
        {
            System.out.println("Provide binder password:");
            binder
                .getRdb()
                .setPassword(ConsoleUtil.readPassword());
        }
        
        JDBCDataReader reader = new JDBCDataReader(binder);
        
        System.out.println("SQL query:");
        ContractedDataset data = reader.readContractedData(ConsoleUtil.readLine());
        try
        {
            reader.closeConnection();
        }
        catch (SQLException ex)
        {
            throw new DataReadException(ex);
        }
        finally
        {
            //Make sure we never store this password.
            binder
                .getRdb()
                .setPassword(null);
        }
        
        return data;
    }

    @Override
    public DatabaseSchema schema(DiagrammerOptions options) throws BindingException
    {
        if (binder.getRdb().getPassword() == null)
        {
            System.out.println("Provide binder password:");
            binder
                .getRdb()
                .setPassword(ConsoleUtil.readPassword());
        }
        Schematizer schematizer = new Schematizer();
        schematizer.setAddRowCounts(options.isShowRowCount());
        schematizer.addFilter(options.createFilter());
        return schematizer.schematize(binder.getRdb());
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return super
            .buildFeatureMap()
            .addFeature(BINDER, new JDBCBinderFactory()
                .buildFeatureMap(binder)
                .getFeatures()
            );
    }

    @Override
    public String toString()
    {
        return super.toString()
            + "\n -> " + binder.getRdb().getConnectionString();
    }
}
