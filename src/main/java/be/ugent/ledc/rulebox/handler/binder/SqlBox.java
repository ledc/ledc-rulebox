package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.JDBCDataReader;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.dataset.ContractedDataset;
import java.io.FileNotFoundException;
import java.sql.SQLException;

public class SqlBox extends JdbcBox
{
    public static final String SQL = "Sql";
    
    private final String sql;
    
    public SqlBox(String alias, JDBCBinder binder, String sql)
    {
        super(alias, binder, BinderBox.TYPE_JDBC_SQL);
        this.sql = sql;
    }

    @Override
    public ContractedDataset data() throws FileNotFoundException, DataReadException
    {
        JDBCDataReader reader = new JDBCDataReader(bind());
        
        ContractedDataset data = reader.readContractedData(sql);
        
        try
        {
            reader.closeConnection();
        }
        catch (SQLException ex)
        {
            throw new DataReadException(ex);
        }
        
        return data;
    }
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        return super
            .buildFeatureMap()
            .addFeature(SQL, sql);
    }
    
    @Override
    public String toString()
    {
        return super
            .toString()
            .concat("\n -> " + sql);
    }

    public String getSql()
    {
        return sql;
    }
}
