package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.JDBCDataReader;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.dataset.ContractedDataset;
import java.io.FileNotFoundException;
import java.sql.SQLException;

public class TableBox extends JdbcBox
{
    public static final String TABLE = "Table";
    
    private final String table;
    
    public TableBox(String alias, JDBCBinder binder, String table)
    {
        super(alias, binder, BinderBox.TYPE_JDBC_TABLE);
        this.table = table;
    }

    @Override
    public ContractedDataset data() throws FileNotFoundException, DataReadException
    {
        JDBCDataReader reader = new JDBCDataReader(bind());
        
        ContractedDataset data = reader.readContractedData("select * from " + reader
            .getBinder()
            .getRdb()
            .getVendor()
            .getAgent()
            .escaping()
            .apply(table)
            + ";");
        
        try
        {
            reader.closeConnection();
        }
        catch (SQLException ex)
        {
            throw new DataReadException(ex);
        }
        
        return data;
    }
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        return super
            .buildFeatureMap()
            .addFeature(TABLE, table);
    }
    
    @Override
    public String toString()
    {
        return super.toString().concat("\n -> select * from " + table + ";");
    }

    public String getTable()
    {
        return table;
    }
}
