package be.ugent.ledc.rulebox.handler.binder;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.util.Objects;

public class Temporal<I extends Comparable<? super I>> implements Persistable
{
    private final String timeAttribute;
    
    private final OrdinalContractor<I> contractor;
    
    private final String partitionAttribute;
    
    public static final String TIME         = "TimeAttribute";
    public static final String CONTRACTOR   = "TimeContractor";
    public static final String PARTITION    = "PartitionAttribute";

    public Temporal(String timeAttribute, OrdinalContractor<I> contractor, String partitionAttribute)
    {
        this.timeAttribute = timeAttribute;
        this.contractor = contractor;
        this.partitionAttribute = partitionAttribute;
    }
    
    public Temporal(String indexAttribute, OrdinalContractor<I> contractor)
    {
        this(indexAttribute, contractor, null);
    }

    public String getTimeAttribute()
    {
        return timeAttribute;
    }

    public OrdinalContractor getContractor()
    {
        return contractor;
    }

    public String getPartitionAttribute()
    {
        return partitionAttribute;
    }

    public boolean isPartioned()
    {
        return this.partitionAttribute != null;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.timeAttribute);
        hash = 47 * hash + Objects.hashCode(this.contractor);
        hash = 47 * hash + Objects.hashCode(this.partitionAttribute);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Temporal<?> other = (Temporal<?>) obj;
        if (!Objects.equals(this.timeAttribute, other.timeAttribute))
        {
            return false;
        }
        if (!Objects.equals(this.partitionAttribute, other.partitionAttribute))
        {
            return false;
        }
        if (!Objects.equals(this.contractor, other.contractor))
        {
            return false;
        }
        return true;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(TIME, timeAttribute)
            .addFeature(CONTRACTOR, contractor.name())
            .addFeature(PARTITION, partitionAttribute);
    }

    
}
