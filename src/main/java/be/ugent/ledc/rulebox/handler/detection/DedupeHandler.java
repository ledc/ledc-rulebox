package be.ugent.ledc.rulebox.handler.detection;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.operators.aggregation.BasicAggregatorFactory;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.hbos.OrdinalHistogram;
import be.ugent.ledc.match.AbstractDeduplicator;
import static be.ugent.ledc.match.AbstractDeduplicator.DUPLICATE_CLUSTER;
import be.ugent.ledc.match.BruteForceDeduplicator;
import be.ugent.ledc.match.Deduplicator;
import be.ugent.ledc.match.SortDeduplicator;
import be.ugent.ledc.match.closure.Grouper;
import be.ugent.ledc.match.closure.SingleLinkageGrouper;
import be.ugent.ledc.match.matcher.dataobject.DataObjectMatcher;
import be.ugent.ledc.match.matcher.dataobject.FellegiSunterMatcher;
import be.ugent.ledc.match.matcher.dataobject.MinimaxMatcher;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.DataConstraintHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.util.IdHandler;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.HistogramPlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import be.ugent.ledc.rulebox.io.result.table.TableContainer;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import be.ugent.ledc.rulebox.io.result.table.ViolationContainer;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DedupeHandler extends DataConstraintHandler
{
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (file) -> file.getName().endsWith(".html") || file.getName().endsWith(".xml"),
            "Outputfile must be an .html or .xml file");
    
    private final Parameter<List<String>> sortKeyParameter = new Parameter<>(
        "sort-key",
        "--sk",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .toList(),
        "When specified, deduplication with linkage rules will use the Sorted Neighborhood algorithm rather than a brute force algorithm."
    );
    
    /**
     * Parameter used to pass the source attribute. Tuples with the same value 
     * for this attribute are not compared
     */
    private final Parameter<String> sourceAttributeParameter = new Parameter<>(
        "source-attribute",
        "--sa",
        false,
        s->s,
        "An attribute that the source of a tuple. Tuples with equal values for this source attribute, are not compared." 
    );
    
    /**
     * Parameter used to pass the source attribute. Tuples with the same value 
     * for this attribute are not compared
     */
    private final Parameter<String> evaluateParameter = new Parameter<>(
        "evaluate-by",
        "--e",
        false,
        s->s,
        "A group attribute that holds the actual duplicates in the dataset and evaluates the linkage in terms of precision, recall and F1-score." 
    );
    
    private final Parameter<Integer> linkageWindowParameter = new OptionalParameter<>(
        "linkage-window",
        "--lw",
        (s) -> Integer.valueOf(s),
        "If deduplication is done with Sorted Neighborhood algorithm, this window is used to select candidate tuple pairs. Default is 10.",
        10
    ).addVerifier(w -> w >=2, "Linkage window must be larger then 2.");

    private final IdHandler idHandler = new IdHandler();
    
    public DedupeHandler() {
        super("dedupe", "Searches duplicates in a dataset based on a linkage rule.");
        
        addParameter(outputFileParameter);
        addParameter(sortKeyParameter);
        addParameter(evaluateParameter);
        addParameter(linkageWindowParameter);
        addParameter(sourceAttributeParameter);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);

            System.out.println("Reading constraints");
            Rubix rubix = RubixReader.readRubix(ParameterTools.extractParameter(getConstraintFileParameter(), args));
            
            String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(binder);
                        
            List<Container<?>> containers = new ArrayList<>();
            
            PlotContainer violationsContainer = new PlotContainer(
                "Duplicates overview",
                idHandler.next(),
                2);
            
            ContractedDataset evalResults = new ContractedDataset(new Contract
                .ContractBuilder()
                .addContractor("measure", TypeContractorFactory.STRING)
                .addContractor("result", TypeContractorFactory.BIGDECIMAL)
                .build()
            );

            Set<String> selection = new HashSet<>(data.getContract().getAttributes());
            selection.add(AbstractDeduplicator.DUPLICATE_CLUSTER);
            
            //Copy the contract
            Contract.ContractBuilder builder = new Contract.ContractBuilder(data.getContract());
            
            builder.addContractor(
                AbstractDeduplicator.DUPLICATE_CLUSTER,
                TypeContractorFactory.INTEGER
            );
            
            ContractedDataset rowsWithErrors = new ContractedDataset(builder.build());
            
            Map<DataObject, Set<Violation>> violations = new HashMap<>();
            
            //Count object occurences
            Map<DataObject, Long> objectCounts = data
                .stream()
                .collect(Collectors.groupingBy(o -> o, Collectors.counting()));
            
            //We are only interested in objects appearing more than once.
            objectCounts
                .keySet()
                .removeIf(o -> objectCounts.get(o) == 1);

            System.out.println("Processing linkage rules ...");

            //Fetch matcher from rubix
            DataObjectMatcher<?, ?> matcher = rubix.getMatcher();

            if(matcher != null)
            {
                //Extract additional parameters
                List<String> sortKey = ParameterTools.extractParameter(sortKeyParameter, args);
                String source = ParameterTools.extractParameter(sourceAttributeParameter, args);
                String eval = ParameterTools.extractParameter(evaluateParameter, args);
                Integer window = ParameterTools.extractParameter(linkageWindowParameter, args);

                if(sortKey != null && !sortKey.isEmpty())
                {
                    boolean unknownAttribute = sortKey
                        .stream()
                        .anyMatch(a -> !data
                            .getContract()
                            .getAttributes()
                            .contains(a));

                    if(unknownAttribute)
                    {
                        System.err.println("Unknown sort key attribute in: " + sortKey);
                        System.exit(-1);
                    }
                }
                
                if(source != null && !data.getContract().getAttributes().contains(source))
                {
                    System.err.println("Unknown source attribute: " + source);
                    System.exit(-1);
                }
                
                if(eval != null && !data.getContract().getAttributes().contains(eval))
                {
                    System.err.println("Unknown evaluation attribute: " + source);
                    System.exit(-1);
                }

                Deduplicator deduper = sortKey == null || sortKey.isEmpty()
                    ? new BruteForceDeduplicator(matcher, source)
                    : new SortDeduplicator(
                        sortKey,
                        window,
                        matcher,
                        source
                    );

                Comparable threshold = matcher instanceof FellegiSunterMatcher
                    ? 0.0
                    : ((MinimaxMatcher)matcher).cutoff();

                System.out.println("Computing matches...");

                Map<Couple<DataObject>, Comparable> matchCouples = deduper.linkage(
                    data,
                    threshold);

                System.out.println("Computing closure...");

                //Closure computation
                Grouper<DataObject> grouper = new SingleLinkageGrouper<>();
                
                List<Set<DataObject>> duplicates = grouper.group(matchCouples
                    .keySet()
                    .stream()
                    .toList());
                
                if(eval != null)
                {
                    //The number of duplicate pairs that were found correctly
                    int dupsFound = 0;

                    //The number of actual duplicates present
                    int dups = 0;
                    
                    for(int i=0; i<data.getDataObjects().size(); i++)
                    {
                        DataObject o1 = data.getDataObjects().get(i);
                        
                        for(int j=i+1; j<data.getDataObjects().size(); j++)
                        {
                            DataObject o2 = data.getDataObjects().get(j);
                            
                            if(source != null && Objects.equals(o1.get(source), o2.get(source)))
                                continue;
                            
                            //If o1 and o2 are in the same group: increase the number of true duplicates
                            if(Objects.equals(o1.get(eval), o2.get(eval)))
                            {
                                dups++;
                                
                                //Check if some cluster contains both o1 and o2
                                if(duplicates.stream().anyMatch(cluster -> cluster.contains(o1) && cluster.contains(o2)))
                                {
                                    dupsFound++;
                                }
                            }
                        }
                    }
                    
                    //The number of pairs found after closure
                    int found = duplicates
                        .stream()
                        .mapToInt(cluster -> (cluster.size() * (cluster.size() - 1)) / 2)
                        .sum();
                    
                    double p = (double)dupsFound / (double)found;
                    double r = (double)dupsFound / (double)dups;
                    double f = BasicAggregatorFactory
                        .createHarmonicMeanDoubleAggregator()
                        .aggregate(p,r);
                    
                    evalResults.addDataObject(new DataObject()
                        .setString("measure", "Precision")
                        .setBigDecimal("result", new BigDecimal(p).setScale(3, RoundingMode.CEILING))
                    );
                    
                    evalResults.addDataObject(new DataObject()
                        .setString("measure", "Recall")
                        .setBigDecimal("result", new BigDecimal(r).setScale(3, RoundingMode.CEILING))
                    );
                    
                    evalResults.addDataObject(new DataObject()
                        .setString("measure", "F1-value")
                        .setBigDecimal("result", new BigDecimal(f).setScale(3, RoundingMode.CEILING))
                    );
                }

                Map<DataObject, Set<DataObject>> dViolations = new HashMap<>();

                for(int g=0; g<duplicates.size(); g++)
                {
                    List<DataObject> group = new ArrayList<>(duplicates.get(g));

                    for(int i=0; i<group.size(); i++)
                    {
                        DataObject o = group.get(i);

                        DataObject key = new DataObject(o);
                        key.setInteger(DUPLICATE_CLUSTER, g);

                        dViolations.put(key, new HashSet<>());

                        for(int j=0; j<group.size(); j++)
                        {
                            if(i == j)
                                continue;

                            DataObject oo = group.get(j);

                            if(matchCouples.containsKey(new Couple(o, oo)))
                            {
                                double belief= convert(matchCouples.get(new Couple(o, oo)));
                                dViolations
                                    .get(key)
                                    .add(oo
                                        .project(matcher.getMatchAttributes())
                                        .set("belief", belief)
                                    );
                            }
                            else if(matchCouples.containsKey(new Couple(oo, o)))
                            {
                                double belief=convert(matchCouples.get(new Couple(oo, o)));
                                dViolations
                                    .get(key)
                                    .add(oo
                                        .project(matcher.getMatchAttributes())
                                        .set("belief", belief)
                                    );
                            }
                        }
                    }
                }

                if(matchCouples.isEmpty())
                {
                    System.out.println("No duplicates found...");
                }
                else
                {
                    for(DataObject o: dViolations.keySet())
                    {
                        rowsWithErrors.addDataObject(o);

                        Set<Violation> lViolations = dViolations
                            .get(o)
                            .stream()
                            .map(rule -> new Violation(
                                "duplicates",
                                o.toString(),
                                SetOperations.set(AbstractDeduplicator.DUPLICATE_CLUSTER)))
                            .collect(Collectors.toSet());

                        violations.merge(
                            o.project(selection),
                            lViolations,
                            SetOperations::union);
                    }

                    ContractedDataset counts = new ContractedDataset(new Contract
                        .ContractBuilder()
                        .addContractor("s", TypeContractorFactory.INTEGER)
                        .build());

                    int bins = 0;

                    for(Set<DataObject> set: duplicates)
                    {
                        bins = Math.max(bins, set.size());

                        counts.addDataObject(new DataObject().set("s", set.size()));
                    }

                    OrdinalHistogram<Integer> histogram = new OrdinalHistogram<>(
                        SigmaContractorFactory.INTEGER,
                        IntStream
                            .rangeClosed(2, bins)
                            .boxed()
                            .collect(Collectors.toMap(i->Interval.closed(i,i), i-> 0l))
                    );
                    histogram.fill(
                        counts,
                        "s",
                        SigmaContractorFactory.INTEGER);

                    //Produce histogram
                    violationsContainer.addItem(
                        new HistogramPlot(
                            histogram,
                            "Distribution of duplicate group sizes (#matches=" + matchCouples.size() + ")",
                            true,
                            true
                        )
                    );
                }

                containers.add(violationsContainer);
                
                if(evalResults.getSize() > 0)
                {
                    containers.add(new TableContainer(
                        evalResults,
                        "Evaluation",
                        idHandler.next(),
                        ListOperations.list("measure","result")
                    ));
                }

                final ContractedDataset sorted = new ContractedDataset(rowsWithErrors.getContract());

                rowsWithErrors
                    .sort(DataObject.getProjectionComparator(AbstractDeduplicator.DUPLICATE_CLUSTER))
                    .forEach(o -> sorted.addDataObject(o));

                containers.add(new ViolationContainer
                (
                    sorted,
                    violations,
                    "Duplicate rows",
                    idHandler.next(),
                    objectCounts
                ));
            
                //Dump containers in output
                if(!containers.isEmpty())
                {
                    OutputFactory.dump(
                        containers,
                        ParameterTools.extractParameter(outputFileParameter, args)
                    );
                }
            }
        }
        catch (LedcException | MarshallException | IOException  ex)
        {
            System.err.println(ex.getMessage());
        }
    }

    private double convert(Comparable c) throws LedcException
    {
        if(c instanceof Number)
        {
            return ((Number)c).doubleValue();
        }
        else if(c instanceof UnitScore)
        {
            return ((UnitScore)c).getValue();
        }
        else
            throw new LedcException("Unknown outcome type of DataObjectMatcher.");
    }
}
