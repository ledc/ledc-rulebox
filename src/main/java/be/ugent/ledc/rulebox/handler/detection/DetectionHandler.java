package be.ugent.ledc.rulebox.handler.detection;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.core.datastructures.histogram.CategoricalHistogram;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.detection.Detector;
import be.ugent.ledc.fundy.algorithms.detection.FDViolation;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.util.LexicographicComparator;
import be.ugent.ledc.indy.algorithms.detection.INDDetector;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.assertion.AbstractAssertor;
import be.ugent.ledc.pi.assertion.AndAssertor;
import be.ugent.ledc.pi.assertion.ContainmentAssertor;
import be.ugent.ledc.pi.assertion.InstanceAssertor;
import be.ugent.ledc.pi.assertion.OrAssertor;
import be.ugent.ledc.pi.assertion.PropertyAssertor;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.rulebox.Config;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.DataConstraintHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.util.IdHandler;
import be.ugent.ledc.rulebox.handler.util.StringUtil;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.DualBarPlot;
import be.ugent.ledc.rulebox.io.result.plot.HistogramPlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import be.ugent.ledc.rulebox.io.result.table.TableContainer;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import be.ugent.ledc.rulebox.io.result.table.ViolationContainer;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DetectionHandler extends DataConstraintHandler
{
    private final Parameter<Set<String>> onlyOnParameter = new Parameter<>(
        "only",
        "--o",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Show errors only for these attributes (comma-separated list)"
    );
    
    private final Parameter<Set<String>> excludeParameter = new Parameter<>(
        "exclude",
        "--e",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Don't show errors for these attributes (comma-separated list)"
    );
    
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (file) -> file.getName().endsWith(".html") || file.getName().endsWith(".xml"),
            "Outputfile must be an .html or .xml file");
    
    private final Flag selectionRulesFlag = new Flag
    (
        "selection-rules",
        "--sr",
        "Includes selection rules in the detection process"
    );
    
    private final Flag functionalDependenciesFlag = new Flag
    (
        "functional-dependencies",
        "--fd",
        "Includes functional dependencies in the detection process"
    );
    
    private final Flag propertyAssertionsFlag = new Flag
    (
        "property-assertions",
        "--pa",
        "Includes property assertions in the detection process"
    );
    
    private final Flag inclusionDependenciesFlag = new Flag
    (
        "inclusion-dependencies",
        "--ind",
        "Includes inclusion dependencies in the detection process"
    );
        
    private final Flag allFlag = new Flag
    (
        "all",
        "--a",
        "Includes all available constraints in the detection process"
    );
    
    private final Flag rowsFlag = new Flag
    (
        "rows",
        "--r",
        "Displays rows with errors"
    );
    
    private final Flag orAssertionsFlag = new Flag(
        "or-assertions",
        "--oa",
        "Combine multiple assertions for the same attribute in a disjunctive way: only one of them needs to pass."
    );

    private final Flag andAssertionsFlag = new Flag(
        "and-assertions",
        "--aa",
        "Combine multiple assertions for the same attribute in a conjunctive way: all of them need to pass."
    );
    
    
    
    private final Flag verboseFlag = new Flag
    (
        "verbose",
        "--v",
        "Verbose output. Includes more detailed information on detected errors in the output. Relevant for functional dependencies and assertions."
    );
    
    private final IdHandler idHandler = new IdHandler();
    
    public DetectionHandler()
    {
        super("detect", "Detects violations of constraints in a dataset and builds a detection report.");
        
        addParameter(onlyOnParameter);
        addParameter(excludeParameter);
        addParameter(outputFileParameter);
        
        addFlag(allFlag);
        addFlag(selectionRulesFlag);
        addFlag(functionalDependenciesFlag);
        addFlag(propertyAssertionsFlag);
        addFlag(inclusionDependenciesFlag);
        
        addFlag(rowsFlag);
        addFlag(verboseFlag);
        
        addFlag(orAssertionsFlag);
        addFlag(andAssertionsFlag);
        
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            Config.loadRegistry();
            
            boolean all = ParameterTools.extractFlag(allFlag, args);
            
            boolean isr     = ParameterTools.extractFlag(selectionRulesFlag, args);
            boolean ifd     = ParameterTools.extractFlag(functionalDependenciesFlag, args);
            boolean iind    = ParameterTools.extractFlag(inclusionDependenciesFlag, args);
            boolean ipa     = ParameterTools.extractFlag(propertyAssertionsFlag, args);
            
            boolean rows    = ParameterTools.extractFlag(rowsFlag, args);

            System.out.println("Reading constraints");
            Rubix rubix = RubixReader.readRubix(ParameterTools.extractParameter(getConstraintFileParameter(), args));
            
            String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(binder);
            
            final Set<String> selection = data
                .getContract()
                .getAttributes();
            
            Set<String> onlyOn  = ParameterTools.extractParameter(onlyOnParameter, args);
            Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);

            if(onlyOn != null)
                selection.retainAll(onlyOn);
            if(exclude != null)
                selection.removeAll(exclude);
            
            List<Container<?>> containers = new ArrayList<>();
            
            PlotContainer violationsContainer = new PlotContainer(
                "Constraint violation overview",
                idHandler.next(),
                2);
            
            PlotContainer paDistributionsContainer = new PlotContainer(
                "Quality distributions of property assertions",
                idHandler.next(), 
                3);
            
            List<TableContainer> fdViolationsContainers = new ArrayList<>();
            
            //Copy the contract
            Contract.ContractBuilder builder = new Contract.ContractBuilder(data.getContract());
            
            ContractedDataset rowsWithErrors = new ContractedDataset(builder.build());
            
            Map<DataObject, Set<Violation>> violations = new HashMap<>();
            
            //Count object occurences
            Map<DataObject, Long> objectCounts = data
                .stream()
                .collect(Collectors.groupingBy(o -> o, Collectors.counting()));
            
            //We are only interested in objects appearing more than once.
            objectCounts
                .keySet()
                .removeIf(o -> objectCounts.get(o) == 1);
            
            //Check if selection rules need to be inspected
            if(all || isr)
            {
                System.out.println("Processing violations of selection rules...");
                
                Map<DataObject, Set<SigmaRule>> srErrors = selectionRuleDetection(selection, rubix, data, binder);
                
                if(srErrors.isEmpty())
                {
                    System.out.println("No violations of selection rules found...");
                }
                else
                {
                    //Produce histogram
                    violationsContainer.addItem(
                        new HistogramPlot(
                            buildHistogram(
                                srErrors,
                                objectCounts,
                                SigmaRule::toString
                            ),
                            "Violations of selection rules",
                            true,
                            true
                        )
                    );

                    if(rows)
                    {
                        handleRowsWithErrors(
                            rowsWithErrors,
                            srErrors,
                            violations,
                            selection,
                            "selection-rule",
                            SigmaRule::toString,
                            SigmaRule::getInvolvedAttributes
                        );
                    }
                }
            }
            
            //Check if property assertions need to be inspected
            if(all || ipa)
            {
                System.out.println("Processing violations of property assertions...");
                
                boolean or  = ParameterTools.extractFlag(orAssertionsFlag, args);
                boolean and = ParameterTools.extractFlag(andAssertionsFlag, args);
                
                List<PropertyAssertor> assertions = initAssertions(rubix, selection, and, or, binder);
                
                Map<DataObject, Set<PropertyAssertor>> paErrors = propertyAssertionDetection(
                    assertions,
                    data
                );

                if(paErrors.isEmpty())
                {
                    System.out.println("No violations of property assertions found...");
                }
                else
                {
                    //Produce histogram
                    violationsContainer.addItem(
                        new HistogramPlot(
                            buildHistogram(
                                paErrors,
                                objectCounts,
                                PropertyAssertor::toString
                            ),
                            "Violations of property assertions",
                            true,
                            true
                        )
                    );

                    if(rows)
                    {
                        handleRowsWithErrors(
                            rowsWithErrors,
                            paErrors,
                            violations,
                            selection,
                            "property-assertion",
                            PropertyAssertor::toString,
                            (pa) -> SetOperations.set(pa.getAttribute())
                        );
                    }
                    
                    if(ParameterTools.extractFlag(verboseFlag, args))
                    {
                        Map<PropertyAssertor, Map<Integer, Long>> distributionMap = new HashMap<>();

                        for(PropertyAssertor pa: assertions)
                        {
                            //Initialize a quality distribution based on the scale
                            distributionMap.put(
                                pa,
                                IntStream
                                    .rangeClosed(0, pa.scale())
                                    .mapToObj(i -> i)
                                    .collect(Collectors.toMap(i->i, i->0l))
                            );
                        }

                        for(DataObject o: paErrors.keySet())
                        {
                            for(PropertyAssertor pa: assertions)
                            {
                                //If the assertors is in error for this object
                                //increase the quality level with one
                                if(paErrors.get(o).contains(pa))
                                {
                                    distributionMap
                                        .get(pa)
                                        .merge(
                                            pa.quality(o),
                                            1L,
                                            Long::sum
                                    );
                                }
                            }
                        }

                        for(PropertyAssertor pa: assertions)
                        {
                            long perfect = data.getSize() - distributionMap
                                .get(pa)
                                .values()
                                .stream()
                                .mapToLong(l -> l)
                                .sum();

                            if(perfect > 0L)
                            {
                                distributionMap
                                    .get(pa)
                                    .put(pa.scale(), perfect);
                            }

                            //Add a histogram
                            paDistributionsContainer
                                .addItem(
                                    new HistogramPlot(
                                        new CategoricalHistogram<>(distributionMap
                                            .get(pa)
                                            .entrySet()
                                            .stream()
                                            .collect(Collectors.toMap(
                                                e -> new Interval<>(e.getKey(),e.getKey(),false,false),
                                                e -> e.getValue()
                                            ))),
                                        pa.toString(),
                                        true,
                                        false)
                                );
                        }
                    }
                }
            }
            
            //Check if inclusion dependencies need to be inspected
            if(all || iind)
            {
                System.out.println("Processing violations of inclusion dependencies...");
                
                Map<DataObject, Set<IND>> indErrors = inclusionDependencyDetection(selection, rubix, data, binder);
                
                //A mapper to represent an IND as string
                Function<IND, String> mapper = ind -> ind.toString();

                if(indErrors.isEmpty())
                {
                    System.out.println("No violations of inclusion dependencies found...");
                }
                else
                {
                    //Produce histogram
                    violationsContainer.addItem(
                        new HistogramPlot(
                            buildHistogram(
                                indErrors,
                                objectCounts,
                                mapper
                            ),
                            "Violations of inclusion dependencies",
                            true,
                            true
                        )
                    );

                    if(rows)
                    {
                        handleRowsWithErrors(
                            rowsWithErrors,
                            indErrors,
                            violations,
                            selection,
                            "inclusion-dependencies",
                            mapper,
                            IND::getAttributesToTest
                        );
                    }
                }
            }
            
            //Check if functional dependencies need to be inspected
            if(all || ifd)
            {
                System.out.println("Processing violations of functional dependencies...");

                RuleSet<Dataset,FD> selectedFDs = rubix
                        .getFunctionalDependencies(binder)
                        .project(selection);
                
                //Violations of FDs
                Map<FD, Set<FDViolation>> fdViolations = Detector
                    .detectErrors(
                        data,
                        selectedFDs
                    );

                if(fdViolations.isEmpty())
                {
                    System.out.println("No violations of functional dependencies found...");
                }
                else
                {
                    CategoricalHistogram<String> fdHistogram = new CategoricalHistogram<>(fdViolations
                        .entrySet()
                        .stream()
                        .collect(Collectors.toMap(
                            e -> new Interval<>(
                                e.getKey().toString(),
                                e.getKey().toString(),
                                false,
                                false
                            ),
                            e -> (long)e.getValue().size()))
                    );
                    
                    //Produce histogram
                    violationsContainer.addItem(
                        new HistogramPlot(
                            fdHistogram,
                            "Violations of functional dependencies",
                            true,
                            true
                        )
                    );

                    if(rows)
                    {
                         handleRowsWithErrors(
                            rowsWithErrors,
                            Detector.buildViolationMap(data, selectedFDs),
                            violations,
                            selection,
                            "functional-dependencies",
                            FD::toString,
                            FD::getInvolvedAttributes
                        );
                    }
                    
                    if(ParameterTools.extractFlag(verboseFlag, args))
                    {
                        Map<String, Couple<Long>> strongCFViolations = new HashMap<>();
                        Map<String, Couple<Long>> weakCFViolations = new HashMap<>();
                        
                        for(FD fd: fdViolations.keySet())
                        {   
                            ContractedDataset violationDistr = new ContractedDataset(new Contract
                                .ContractBuilder()
                                .addContractor("LHS", TypeContractorFactory.STRING)
                                .addContractor("RHS distribution", TypeContractorFactory.STRING)
                                .build()
                            );
                            
                            List<String> aList = new ArrayList<>(fd.getLeftHandSide());
                            
                            List<FDViolation> sortedViolations = fdViolations
                                .get(fd)
                                .stream()
                                .sorted(Comparator.comparing(
                                        FDViolation::getLhsValue,
                                        DataObject.getProjectionComparator(aList)))
                                .collect(Collectors.toList());
                            
                            for(FDViolation violation: sortedViolations)
                            {
                                violationDistr.addDataObject(
                                    new DataObject(true)
                                        .setString("LHS", valueToString(violation.getLhsValue()))
                                        .setString("RHS distribution", violation
                                            .getRhsValues()
                                            .entrySet()
                                            .stream()
                                            .map(e -> valueToString(e.getKey())
                                                .concat(": ")
                                                .concat(Long.toString(e.getValue())))
                                            .collect(Collectors.joining(", "))
                                        )
                                );
                            }
                            
                            fdViolationsContainers.add(
                                new TableContainer(
                                    violationDistr,
                                    "Violations of " + fd.toString(),
                                    idHandler.next(),
                                    ListOperations.list("LHS", "RHS distribution"),
                                    false));
                            
                            strongCFViolations.put(
                                fd.toString(),
                                new Couple<>(
                                    (long)fdViolations.get(fd).size(), //Violation count
                                    fdViolations                       //Strong conflict-free count
                                        .get(fd)
                                        .stream()
                                        .filter(v -> v.isStrongConflictFree())
                                        .count())
                                );
                            
                            weakCFViolations.put(
                                fd.toString(),
                                new Couple<>(
                                    (long)fdViolations.get(fd).size(), //Violation count
                                    fdViolations                       //Weak conflict-free count
                                        .get(fd)
                                        .stream()
                                        .filter(v -> v.isWeakConflictFree())
                                        .count())
                                );
                        }
                        
                        //Add histograms
                        violationsContainer.addItem(
                            new DualBarPlot<>(
                                strongCFViolations,
                                "Strong conflict-free FD violations",
                                true,
                                "Violations",
                                "Strong conflict-free violations"
                            )
                        );
                        
                        violationsContainer.addItem(
                            new DualBarPlot<>(
                                weakCFViolations,
                                "Weak conflict-free FD violations",
                                true,
                                "Violations",
                                "Weak conflict-free violations"
                            )
                        );
                    }
                }
            }
            
            containers.add(violationsContainer);
            
            if(!paDistributionsContainer.isEmpty())
            {
                containers.add(paDistributionsContainer);
            }

            if(rows)
            {
                List<String> sortKey = rubix
                    .getFunctionalDependencies(binder)
                    .stream()
                    .sorted((fd1,fd2) -> new LexicographicComparator<String>()
                        .compare(
                            fd1.getLeftHandSide().stream().sorted().collect(Collectors.toList()),
                            fd2.getLeftHandSide().stream().sorted().collect(Collectors.toList())
                        ))
                    .map(fd -> fd.getLeftHandSide())
                    .distinct()
                    .flatMap(lhs -> lhs.stream())
                    .distinct()
                    .collect(Collectors.toList());
                
                final ContractedDataset sorted = new ContractedDataset(rowsWithErrors.getContract());
                
                if(sortKey.isEmpty())
                        rowsWithErrors
                        .stream()
                        .forEach(o -> sorted.addDataObject(o));
                    else
                    rowsWithErrors
                        .sort(DataObject.getProjectionComparator(sortKey))
                        .forEach(o -> sorted.addDataObject(o));
                
                containers.add(new ViolationContainer
                (
                    sorted,
                    violations,
                    "Tuples with violations (n=" + sorted.getSize()+")",
                    idHandler.next(),
                    objectCounts
                ));
            }
            
            if(!fdViolationsContainers.isEmpty())
            {
                containers.addAll(fdViolationsContainers);
            }
            
            //Dump containers in output
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers,
                    ParameterTools.extractParameter(outputFileParameter, args)
                );
            }
        }

        catch (LedcException | MarshallException | IOException | URISyntaxException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    private Map<DataObject, Set<SigmaRule>> selectionRuleDetection(Set<String> selection, Rubix rubix, ContractedDataset data, String binder)
    {
        //Select relevant sigma rules
        SigmaRuleset targetSigmaRules = rubix
            .getSigmaRules(binder)
            .project(selection);
      
        Map<DataObject, Set<SigmaRule>> failedRules = new HashMap<>();

        for(DataObject o: data)
        {
            //Check sigma rules
            Set<SigmaRule> failingRules = targetSigmaRules.failingRules(o);
            
            if(!failingRules.isEmpty())
            {
                failedRules.put(o, failingRules);
            }
        }

        return failedRules;
    }
    
    private Map<DataObject, Set<IND>> inclusionDependencyDetection(Set<String> selection, Rubix rubix, ContractedDataset data, String binder) throws FileNotFoundException, LedcException 
    {
        //Build ruleset of INDS
        INDRuleset indRuleset = rubix.getINDRuleset(binder, true)
            .leftProject(selection);

        Map<IND, Set<DataObject>> violations = INDDetector.detectErrors(
            data,
            indRuleset);
        
        Map<DataObject, Set<IND>> inverseMap = new HashMap<>();
        
        for(IND ind: violations.keySet())
        {
            for(DataObject o: violations.get(ind))
            {
                inverseMap.merge(
                    o,
                    SetOperations.set(ind),
                    SetOperations::union);
            }
        }
        
        return inverseMap;        
    }
    
    private Map<DataObject, Set<PropertyAssertor>> propertyAssertionDetection(List<PropertyAssertor> assertions, ContractedDataset data) throws PiException
    {
        Map<DataObject, Set<PropertyAssertor>> failedAssertions = new HashMap<>();

        for(PropertyAssertor pa: assertions)
        {
            for (DataObject o : data)
            {
                if(pa.quality(o) < pa.scale())
                {                    
                    failedAssertions.merge(
                        o,  //For object o
                        SetOperations.set(pa), //register assertion that failed
                        SetOperations::union); //union with existing failures, if any
                }
            }
        }
        

        return failedAssertions;
    }
    
    private List<PropertyAssertor> initAssertions(Rubix rubix, Set<String> selection, boolean and, boolean or, String binder) throws DependencyGraphException, PropertyParseException
    {
        List<PropertyAssertor> listOfAssertors = new ArrayList<>();
        
        if(rubix.getPropertyAssertions(binder).getRules().isEmpty())
        {
            return listOfAssertors;
        }
        
        //Collect assertors that cannot be validated
        Set<PropertyAssertor> unvalidatable = rubix
            .getPropertyAssertions(binder)
            .getRules()
            .stream()
            .filter(aa -> !aa.canValidate())
            .collect(Collectors.toSet());
        
        //Report
        for(PropertyAssertor aa: unvalidatable)
        {
            if (aa instanceof InstanceAssertor)
            {
                System.out.println("Cannot verify assertion " + aa.toString());
                System.out.println(StringUtil.indent("Cause: no measure found in local registry folder", 1));
                System.out.println(StringUtil.indent("Solution: add a quality measure manually or try 'server fetch'", 1));
            }
            else if (aa instanceof ContainmentAssertor)
            {
                System.out.println("Cannot verify assertion " + aa.toString());
                System.out.println(StringUtil.indent("Cause: no scanner found in local registry folder", 1));
                System.out.println(StringUtil.indent("Solution: add a scanner manually or try 'server fetch'", 1));
            }
        }
        
        //Collect assertors that can be validated
        List<AbstractAssertor> validatable = rubix
            .getPropertyAssertions(binder)
            .getRules()
            .stream()
            .filter(PropertyAssertor::canValidate)
            .collect(Collectors.toList());
        
        for (String a : selection)
        {
            //Find all assertions that can be validated related to attribute a
            Set<AbstractAssertor> assertors = validatable
                .stream()
                .filter(ass -> ass.getAttribute().equals(a))
                .collect(Collectors.toSet());
            
            //Add them
            assertors
                .stream()
                .forEach(listOfAssertors::add);

            if(assertors.size() > 1)
            {
                if(or)
                    listOfAssertors.add(new OrAssertor(assertors, a));
                
                if(and)
                    listOfAssertors.add(new AndAssertor(assertors, a));
            }
        }
        
        return listOfAssertors;

    }

    private <T> Histogram<String> buildHistogram(Map<DataObject, Set<T>> violations, Map<DataObject, Long> objectCounts, Function<T,String> mapper)
    {
        Map<String, Long> counts = new HashMap<>();
            
        for(DataObject o: violations.keySet())
        {
            long oCount = objectCounts.get(o) == null
                ? 1
                : objectCounts.get(o);
            
            for(T constraint: violations.get(o))
            {
                counts.merge(mapper.apply(constraint), oCount, Long::sum);
            }
        }
        
        return new CategoricalHistogram<>(counts
            .entrySet()
            .stream()
            .collect(Collectors.toMap(
                e -> new Interval<>(e.getKey(),e.getKey(),false,false),
                e -> e.getValue()
            )));
    }

    private <T> void handleRowsWithErrors(ContractedDataset rowsWithErrors, Map<DataObject, Set<T>> violations, Map<DataObject, Set<Violation>> violationMap, Set<String> selection, String type, Function<T, String> stringMapper, Function<T, Set<String>> attributeMapper)
    {
        for(DataObject o: violations.keySet())
        {
            rowsWithErrors.addDataObject(o.project(selection));

            Set<Violation> lViolations = violations
                .get(o)
                .stream()
                .map(rule -> new Violation(
                    type,
                    stringMapper.apply(rule),
                    attributeMapper.apply(rule)))
                .collect(Collectors.toSet());

            violationMap.merge(
                o.project(selection),
                lViolations,
                SetOperations::union);
        }
    }    

    private String valueToString(DataObject o)
    {
        if(o.getAttributes().size() != 1)
            return o.toString();
        
        String a = o.getAttributes().stream().findFirst().get();
        
        return o.get(a) == null
            ? Config.NULL_SYMBOL
            : o.get(a).toString();
    }
}
