package be.ugent.ledc.rulebox.handler.explore;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.binding.jdbc.schema.Diagrammer;
import be.ugent.ledc.core.binding.jdbc.schema.DiagrammerOptions;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import java.io.File;
import java.io.IOException;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DiagramHandler extends DataHandler
{
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (f) -> f.getName().endsWith(".html"),
            "Output file must have extension .html"
        );
    
    private final Flag checkConstraintsFlag = new Flag(
        "check-constraints", 
        "--cc",
        "Adds information about check constraints to the diagram");
    
    private final Flag invertFilterFlag = new Flag(
        "invert-filter", 
        "--if",
        "Inverts the selection of tables made by the name-filter parameter");
    
    private final Flag rowCountFlag = new Flag(
        "row-count", 
        "--rc",
        "Show table sizes (row counts) for each table.");
    
    private final Flag excludeViewsFlag = new Flag(
        "exclude-views", 
        "--ev",
        "Exclude views from the diagram.");
    
    private final Parameter<String> nameFilterParameter = new OptionalParameter<>(
        "name-filter",
        "--nf",
        Function.identity(),
        "Only tables with a name that matches this regex pattern will be shown in the diagram",
        ".+");
    
    public DiagramHandler()
    {
        super("schema", "draws a diagram of the schema of a JDBC data source using mermaid.");
        addParameter(outputFileParameter);
        addParameter(nameFilterParameter);
        addFlag(checkConstraintsFlag);
        addFlag(invertFilterFlag);
        addFlag(rowCountFlag);
        addFlag(excludeViewsFlag);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            //Read schema
            System.out.println("Building schema...");
            
            DiagrammerOptions options = new DiagrammerOptions(
                ParameterTools.extractFlag(checkConstraintsFlag, args),
                ParameterTools.extractFlag(rowCountFlag, args),
                ParameterTools.extractParameter(nameFilterParameter, args),
                ParameterTools.extractFlag(invertFilterFlag, args),
                ParameterTools.extractFlag(excludeViewsFlag, args)
            );
            
            DatabaseSchema schema = BinderPool
                .getInstance()
                .schema(ParameterTools.extractParameter(getBinderNameParameter(), args), options);
        
            System.out.println("Writing to output file...");
            Diagrammer.buildDiagram(
                schema,
                ParameterTools.extractParameter(outputFileParameter, args),
                options
            );
            
            if(ParameterTools.extractFlag(rowCountFlag, args))
            {
                for(TableSchema ts: schema.getTableSchemas())
                {
                    System.out.println(ts.getName()
                        + ": "
                        + ts.getRowCount()
                        + " rows"
                    );
                }
            }
        }
        catch (ParameterException | BindingException | BinderPoolException ex)
        {
            System.err.println(ex.getMessage());
            System.exit(-1);
        }
        catch (IOException ex)
        {
            Logger.getLogger(DiagramHandler.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
}
