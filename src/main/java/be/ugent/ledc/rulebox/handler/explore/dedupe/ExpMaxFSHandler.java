package be.ugent.ledc.rulebox.handler.explore.dedupe;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.operators.quantifier.AtLeastQuantifier;
import be.ugent.ledc.core.operators.quantifier.BasicQuantifier;
import be.ugent.ledc.core.operators.quantifier.Quantifier;
import be.ugent.ledc.core.operators.quantifier.QuantifierFactory;
import be.ugent.ledc.match.matcher.CutoffMatcher;
import be.ugent.ledc.match.matcher.dataobject.DataObjectMatcher;
import be.ugent.ledc.match.matcher.dataobject.FellegiSunterFactory;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.InterpretationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Unsupervised learning of a Fellegi-Sunter linkage rule with the EM algorithm.
 * @author abronsel
 */
public class ExpMaxFSHandler extends LearnHandler
{
    /**
     * Parameter used to pass the source attribute. Tuples with the same value 
     * for this attribute are not compared
     */
    private final Parameter<String> sourceAttributeParameter = new Parameter<>(
        "source-attribute",
        "--sa",
        false,
        s->s,
        "An attribute that the source of a tuple. Tuples with equal values for this source attribute, are not compared." 
    );
    
    /**
     * Parameter used to pass the quantifier used in the initialization of EM.
     * The quantifier is a function Q: [0,1] -> [0,1] such that Q(x) gives 
     * the probability of a match when x is the proportion of attributes
     * for which there is a match. Options are: All, Most (Q(x):=x^4), Many (Q(x):= x^2) and At least q (Q(x):=1 if x >= q).
     * Quantifiers can be combined point-wise minimally by using '&' (e.g., Many & At least 0.8)
     */
    private final Parameter<String> quantifierParameter = new OptionalParameter<>(
        "quantifier",
        "--q",
        s->s,
        "A function Q where Q(x) gives the probability "
            + "of a match if x is portion of attributes for which there is a match. "
            + "Options are: All, Most, Many and At least q.",
            //+ "Point-wise minimal combinations can be made with '&' (e.g., Many & At least 0.8)." ,
        "All"
    ).addVerifier(
        s -> Stream
            .of(s.split("\\s*&\\s*"))
            .allMatch(q ->
                    q.equals("All")
                ||  q.equals("Most")
                ||  q.equals("Many")
                ||  q.matches("At least 0\\.\\d+")),
        "Invalid quantifier specification.");
    
    
    private final Flag strictInitFlag = new Flag(
        "strict-init",
        "--si",
        "When activated, the initial assignment considers two attribute value a match when equal."
        + " Else, equal or similar are acceptable for a match.");
    
    public ExpMaxFSHandler()
    {
        super("em", "Learns a Fellegi-Sunter linkage rule in an unsupervised way by using the EM algorithm.");
    
        addParameter(sourceAttributeParameter);
        addParameter(quantifierParameter);
        
        addFlag(strictInitFlag);
    }

    @Override
    public DataObjectMatcher<?, ?> learn(ContractedDataset data, DataObjectMatcher<?, ?> matcher, String[] args) throws ParameterException
    {
        Map<String, CutoffMatcher<Object>> matchers = matcher
                .getMatchAttributes()
                .stream()
                .collect(Collectors.toMap(
                        a -> a,
                        a -> (CutoffMatcher)matcher.getAttributeMatcher(a)
                ));
        
        String source = ParameterTools.extractParameter(sourceAttributeParameter, args);
        
        if(source != null && !data.getContract().getAttributes().contains(source))
            throw new ParameterException("Source attribute '" + source + "' must appear in the dataset.");
        
        Map<Couple<DataObject>, Integer> sample = new HashMap<>();
            
        for(int i=0; i<data.getSize(); i++)
        {
            DataObject o1 = data.getDataObjects().get(i);           

            for(int j=i+1; j<data.getSize(); j++)
            {
                DataObject o2 = data.getDataObjects().get(j);

                if(source != null
                && o1.get(source) != null
                && o2.get(source) != null
                && Objects.equals(o1.get(source),o2.get(source)))
                    continue;
                
                sample.merge(new Couple<>(o1,o2), 1, Integer::sum);

            }
        }
        
        
        try
        {
            Quantifier q = parseQuantifier(ParameterTools.extractParameter(quantifierParameter, args));
            
            return FellegiSunterFactory.createUnsupervised(
                matchers,
                sample,
                q,
                true);
            
        }
        catch (InterpretationException ex)
        {
            throw new ParameterException(ex);
        }
    }
    
    private Quantifier parseQuantifier(String s) throws InterpretationException
    {
        String[] parts = s.split("\\s*&\\s*");
        
        Quantifier[] quantifiers = new Quantifier[parts.length];
        
        for(int i=0; i<parts.length; i++)
        {
            String part = parts[i];
            
            if(part.equals("All"))
                quantifiers[i]=BasicQuantifier.ALL;
            if(part.equals("Most"))
                quantifiers[i]=BasicQuantifier.MOST;
            if(part.equals("Many"))
                quantifiers[i]=BasicQuantifier.MANY;
            if(part.matches("At least\\s+(0.\\d+)"))
                quantifiers[i]= new AtLeastQuantifier(
                    new UnitScore(new Grex("@1")
                        .resolveAsDouble("At least\\s+(0.\\d+)", part))
                );
        }

        return quantifiers.length == 1
            ? quantifiers[0]
            : QuantifierFactory.and(quantifiers);
    }
    
}
