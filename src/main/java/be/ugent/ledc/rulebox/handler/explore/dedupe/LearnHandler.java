package be.ugent.ledc.rulebox.handler.explore.dedupe;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.match.matcher.dataobject.DataObjectMatcher;
import be.ugent.ledc.match.matcher.dataobject.FellegiSunterFactory;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.DataConstraintHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import be.ugent.ledc.rulebox.io.rubix.RubixWriter;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Learns an optimal Fellegi-Sunter model based on a given dataset.
 * Implementations of this handler offer different methods for learning. 
 * @author abronsel
 */
public abstract class LearnHandler extends DataConstraintHandler
{
    /**
     * The file to which the learned linkage rules are written.
     */
    private final Parameter<File> outputFileParameter = ParameterFactory.createOutputFileParameter(true);
    
    /**
     * Verbose information for the learning process.
     */
    private final Parameter<Integer> verbosityParameter = new OptionalParameter<>(
        "verbose",
        "--v",
        Integer::valueOf,
        "Allows to tweak the output you get during the learning process",
        0);
    
    public LearnHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(outputFileParameter);
        addParameter(verbosityParameter);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
            
            ContractedDataset data = BinderPool
                .getInstance()
                .data(binder);
            
            //Read the input constraints
            File inputRbx = ParameterTools.extractParameter(getConstraintFileParameter(), args);
            Rubix rubix = RubixReader.readRubix(inputRbx);
            
            FellegiSunterFactory.VERBOSITY = ParameterTools.extractParameter(verbosityParameter, args);
            
            //Learn the linkage rules
            DataObjectMatcher<?, ?> matcher = learn(data, rubix.getMatcher(), args);
                        
            //Write the result
            System.out.println("Writing rubix file");
            Rubix modifiedRubix = new Rubix.RubixBuilder()
                .withSigmaRules(rubix.getAllSigmaRules())
                .withFunctionalDependencies(rubix.getAllFunctionalDependencies())
                .withPropertyAssertions(rubix.getAllPropertyAssertions())
                .withInclusionDependencies(rubix.getAllInclusionDependencies())
                .withSigmaCostFunctions(rubix.getCostFunctions())
                .withMatcher(matcher)
                .build();
            
            RubixWriter.writeRubix(
                modifiedRubix,
                ParameterTools.extractParameter(outputFileParameter, args));
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }

    public abstract DataObjectMatcher<?,?> learn(ContractedDataset data, DataObjectMatcher<?, ?> matcher, String[] args) throws ParameterException;
}
