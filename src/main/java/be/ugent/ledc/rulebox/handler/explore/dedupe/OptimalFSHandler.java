
package be.ugent.ledc.rulebox.handler.explore.dedupe;

import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.match.matcher.CutoffMatcher;
import be.ugent.ledc.match.matcher.dataobject.DataObjectMatcher;
import be.ugent.ledc.match.matcher.dataobject.FellegiSunterFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Learns the optimal Fellegi-Sunter linkage rule based on training data. The labels
 * are encoded via a group attribute. Tuples with the same value for this attribute
 * are considered duplicates.
 * @author abronsel
 */
public class OptimalFSHandler extends LearnHandler
{
    /**
     * Parameter used to pass the  group attribute. Tuples with the same value 
     * for this attribute are considered duplicates.
     */
    private final Parameter<String> groupAttributeParameter = new Parameter<>(
        "group-attribute",
        "--g",
        true,
        s->s,
        "The attribute that contains the group to which a tuple belongs. Tuples with equal values for this group attribute, are considered duplicates." 
    );
    
    /**
     * Parameter used to pass the source attribute. Tuples with the same value 
     * for this attribute are not compared
     */
    private final Parameter<String> sourceAttributeParameter = new Parameter<>(
        "source-attribute",
        "--sa",
        false,
        s->s,
        "An attribute that the source of a tuple. Tuples with equal values for this source attribute, are not compared." 
    );
    
    
    public OptimalFSHandler()
    {
        super("optimal", "Learns the optimal Fellegi-Sunter linkage rule based on training data.");
        
        addParameter(groupAttributeParameter);
        addParameter(sourceAttributeParameter);
    }

    @Override
    public DataObjectMatcher<?, ?> learn(ContractedDataset data, DataObjectMatcher<?, ?> matcher, String[] args) throws ParameterException
    {
        Map<String, CutoffMatcher<Object>> matchers = matcher
                .getMatchAttributes()
                .stream()
                .collect(Collectors.toMap(
                        a -> a,
                        a -> (CutoffMatcher)matcher.getAttributeMatcher(a)
                ));
        
        String group = ParameterTools.extractParameter(groupAttributeParameter, args);
        String source = ParameterTools.extractParameter(sourceAttributeParameter, args);

        if(!data.getContract().getAttributes().contains(group))
            throw new ParameterException("Group attribute '" + group + "' must appear in the dataset.");
        
        if(source != null && !data.getContract().getAttributes().contains(source))
            throw new ParameterException("Source attribute '" + source + "' must appear in the dataset.");
        
        if(source == null)
            return FellegiSunterFactory.createSupervised(
                matchers,
                data,
                group
            );
        else
        {
            Map<Couple<DataObject>, Pair<Boolean,Integer>> training = new HashMap<>();
            
            for(int i=0; i<data.getSize(); i++)
            {
                DataObject o1 = data.getDataObjects().get(i);           

                Object g1 = o1.get(group);
                Object s1 = o1.get(source);

                for(int j=i+1; j<data.getSize(); j++)
                {
                    DataObject o2 = data.getDataObjects().get(j);
                    
                    Object g2 = o2.get(group);
                    Object s2 = o2.get(source);
                    
                    if(Objects.equals(s1,s2))
                        continue;
                    
                    Couple<DataObject> c = new Couple<>(o1,o2);
                    
                    int count = training.containsKey(c)
                        ? training.get(c).getSecond() + 1
                        : 1;
                        
                    training.put(c, new Pair<>(g1.equals(g2), count));
                    
                }
            }
            
            return FellegiSunterFactory.createSupervised(matchers, training);
        }
    }
    
}
