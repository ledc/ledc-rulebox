package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.dino.outlierdetection.BruteForceNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.TreeNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.dbscan.DBScanDetector;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DBScanHandler extends MetricBasedOutlierHandler
{
    public static final String NAME = "dbscan";

    private final Parameter<Double> epsilon = new Parameter<>(
        "epsilon",
        "--eps",
        true,
        (String s) -> Double.valueOf(s),
        "The radius of the neighborhood of a point in the vector space.");
    
    private final Parameter<Integer> minPts = new Parameter<>(
        "min-points",
        "--mp",
        true,
        (String s) -> Integer.valueOf(s),
        "The minimal number of points required for a neighborhood to be considered dense.");
    
    public DBScanHandler()
    {
        super(NAME, "Finds outliers by application of the DB-SCAN cluster algorithm on all numerical attributes.\n"
                + "Distance between the vectors of numerical data is measured by the Euclidean distance.");
        addParameter(minPts);
        addParameter(epsilon);
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
            
            Contract contract = data.getContract();
            
            //Build selection
            Set<String> selection = super.buildSelection(
                args,
                data.getContract(),
                a -> isNumeric(contract.getAttributeContract(a)));
            
            //Cast
            data = data.asDouble(selection, TypeContractorFactory.DOUBLE);
            
            System.out.println("Searching for outliers with DB-SCAN detector");
            
            long time = System.nanoTime();
            Map<DataObject, Boolean> outlierScores = new DBScanDetector(
                !useKdTree(args) //Check if a KD tree is requested
                    ? new BruteForceNeighbourSearch(
                        selection,
                        data)
                    : new TreeNeighbourSearch<>(
                        selection,
                        data,
                        TypeContractorFactory.DOUBLE), 
                ParameterTools.extractParameter(epsilon, args),
                ParameterTools.extractParameter(minPts, args),
                DataObjectMetrics.FAST_EUCLIDEAN
            )
            .findOutliers(data, selection);
            time = (System.nanoTime() - time)/1000000l;
            
            System.out.println("Search time: " + time + " ms");            
            
            System.out.println("Found " + outlierScores.size() + " in the dataset.");
            
            if(mustOutput(args))
            {
                ContractedDataset outliers = new ContractedDataset(data.getContract());

                //Add outliers to output data
                outlierScores
                    .keySet()
                    .stream()
                    .forEach(outliers::addDataObject);

                //Call output
                super.output(args, outliers, new HashMap<>(), "DBScan");
            }
            
            if(mustRepair(args))
            {
                repair(args, data, outlierScores.keySet());
            }
        }
        catch (LedcException | SQLException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
