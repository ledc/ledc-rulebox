package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.hbos.HBOSDetector;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class HBOSHandler extends OutlierHandler
{
    private final Flag dynamicBinningFlag = new Flag(
        "dynamic-binning",
        "--db",
        "Uses dynamic binning to compute histograms.");
    
    private final Parameter<Double> thresholdParameter = new OptionalParameter<>(
        "threshold",
        "--th",
        (String s) -> Double.parseDouble(s),
        "Threshold to decide which objects are outliers. Default value is 2.0",
        2.0
    )
    .addVerifier(d -> d > 0.0, "Threshold must be a real, positive value");
    
    public static final String NAME = "hbos";
    
    public HBOSHandler()
    {
        super(NAME, "Finds outliers by running the HBOS algorithm.");
        addFlag(dynamicBinningFlag);
        addParameter(thresholdParameter);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
                   
            //Cast
            for(String a: data.getContract().getAttributes())
            {
                data = DataHandler.toSigmaContractor(data, a);
            }
            
            final Contract c = data.getContract();
            
            Set<String> decimals = data
                .getContract()
                .getAttributes()
                .stream()
                .filter(a -> !(c.getAttributeContract(a) instanceof OrdinalContractor))
                .filter(a -> isNumeric(c.getAttributeContract(a)))
                .collect(Collectors.toSet());
            
            //Cast
            data = data.asDouble(decimals, TypeContractorFactory.DOUBLE);
                
            final Contract cc = data.getContract();
            
            //Build selection
            Set<String> selection = super.buildSelection(
                args,
                cc,
                a -> (cc.getAttributeContract(a) instanceof OrdinalContractor)
                ||  (cc.getAttributeContract(a).equals(TypeContractorFactory.DOUBLE))
            );
                                  
            System.out.println("Searching for outliers with HBOS detector");
            long time = System.nanoTime();
            Map<DataObject, Double> outlierScores = new HBOSDetector
            (
                ParameterTools.extractParameter(thresholdParameter, args),
                ParameterTools.extractFlag(dynamicBinningFlag, args)
            ).findOutliers(data, selection);
            time = (System.nanoTime() - time)/1000000l;
            
            System.out.println("Search time: " + time + " ms");            
            System.out.println("Found " + outlierScores.size() + " in the dataset.");
            
            if(mustOutput(args))
            {
                ContractedDataset outliers = new ContractedDataset(data.getContract());

                //Add outliers to output data
                outlierScores
                    .entrySet()
                    .stream()
                    .sorted(Comparator.comparing(e -> e.getValue()))
                    .map(e -> e.getKey())
                    .forEach(outliers::addDataObject);
            

                //Call output
                super.output(
                    args,
                    outliers,
                    outlierScores
                        .entrySet()
                        .stream()
                        .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> SetOperations.set(new Violation("hbos-outlier", "HBOS-score: " + e.getValue(), selection))
                        )),
                    "HBOS");
            }
            
            if(mustRepair(args))
            {
                repair(args, data, outlierScores.keySet());
            }
        }
        catch (LedcException | FileNotFoundException | SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
}
