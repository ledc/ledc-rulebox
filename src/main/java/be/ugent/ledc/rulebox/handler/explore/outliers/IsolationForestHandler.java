package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.isolationforest.IsolationForestDetector;
import be.ugent.ledc.dino.outlierdetection.isolationforest.splitter.DoubleSplitter;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class IsolationForestHandler extends OutlierHandler
{
    public static final String NAME = "iforest";
   
    private final Parameter<Double> thresholdParameter = new OptionalParameter<>(
        "threshold",
        "--th",
        (String s) -> Double.valueOf(s),
        "Cutoff-value in the unit interval to decide which objects are outliers. Default value is 0.8",
        0.8
    )
    .addVerifier(d -> d >= 0.0, "Threshold must be a real value between 0 and 1")
    .addVerifier(d -> d <= 1.0, "Threshold must be a real value between 0 and 1");
    
    private final Parameter<Integer> treesParameter = new OptionalParameter<>(
        "trees",
        "--t",
        (String s) -> Integer.valueOf(s),
        "The number of trees in the forest. Default values is 100.",
        100
    );
    
    private final Parameter<Integer> sampleSizeParameter = new OptionalParameter<>(
        "sample",
        "--ss",
        (String s) -> Integer.valueOf(s),
        "The sample size to train individual trees with. Default values is 256 (or less if dataset has less objects).",
        256
    );
    
    public IsolationForestHandler()
    {
        super(NAME, "Finds outliers by learning an isolation forest. "
            + "The current implementation supports trees on numerical attributes only.");
        
        addParameter(treesParameter);
        addParameter(sampleSizeParameter);
        addParameter(thresholdParameter);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
            
            Contract contract = data.getContract();
            
            //Build selection
            Set<String> selection = super.buildSelection(
                args,
                data.getContract(),
                a -> isNumeric(contract.getAttributeContract(a)));
            
            //Cast
            data = data.asDouble(selection, TypeContractorFactory.DOUBLE);
            
            System.out.println("Searching for outliers with Isolation Forest detector");

            Double threshold = ParameterTools.extractParameter(thresholdParameter, args);
            Integer trees = ParameterTools.extractParameter(treesParameter, args);
            Integer sample = ParameterTools.extractParameter(sampleSizeParameter, args);
            
            long time = System.nanoTime();
            Map<DataObject, Double> outlierScores = new IsolationForestDetector<>(
                new DoubleSplitter(),
                threshold,
                trees,
                sample
            )
            .findOutliers(data, selection);
            time = (System.nanoTime() - time)/1000000l;
            
            System.out.println("Search time: " + time + " ms");            
            System.out.println("Found " + outlierScores.size() + " in the dataset.");
            
            if(mustOutput(args))
            {
                ContractedDataset outliers = new ContractedDataset(data.getContract());
                
                //Add outliers to output data
                outlierScores
                    .entrySet()
                    .stream()
                    .sorted(Comparator.comparing(e -> e.getValue()))
                    .map(e -> e.getKey())
                    .forEach(outliers::addDataObject);

                //Call output
                super.output(
                    args,
                    outliers,
                    outlierScores
                        .entrySet()
                        .stream()
                        .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> SetOperations.set(new Violation("isolation-forest-outlier", "Score: " + e.getValue(), selection))
                        )),
                    "Isolation Forest");
            }
            
            if(mustRepair(args))
            {
                repair(args, data, outlierScores.keySet());
            }

        }
        catch (ParameterException
            | BinderPoolException
            | DataWriteException
            | SQLException
            | BindingException ex)
        
        {
            System.err.println(ex.getMessage());
        }
        catch (IOException | DataReadException ex)
        {
            Logger.getLogger(IsolationForestHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
