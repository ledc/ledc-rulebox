package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.ParameterTools;

public abstract class MetricBasedOutlierHandler extends OutlierHandler
{
    private final Flag kdTreeFlag = new Flag(
        "kd-tree",
        "--kd",
        "Use a kD tree structure to support neighbourhood operations.");
    
    public MetricBasedOutlierHandler(String command, String description)
    {
        super(command, description);
        addFlag(kdTreeFlag);
    }
    
    protected boolean useKdTree(String[] args)
    {
        return ParameterTools.extractFlag(kdTreeFlag, args);
    }
    
}
