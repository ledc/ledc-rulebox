package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.BruteForceNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.TreeNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.knn.KNNDetector;
import be.ugent.ledc.dino.outlierdetection.knn.KNNProcessor;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class NearestNeighbourHandler extends MetricBasedOutlierHandler
{
    public static final String NAME = "knn";
    
    private final Parameter<Integer> neighboursParameter = new OptionalParameter<>(
        "k-neighbours",
        "--k",
        (String s) -> Integer.valueOf(s),
        "Amount of neighbours to consider. Default value is 3.",
        3
        
    );
    
    private final Parameter<Double> thresholdParameter = new Parameter<>(
        "threshold",
        "--th",
        true,
        (String s) -> Double.valueOf(s),
        "Cutoff-value to decide which objects are outliers."
    );
    
    private final Flag averageDistanceFlag = new Flag(
        "average",
        "--a",
        "When activated, the outlier score is computed as the average distance to neighbours instead of the maximum distance.");
    
    private final Flag verboseFlag = new Flag(
        "verbose",
        "--v",
        "Verbose output on the command line, like progress of the detector."
    );
    
    public NearestNeighbourHandler()
    {
        super(NAME, "Finds outliers by evaluation of the k nearest neighbours of an object.");
        
        addParameter(thresholdParameter);
        addParameter(neighboursParameter);
        addFlag(averageDistanceFlag);
        addFlag(verboseFlag);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
            
            Contract contract = data.getContract();
            
            //Build selection
            Set<String> selection = super.buildSelection(
                args,
                data.getContract(),
                a -> isNumeric(contract.getAttributeContract(a)));
            
            //Cast
            data = data.asDouble(selection, TypeContractorFactory.DOUBLE);
            
            System.out.println("Searching for outliers with k-NN detector");
            
            if(ParameterTools.extractFlag(verboseFlag, args))
                KNNDetector.SHOW_PROGRESS = true;
            
            long time = System.nanoTime();
            Map<DataObject, Double> outlierScores = new KNNDetector(
                !useKdTree(args) //Check if a KD tree is requested
                    ? new BruteForceNeighbourSearch(
                        selection,
                        data)
                    : new TreeNeighbourSearch<>(
                        selection,
                        data,
                        TypeContractorFactory.DOUBLE), 
                ParameterTools.extractParameter(neighboursParameter, args),
                ParameterTools.extractParameter(thresholdParameter, args),
                ParameterTools.extractFlag(averageDistanceFlag, args)
                    ? KNNProcessor.AVG
                    : KNNProcessor.MAX,
                DataObjectMetrics.FAST_EUCLIDEAN
            )
            .findOutliers(data, selection);
            time = (System.nanoTime() - time)/1000000l;
            
            System.out.println("Search time: " + time + " ms");            
            System.out.println("Found " + outlierScores.size() + " outliers in the dataset.");
            
            if(mustOutput(args))
            {
                ContractedDataset outliers = new ContractedDataset(data.getContract());

                //Add outliers to output data
                outlierScores
                    .entrySet()
                    .stream()
                    .sorted(Comparator.comparing(e -> e.getValue()))
                    .map(e -> e.getKey())
                    .forEach(outliers::addDataObject);

                //Call output
                super.output(
                    args,
                    outliers,
                    outlierScores
                        .entrySet()
                        .stream()
                        .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> SetOperations.set(new Violation("knn-outlier", "Score: " + e.getValue(), selection))
                        )),
                    "k-Nearest Neighbours");
            }
            
            if(mustRepair(args))
            {
                repair(args, data, outlierScores.keySet());
            }
        }
        catch (SQLException | LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
