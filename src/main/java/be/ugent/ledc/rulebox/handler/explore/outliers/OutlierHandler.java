package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.DataModifier;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import be.ugent.ledc.rulebox.io.result.table.ViolationContainer;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class OutlierHandler extends DataHandler implements DataModifier
{
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(false)
        .addVerifier(
            (file) -> file.getName().endsWith(".html") || file.getName().endsWith(".xml"),
            "Outputfile must be an .html or .xml file");
    
    private final Parameter<Set<String>> onlyOnParameter = new Parameter<>(
        "only",
        "--o",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Account only for these attributes when computing outliers (comma-separated list)"
    );
    
    private final Parameter<Set<String>> excludeParameter = new Parameter<>(
        "exclude",
        "--e",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Ignore these attributes when computing outliers (comma-separated list)"
    );
     
    public OutlierHandler(String command, String description)
    {
        super(command, description);
        addParameter(outputFileParameter);
        addParameter(repairBinderParameter);
        addParameter(onlyOnParameter);
        addParameter(excludeParameter);
        addFlag(softRepairFlag);
    }
    
    public boolean mustOutput(String[] args) throws ParameterException
    {
        return ParameterTools.extractParameter(outputFileParameter, args) != null;
    }
    
    public boolean mustRepair(String[] args) throws ParameterException
    {
        return ParameterTools.extractParameter(repairBinderParameter, args) != null;
    }
    
    public void output(String[] args, ContractedDataset outliers, Map<DataObject, Set<Violation>> violations, String method) throws ParameterException
    {
        try
        {
            File outputFile = ParameterTools.extractParameter(outputFileParameter, args);

            if(outputFile == null || outliers.getSize() == 0)
                return;
        
            List<Container<?>> containers = new ArrayList<>();
            
            containers.add(new ViolationContainer(
                outliers,
                violations,
                "Outliers detected with " + method,
                "1"));
            
            //Dump containers in output
            if(!containers.isEmpty())
            {
                OutputFactory.dump(containers, outputFile);
            }
        }
        catch (IOException | MarshallException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    public void repair(String[] args, ContractedDataset data, Set<DataObject> outliers) throws ParameterException, BinderPoolException, DataWriteException, BindingException, SQLException
    {
        ContractedDataset toRemove = new ContractedDataset(data.getContract());
        
        for(DataObject outlier: outliers)
            toRemove.addDataObject(outlier);
        
        removeRows(
            data,
            ParameterTools.extractParameter(getBinderNameParameter(), args), 
            toRemove, 
            ParameterTools.extractParameter(repairBinderParameter, args),
            ParameterTools.extractFlag(softRepairFlag, args)
        );
    }
    
    public Set<String> buildSelection(String[] args, Contract contract, Predicate<String> filter) throws ParameterException
    {
        final Set<String> selection = contract.getAttributes();
            
        Set<String> onlyOn  = ParameterTools.extractParameter(onlyOnParameter, args);
        Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);

        if(onlyOn != null)
            selection.retainAll(onlyOn);
        if(exclude != null)
            selection.removeAll(exclude);
        
        selection.removeIf(a -> !contract.getAttributes().contains(a));
        selection.removeIf(filter.negate());
        
        return selection;
    }
}
