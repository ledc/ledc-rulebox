package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.BruteForceNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.TreeNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.soda.SodaDetector;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class SodaHandler extends MetricBasedOutlierHandler
{
    public static final String NAME = "soda";

    private final Parameter<Integer> neighboursParameter = new OptionalParameter<>(
        "k-neighbours",
        "--k",
        (String s) -> Integer.valueOf(s),
        "Amount of neighbours to consider when computing Local Euclidian Manhattan Ratio (LEMR). Default value is 5",
        5
        
    );
    
    private final Parameter<Double> thresholdParameter = new OptionalParameter<>(
        "threshold",
        "--th",
        (String s) -> Double.valueOf(s),
        "Cutoff-value in the unit interval to decide which objects are outliers. Default value is 0.8",
        0.8
    )
    .addVerifier(d -> d < 0.0, "Threshold must be a real value between 0 and 1")
    .addVerifier(d -> d > 1.0, "Threshold must be a real value between 0 and 1");
    
    public SodaHandler()
    {
        super(NAME, "Finds outliers by application of SODA (Singular Outlier Detection Algorithm).\n"
            + "Computes outlier scores based on the Local Euclidian Manhattan Ratio (LEMR) w.r.t. the k nearest neighbours.");
        
        addParameter(thresholdParameter);
        addParameter(neighboursParameter);
        
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
            
            Contract contract = data.getContract();
            
            //Build selection
            Set<String> selection = super.buildSelection(
                args,
                data.getContract(),
                a -> isNumeric(contract.getAttributeContract(a)));
            
            //Cast
            data = data.asDouble(selection, TypeContractorFactory.DOUBLE);
            
            System.out.println("Searching for outliers with Soda detector");
            
            long time = System.nanoTime();
            Map<DataObject, Double> outlierScores = new SodaDetector(
                !useKdTree(args) //Check if a KD tree is requested
                    ? new BruteForceNeighbourSearch(
                        selection,
                        data)
                    : new TreeNeighbourSearch<>(
                        selection,
                        data,
                        TypeContractorFactory.DOUBLE), 
                ParameterTools.extractParameter(neighboursParameter, args),
                ParameterTools.extractParameter(thresholdParameter, args)
            )
            .findOutliers(data, selection);
            time = (System.nanoTime() - time)/1000000l;
            
            System.out.println("Search time: " + time + " ms");            
            System.out.println("Found " + outlierScores.size() + " in the dataset.");

            if(mustOutput(args))
            {
                ContractedDataset outliers = new ContractedDataset(data.getContract());

                //Add outliers to output data
                outlierScores
                    .entrySet()
                    .stream()
                    .sorted(Comparator.comparing(e -> e.getValue()))
                    .map(e -> e.getKey())
                    .forEach(outliers::addDataObject);

                //Call output
                super.output(
                    args,
                    outliers,
                    outlierScores
                        .entrySet()
                        .stream()
                        .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> SetOperations.set(new Violation("soda-outlier", "LMER-score: " + e.getValue(), selection))
                        )),
                    "Soda");
            }
            
            if(mustRepair(args))
            {
                repair(args, data, outlierScores.keySet());
            }
        }
        catch (LedcException | SQLException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
