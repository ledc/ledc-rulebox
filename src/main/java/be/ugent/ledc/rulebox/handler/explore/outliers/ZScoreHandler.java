package be.ugent.ledc.rulebox.handler.explore.outliers;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.zscore.ZScoreOutlierDetector;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.result.table.Violation;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ZScoreHandler extends OutlierHandler
{
    public static final String NAME = "zscore";
        
    public ZScoreHandler()
    {
        super(NAME, "Finds outliers by computation of the Z-score on numerical attributes.");
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));

            //Build selection
            Set<String> selection = super.buildSelection(
                args,
                data.getContract(),
                a -> isNumeric(data.getContract().getAttributeContract(a)));
            
            System.out.println("Searching for outliers with Z-score detector");
            
            ZScoreOutlierDetector detector = new ZScoreOutlierDetector();
            
            Map<DataObject, Map<String,Double>> outlierScores = new HashMap<>();
            
            long time = System.nanoTime();
            for(String a: selection)
            {
                System.out.println("Inspection of attribute '" + a + "':");
                
                Map<DataObject, Double> localOutliers = detector
                    .findOutliers(data, SetOperations.set(a));

                System.out.println("Found " + localOutliers.size() + " in the dataset.\n");
                
                for(DataObject outlier: localOutliers.keySet())
                {
                    if(!outlierScores.containsKey(outlier))
                    {
                        outlierScores.put(outlier, new HashMap<>());
                    }
                    
                    outlierScores.get(outlier).put(a, localOutliers.get(outlier));
                }
            }
            time = (System.nanoTime() - time)/1000000l;
            
            System.out.println("Search time: " + time + " ms");            
            
            if(mustOutput(args))
            {
                ContractedDataset outliers = new ContractedDataset(data.getContract());

                //Add outliers to output data
                outlierScores
                    .entrySet()
                    .stream()
                    .sorted(Comparator.comparing(e -> e //Sort by highest score
                        .getValue()
                        .values()
                        .stream()
                        .mapToDouble(d->d)
                        .max()
                        .getAsDouble()))
                    .map(e -> e.getKey())
                    .forEach(outliers::addDataObject);

                //Call output
                super.output(
                    args,
                    outliers,
                    outlierScores
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e
                            .getValue()
                            .entrySet()
                            .stream()
                            .map(ee -> new Violation("z-score-outlier", "Z-score: " + ee.getValue(), SetOperations.set(ee.getKey())))
                            .collect(Collectors.toSet())     
                    )),
                    "Z-score");
            }
            
            if(mustRepair(args))
            {
                repair(args, data, outlierScores.keySet());
            }
        }
        catch (LedcException | SQLException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
