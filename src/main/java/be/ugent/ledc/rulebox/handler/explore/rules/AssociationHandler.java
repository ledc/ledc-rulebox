package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.dino.rulemining.association.ConfidenceMiner;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.Set;

public class AssociationHandler extends EqualityAtomHandler
{
    public static final String NAME = "association";
    
    private final Parameter<Double> alphaParameter = new Parameter<>(
        "alpha",
        "--a",
        true,
        (String s) -> Double.valueOf(s),
        "Lower bound for the support of association rules."
    )
    .addVerifier(d -> d >= 0.0, "Alpha must be a real value between 0 and 1")
    .addVerifier(d -> d <= 1.0, "Alpha must be a real value between 0 and 1");
    
    private final Parameter<Double> betaParameter = new Parameter<>(
        "beta",
        "--b",
        true,
        (String s) -> Double.valueOf(s),
        "Lower bound for the confidence of association rules."
    )
    .addVerifier(d -> d >= 0.0, "Beta must be a real value between 0 and 1")
    .addVerifier(d -> d <= 1.0, "Beta must be a real value between 0 and 1");
    
    private final Parameter<Double> sigmaParameter = new OptionalParameter<>(
        "sigma",
        "--s",
        (String s) -> Double.valueOf(s),
        "Lower bound for the confidence ratios of an association rule. These are the ratios of the confidence of the rule over the confidence of super-rules (less conditions). Default value is 1.",
        1.0
    )
    .addVerifier(d -> d >= 1.0, "Sigma must be a real value larger than or equal to 1");
    
    private final Parameter<Double> epsilonParameter = new OptionalParameter<>(
        "epsilon",
        "--eps",
        (String s) -> Double.valueOf(s),
        "Upper bound for the confidence of 'exception' rules that indicate exceptions an association rule are present. Default value is 1.",
        1.0
    )
    .addVerifier(d -> d >= 0.0, "Epsilon must be a real value between 0 and 1")
    .addVerifier(d -> d <= 1.0, "Epsilon must be a real value between 0 and 1");
    
    private final Flag includeExcpetions = new Flag(
        "include-exceptions",
        "--ie",
        "Includes exceptions to assocation rules found by lowering epsilon."
    );

    public AssociationHandler()
    {
        super(NAME, "Searches for selection rules by application of assocation analysis "
            + "and the search for 'strong' rules. "
            + "Search is limited to string and integer attributes.");
        
        addParameter(alphaParameter);
        addParameter(betaParameter);
        addParameter(sigmaParameter);
        addParameter(epsilonParameter);
        
        addFlag(includeExcpetions);
    }

    @Override
    public SigmaRuleset mine(ContractedDataset data, Set<String> selection, String[] args) throws ParameterException
    {
        System.out.println("Searching for sigma rules with association analysis");
        System.out.println("Selected attributes: " + selection);

        SigmaRuleset sigmaRules = new ConfidenceMiner
            .ConfidenceMinerBuilder(
                ParameterTools.extractParameter(alphaParameter, args),
                ParameterTools.extractParameter(betaParameter, args)
            )
            .withIgnoreNulls(true)
            .withEpsilon(ParameterTools.extractParameter(epsilonParameter, args))
            .withSigma(ParameterTools.extractParameter(sigmaParameter, args))
            .withIncludeExceptions(ParameterTools.extractFlag(includeExcpetions, args))
            .build()
            .findRules(data, selection);
        
        return sigmaRules;
    }

    
}
