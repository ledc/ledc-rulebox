package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.rubix.RubixWriter;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.AtomType;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A Handler for mining selection rules where atoms involve equality or inequality operators.
 * An exception holds for domain rules, where set atoms are allowed.
 * 
 * Handlers of this type are marked by the property they handle only integer or string data.
 * Rule mining in this scenario is useful to detect errors in code-to-name mappings.
 * @author abronsel
 */
public abstract class EqualityAtomHandler extends SelectionRuleMiner
{  
    public EqualityAtomHandler(String command, String description) {
        super(command, description);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
            
            ContractedDataset data = BinderPool
                .getInstance()
                .data(binder);
            
            Contract contract = data.getContract();

            Set<String> selection = select(args, data);
            
            for(String a: contract.getAttributes())
            {
                String cName = contract.getAttributeContract(a).name();
                
                //String = do nothing
                if(cName.equals(SigmaContractorFactory.STRING.name()))
                    continue;
                
                //Integer data: cast
                if(cName.equals(SigmaContractorFactory.INTEGER.name()) ||
                   cName.equals(SigmaContractorFactory.LONG.name()))
                {
                    data = data.asString(a, TypeContractorFactory.STRING);
                }
                else
                {
                    selection.remove(a);
                }
            } 
            
            File rbxFile = ParameterTools.extractParameter(getOutputFileParameter(), args);
            
            SigmaRuleset sigmaRules = mine(data, selection, args);
            
            //Transform
            if(selection
                .stream()
                .anyMatch(a -> !contract
                    .getAttributeContract(a)
                    .name()
                    .equals(TypeContractorFactory.STRING.name())))
            {
                sigmaRules = transform(sigmaRules, selection, contract);
            }
            
            //Write the result
            System.out.println("Writing rubix file");
            
            Rubix rubix = new Rubix
                .RubixBuilder()
                .withSigmaRules(needsPrefix(args) ? prefix(binder, sigmaRules) : sigmaRules)
                .build();
            
            RubixWriter.writeRubix(rubix, rbxFile);
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    private SigmaRuleset transform(SigmaRuleset sigmaRules, Set<String> selection, Contract contract)
    {
        Map<String, SigmaContractor<?>> transformedContractors = new HashMap<>();
        Map<String, Integer> codes = new HashMap<>();
        
        for(String a: selection)
        {
            if(contract.getAttributeContract(a).name().equals(TypeContractorFactory.STRING.name()))
            {
                transformedContractors.put(a, SigmaContractorFactory.STRING);
                codes.put(a, 0);
            }
            else if(contract.getAttributeContract(a).name().equals(TypeContractorFactory.INTEGER.name()))
            {
                transformedContractors.put(a, SigmaContractorFactory.INTEGER);
                codes.put(a, 1);
            }
            else if(contract.getAttributeContract(a).name().equals(TypeContractorFactory.LONG.name()))
            {
                transformedContractors.put(a, SigmaContractorFactory.LONG);
                codes.put(a, 2);
            }
        }
        
        Set<SigmaRule> transformedRules = new HashSet<>();
        
        for(SigmaRule sr: sigmaRules.getRules())
        {
            Set<AbstractAtom<?,?,?>> tAtoms = new HashSet<>();
            
            for(AbstractAtom<?,?,?> atom: sr.getAtoms())
            {
                if(atom.getAtomType() == AtomType.CONSTANT)
                {
                    ConstantNominalAtom<String> cAtom = (ConstantNominalAtom)atom;
                    String att = cAtom.getAttribute();
                    
                    switch(codes.get(att))
                    {
                        case 0:
                            tAtoms.add(atom);
                            break;
                        case 1:
                            tAtoms.add(new ConstantOrdinalAtom<>(
                                SigmaContractorFactory.INTEGER,
                                att,
                                cAtom.getOperator(),
                                SigmaContractorFactory.INTEGER.parseConstant(cAtom.getConstant())
                            ));
                            break;
                        case 2:
                            tAtoms.add(new ConstantOrdinalAtom<>(
                                SigmaContractorFactory.LONG,
                                att,
                                cAtom.getOperator(),
                                SigmaContractorFactory.LONG.parseConstant(cAtom.getConstant())
                            ));
                            break;
                    }
                    
                }
                else if(atom.getAtomType() == AtomType.SET)
                {
                    SetNominalAtom<String> sAtom = (SetNominalAtom)atom;
                    
                    String att = sAtom.getAttribute();
                    
                    switch(codes.get(att))
                    {
                        case 0:
                            tAtoms.add(atom);
                            break;
                        case 1:
                            tAtoms.add(new SetOrdinalAtom<>(
                                SigmaContractorFactory.INTEGER,
                                att,
                                sAtom.getOperator(),
                                sAtom.getConstants()
                                    .stream()
                                    .map(cst -> SigmaContractorFactory
                                        .INTEGER
                                        .parseConstant(cst))
                                    .collect(Collectors.toSet())
                                
                            ));
                            break;
                        case 2:
                            tAtoms.add(new SetOrdinalAtom<>(
                                SigmaContractorFactory.LONG,
                                att,
                                sAtom.getOperator(),
                                sAtom.getConstants()
                                    .stream()
                                    .map(cst -> SigmaContractorFactory
                                        .LONG
                                        .parseConstant(cst))
                                    .collect(Collectors.toSet())
                                
                            ));
                            break;
                    }
                }
            }
            
            transformedRules.add(new SigmaRule(tAtoms));
        }
        
        return new SigmaRuleset(transformedContractors,transformedRules);
    }

    public abstract SigmaRuleset mine(ContractedDataset data, Set<String> selection, String[] args) throws ParameterException;
    
}
