package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.rulebox.Rubix;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class FDMiner extends RuleMiningHandler
{
    public FDMiner(String command, String description)
    {
        super(command, description);
    }
    
    protected RuleSet<Dataset,FD> prefix(String binder, RuleSet<Dataset,FD> mined)
    {
        return new RuleSet<>(mined
            .stream()
            .map(fd -> new FD(prefix(binder, fd.getLeftHandSide()), prefix(binder, fd.getLeftHandSide())))
            .collect(Collectors.toSet()));
    }
    
    private Set<String> prefix(String binder, Set<String> attrs)
    {
        return attrs
            .stream()
            .map(a -> prefix(binder, a))
            .collect(Collectors.toSet());
    }
    
    private String prefix(String binder, String a)
    {
        return binder + Rubix.SEPARATOR + a;
    }
}
