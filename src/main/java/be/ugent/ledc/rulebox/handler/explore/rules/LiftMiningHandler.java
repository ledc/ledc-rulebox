package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.operators.aggregation.tnorm.BasicTNorm;
import be.ugent.ledc.core.operators.aggregation.tnorm.TNorm;
import be.ugent.ledc.dino.rulemining.tlift.TGenerator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LiftMiningHandler extends EqualityAtomHandler
{
    public static final String NAME = "lift";
    
    public final Map<String, TNorm> norms = new HashMap<>();

    private final Parameter<Double> tauParameter = new OptionalParameter<>(
        "tau",
        "--t",
        (String s) -> Double.valueOf(s),
        "Upper bound for the lift of a combination of values. Low lift implies negative correlation. Default value is 0.01",
        0.01
    )
    .addVerifier(d -> d > 0.0, "Tau must be a real, positive value.")
    .addVerifier(d -> d < 1.0, "Tau must be chosen below 1.0");
    
    private final Parameter<String> tNormParameter = new OptionalParameter<>(
        "tnorm",
        "--tn",
        Function.identity(),
        "Triangular norm that models the notion of 'independence' between values. Default value is product. Other options: minimum, hamacher, lukasiewics",
        "product"
    )
    .addVerifier(
        s -> norms.containsKey(s),
        "tnorm value must be one of " + norms
            .keySet()
            .stream()
            .collect(Collectors.joining(","))
    );
    public LiftMiningHandler()
    {
        super(NAME, "Searches for selection rules by searching for low-lift patterns in the data. "
            + "The current implementation supports string attributes only.");
        
        norms.put("product", BasicTNorm.PRODUCT);
        norms.put("minimum", BasicTNorm.MINIMUM);
        norms.put("hamacher", BasicTNorm.HAMACHER_PRODUCT);
        norms.put("lukasiewics", BasicTNorm.LUKASIEWICS);
        
        addParameter(tauParameter);
        addParameter(tNormParameter);

    }

    @Override
    public SigmaRuleset mine(ContractedDataset data, Set<String> selection, String[] args) throws ParameterException
    {
        System.out.println("Searching for sigma rules via low lift patterns");

        SigmaRuleset sigmaRules = new TGenerator(
            ParameterTools.extractParameter(tauParameter, args),
            true,
            norms.get(ParameterTools.extractParameter(tNormParameter, args))
        ).findRules(data, selection);
        
        return sigmaRules;
    }

}
