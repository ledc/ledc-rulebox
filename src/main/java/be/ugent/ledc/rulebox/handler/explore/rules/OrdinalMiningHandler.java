package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.dino.rulemining.ordinal.OrdinalBinaryMiner;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.rubix.RubixWriter;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

public class OrdinalMiningHandler extends SelectionRuleMiner
{
    public static final String NAME = "ordinal";

    private final Parameter<Double> lambda = new OptionalParameter<>(
        "lambda",
        "--l",
        (String s) -> Double.valueOf(s),
        "Upper bound for the lift of a combination of values. Low lift implies negative correlation. Default value is 0.01",
        0.01
    )
    .addVerifier(d -> d > 0.0, "Lambda must be a real, positive value.")
    .addVerifier(d -> d < 1.0, "Lambda must be chosen below 1.0");
    
    public OrdinalMiningHandler()
    {
        super(NAME, "Searches for selection rules by searching for deviations of strong monotonic patterns in the data."
            + "The current implementation supports numeric attributes only.");
        
        addParameter(lambda);
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));

            Set<String> preSelection = select(args, data);
            
            Set<String> selection = new HashSet<>();
            
            for(String a: data.getContract().getAttributes())
            {
                if(preSelection.contains(a))
                {
                    data = toSigmaContractor(data, a);
                
                    if(data.getContract().getAttributeContract(a) instanceof SigmaContractor)
                        selection.add(a);
                }
            }
            
            System.out.println("Searching for sigma rules via monotonic patterns");

            SigmaRuleset sigmaRules = new OrdinalBinaryMiner(
                ParameterTools.extractParameter(lambda, args)
            )
            .findRules(data, selection);
            
             //Write the result
            System.out.println("Writing rubix file");
            String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
            Rubix rubix = new Rubix
                .RubixBuilder()
                .withSigmaRules(needsPrefix(args) ? prefix(binder, sigmaRules) : sigmaRules)
                .build();
            
            RubixWriter.writeRubix(
                rubix,
                ParameterTools.extractParameter(getOutputFileParameter(), args));
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
