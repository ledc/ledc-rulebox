package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import java.io.File;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class RuleMiningHandler extends DataHandler
{
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (f) -> f.getName().toLowerCase().endsWith(".rbx"),
            "Output file extension must be .rbx"
        );
    
    private final Parameter<Set<String>> onlyOnParameter = new Parameter<>(
        "only",
        "--o",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Find rules only for these attributes (comma-separated list)"
    );
    
    private final Parameter<Set<String>> excludeParameter = new Parameter<>(
        "exclude",
        "--e",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Don't find rules for these attributes (comma-separated list)"
    );
    
    private final Flag noPrefixFlag = new Flag
    (
        "no-prefix",
        "--np",
        "Avoids that the binder name is added as a prefix to attributes in the rules."
    );
    
    public RuleMiningHandler(String command, String description)
    {
        super(command, description);
        addParameter(outputFileParameter);
        addParameter(onlyOnParameter);
        addParameter(excludeParameter);
        addFlag(noPrefixFlag);
    }

    public Parameter<File> getOutputFileParameter()
    {
        return outputFileParameter;
    }

    public Parameter<Set<String>> getOnlyOnParameter()
    {
        return onlyOnParameter;
    }

    public Parameter<Set<String>> getExcludeParameter()
    {
        return excludeParameter;
    }
    
    public Set<String> select(String[] args, ContractedDataset data) throws ParameterException
    {
        Set<String> selection = data
            .getContract()
            .getAttributes();

        Set<String> onlyOn  = ParameterTools.extractParameter(onlyOnParameter, args);
        Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);

        if(onlyOn != null)
            selection = SetOperations.intersect(selection, onlyOn);
        if(exclude != null)
            selection = SetOperations.difference(selection, exclude);
            
        return selection;
    }
    
    public boolean needsPrefix(String[] args)
    {
        return !ParameterTools.extractFlag(noPrefixFlag, args);
    }
}
