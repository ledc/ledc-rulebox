package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class SelectionRuleMiner extends RuleMiningHandler
{
    
    public SelectionRuleMiner(String command, String description)
    {
        super(command, description);
    }
    
    protected SigmaRuleset prefix(String binder, SigmaRuleset mined)
    {
        Map<String, SigmaContractor<?>> eContractors = mined
            .getContractors()
            .entrySet()
            .stream()
            .collect(Collectors.toMap(
                    e -> binder + Rubix.SEPARATOR + e.getKey(),
                    e -> e.getValue()));
        
        return new SigmaRuleset(
            eContractors,
            mined
                .getRules()
                .stream()
                .map(sr -> new SigmaRule(sr
                    .getAtoms()
                    .stream()
                    .map(aa -> aa.nameTransform(n -> binder + Rubix.SEPARATOR + n))
                    .collect(Collectors.toSet())
                ))
                .collect(Collectors.toSet())
            );
    }
    
}
