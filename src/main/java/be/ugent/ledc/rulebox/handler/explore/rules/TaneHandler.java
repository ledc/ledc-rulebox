package be.ugent.ledc.rulebox.handler.explore.rules;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.discovery.tane.TaneMiner;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.rubix.RubixWriter;
import java.io.FileNotFoundException;
import java.util.Set;
import java.util.stream.Collectors;

public class TaneHandler extends FDMiner
{
    public static final String NAME = "tane";
    
    private final Parameter<Double> thresholdParameter = new OptionalParameter<>(
        "error-threshold",
        "--et",
        (String s) -> Double.valueOf(s),
        "Specifies the allowed error rate for an FD. Values greater than zero will imply searching for approximate FDs. Default value is 0.",
        0.0
    )
    .addVerifier(d -> d >= 0.0, "Error threshold must be a real, positive value.")
    .addVerifier(d -> d < 1.0, "Error threshold must be chosen below 1.0.");
    
    private final Parameter<Integer> maxLevelParameter = new OptionalParameter<>(
        "max-level",
        "--m",
        (String s) -> Integer.valueOf(s),
        "Sets a maximum level for TANE. Higher levels will not be examined.",
        Integer.MAX_VALUE
    )
    .addVerifier(i -> i > 1, "Max level must be an integer number larger than 1.");
    
    public TaneHandler()
    {
        super(NAME, "Searches for minimal and non-trivial FDs that are satisfied in the dataset by usage of the TANE algorithm");
        addParameter(thresholdParameter);
        addParameter(maxLevelParameter);
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));

            double e = ParameterTools.extractParameter(thresholdParameter, args);
            int m =  ParameterTools.extractParameter(maxLevelParameter, args);
            
            System.out.println("Searching for FDs via TANE");

            Set<FD> fds = new TaneMiner(select(args, data), e, m)
            .discover(data)
            .stream()
            .map(sfd -> (FD)sfd)
            .collect(Collectors.toSet());
            
             //Write the result
            System.out.println("Writing rubix file");
            String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
            Rubix rubix = new Rubix
                .RubixBuilder()
                .withFunctionalDependencies(needsPrefix(args)
                    ? prefix(binder, new RuleSet<>(fds))
                    : new RuleSet<>(fds))
                .build();
            
            RubixWriter.writeRubix(
                rubix,
                ParameterTools.extractParameter(getOutputFileParameter(), args));
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
