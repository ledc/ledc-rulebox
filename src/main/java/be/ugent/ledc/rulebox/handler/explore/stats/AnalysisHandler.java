package be.ugent.ledc.rulebox.handler.explore.stats;

import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.handler.util.IdHandler;
import be.ugent.ledc.rulebox.handler.util.PlotUtil;
import be.ugent.ledc.core.statistics.Statistics;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.BoxPlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import be.ugent.ledc.rulebox.io.result.table.TableContainer;
import java.io.File;
import java.io.IOException;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnalysisHandler extends DataHandler
{
    public static final String NAME = "analyse";
       
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (file) -> file.getName().endsWith(".html") || file.getName().endsWith(".xml"),
            "Outputfile must be an .html or .xml file");
    
    private final Parameter<Set<String>> onlyOnParameter = new Parameter<>(
        "only",
        "--o",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Analyse only for these attributes (comma-separated list)"
    );
    
    private final Parameter<Set<String>> excludeParameter = new Parameter<>(
        "exclude",
        "--e",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Exclude these attributes from analysis (comma-separated list)"
    );
    
    private final Parameter<String> histogramParameter = new Parameter<>(
        "histogram",
        "--h",
        false,
        (s) -> s,
        "Adds histograms to the analysis. The value for this parameter either indicates an integer number of bins or a method to compute it. Available methods are: "
            + Binning
                .names()
                .stream()
                .collect(Collectors.joining(",")))
        .addVerifier(s ->
            s.trim().matches("\\d+")
            || Binning.names().contains(s),
            "Binning value must be an integer number "
            + "or one of the available methods to compute the number of bins: "
            + Binning
                .names()
                .stream()
                .collect(Collectors.joining(",")))
        ;
    
    private final Flag boxPlotFlag = new Flag(
        "boxplot",
        "--b",
        "Adds boxplots to the analysis");
    
    private final Flag statsFlag = new Flag(
        "statistics",
        "--s",
        "Adds basic statistics of the dataset to the analysis");
    
    private final IdHandler idHandler = new IdHandler();
        
    public AnalysisHandler()
    {
        super(NAME, "Produces a basic analysis of a dataset.");
        
        addParameter(onlyOnParameter);
        addParameter(excludeParameter);
        addParameter(outputFileParameter);
        addParameter(histogramParameter);
        addFlag(boxPlotFlag);
        addFlag(statsFlag);
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
                 
            Set<String> selection = data
                .getContract()
                .getAttributes();

            Set<String> onlyOn  = ParameterTools.extractParameter(onlyOnParameter, args);
            Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);
            
            if(onlyOn != null)
                selection = SetOperations.intersect(selection, onlyOn);
            if(exclude != null)
                selection = SetOperations.difference(selection, exclude);

            String binning = ParameterTools.extractParameter(histogramParameter, args);
            
            Map<Integer, List<Container<?>>> containers = new HashMap<>();
            
            if(binning != null)
            {
                containers.put(
                    ParameterTools.locateParameter(histogramParameter, args),
                    ListOperations.list(buildHistogramPlots(data, selection, binning))
                );
            }
            
            if(ParameterTools.extractFlag(boxPlotFlag, args))
            {
                PlotContainer boxPlots = buildBoxPlots(data, selection);
                        
                if(!boxPlots.isEmpty())
                    containers.put(
                        ParameterTools.locateFlag(boxPlotFlag, args),
                        ListOperations.list(boxPlots)
                    );
            }
            
            if(ParameterTools.extractFlag(statsFlag, args))
            {
                List<Container<?>> tableContainers = ListOperations.list(
                    buildNumericalStatistics(data, selection),
                    buildNonNumericalStatistics(data, selection)
                );
                
                tableContainers.removeIf(tc -> tc.isEmpty());
                
                if(!tableContainers.isEmpty())
                    containers.put(
                        ParameterTools.locateFlag(statsFlag, args),    
                        tableContainers
                    );
            }
            
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers
                        .keySet()
                        .stream()
                        .sorted()
                        .flatMap(i -> containers.get(i).stream())
                        .collect(Collectors.toList()),
                    ParameterTools.extractParameter(outputFileParameter, args)
                );
            }
        }
        catch (ParameterException | DataReadException | BinderPoolException | MarshallException ex)
        {
            System.err.println(ex.getMessage());
        }
        catch (IOException  ex)
        {
            Logger.getLogger(AnalysisHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private PlotContainer buildHistogramPlots(ContractedDataset data, Set<String> selection, String binning)
    {
        System.out.println("Start processing attributes for histogram building");
            
        PlotContainer container = new PlotContainer("Histograms", idHandler.next());

        for(String a: selection)
        {       
            if(binning.equals(Binning.FREEDMAN_DIACONIS.getName())
            && !isNumeric(data.getContract().getAttributeContract(a)))
            {
                System.out.println("Warning: "
                    + Binning.FREEDMAN_DIACONIS.getName()
                    + " can applied only to numerical data."
                    + " Switching to "
                    + Binning.STURGE.getName()
                    + " for attribute '"
                    + a
                    + "'."
                );
            }

            int bins = binning.equals(Binning.FREEDMAN_DIACONIS.getName())
                && !isNumeric(data.getContract().getAttributeContract(a))
                ? Binning.STURGE.bins(data, a)
                :
                    binning.matches("\\d+")
                    ? Integer.parseInt(binning)
                    : Binning.parse(binning).bins(data, a);

            if(bins > 55 ||
                (   data.getContract().getAttributeContract(a).equals(TypeContractorFactory.STRING)
                &&  data.project(a).distinct().getSize() > 55))
            {
                System.out.println("Skipping histogram for attribute '" + a + "'. Too many bins.");
                continue;
            }

            container.addItem(PlotUtil.buildHistogramPlot(data, a, bins));
        }
        
        return container;
    }

    private PlotContainer buildBoxPlots(ContractedDataset data, Set<String> selection)
    {
        System.out.println("Start processing attributes for box plot building");
            
        PlotContainer container = new PlotContainer("Box plots", idHandler.next(), 6);

        for(String a: selection)
        {       
            if(!isNumeric(data.getContract().getAttributeContract(a)))
            {
                System.out.println("Skipping box plot for attribute '" + a + "'. Attribute is not numeric.");
                continue;
            }

            BoxPlot plot = PlotUtil.buildBoxPlot(data, a, a);
            
            if(plot != null)
                container.addItem(plot);
        }
        
        return container;
    }
    
    private TableContainer buildNumericalStatistics(ContractedDataset data, Set<String> selection)
    {
        System.out.println("Start computing stats for numerical attributes");
        
        Contract contract = new Contract
            .ContractBuilder()
            .addContractor("attribute", TypeContractorFactory.STRING)
            .addContractor("datatype", TypeContractorFactory.STRING)
            .addContractor("null values", TypeContractorFactory.INTEGER)
            .addContractor("unique values", TypeContractorFactory.INTEGER)
            .addContractor("values", TypeContractorFactory.INTEGER)
            .addContractor("minimum", TypeContractorFactory.DOUBLE)
            .addContractor("median", TypeContractorFactory.STRING)
            .addContractor("mean", TypeContractorFactory.STRING)
            .addContractor("maximum", TypeContractorFactory.DOUBLE)
            .addContractor("standard deviation", TypeContractorFactory.STRING)
            .build();
        
        ContractedDataset numStats = new ContractedDataset(contract);
        
        for(String a: selection)
        {
            TypeContractor<?> ctrc = data
                .getContract()
                .getAttributeContract(a);

            if(isNumeric(ctrc))
            {
                DataObject dobj = new DataObject();
            
                dobj.setString("attribute", a);
                dobj.setString("datatype", ctrc.name());
            
                List<Double> values = data
                    .project(a)
                    .select(o -> o.get(a) != null)
                    .stream()
                    .map(o -> ((Number)o.get(a)).doubleValue())
                    .collect(Collectors.toList());

                dobj.setInteger("null values", data.getSize() - values.size());
                dobj.setInteger("unique values", new HashSet<>(values).size());
                dobj.setInteger("values", data.getSize());


                if(!values.isEmpty())
                {
                    DoubleSummaryStatistics stats = values
                        .stream()
                        .mapToDouble(d->d)
                        .summaryStatistics();
                    
                    int decimals = (int) (stats.getMax() == stats.getMin()
                        ? 2 + (-Math.log10(stats.getMax()))
                        : 2 + (-Math.log10(stats.getMax() - stats.getMin())));

                    String f = "%." + Math.max(decimals, 0) + "f";

                    dobj.setDouble("minimum", stats.getMin());
                    dobj.setDouble("maximum", stats.getMax());
                    dobj.setString("mean", String.format(f, stats.getAverage()));
                    dobj.setString("median", String.format(f, Statistics.median(values)));
                    dobj.setString("standard deviation", String.format(f, Statistics.stdDev(values)));
                }
                
                numStats.addDataObject(dobj);
            }        
        }
                
        return new TableContainer(
            numStats,
            "Statistics for numerical data",
            idHandler.next(),
            ListOperations.list(
                "attribute",
                "datatype",
                "values",
                "unique values",
                "null values",
                "minimum",
                "median",
                "mean",
                "standard deviation",
                "maximum"
            ),
            false
        );
    }
    
    private TableContainer buildNonNumericalStatistics(ContractedDataset data, Set<String> selection)
    {
        System.out.println("Start computing stats for non-numerical attributes");
        
        Contract contract = new Contract
            .ContractBuilder()
            .addContractor("attribute", TypeContractorFactory.STRING)
            .addContractor("datatype", TypeContractorFactory.STRING)
            .addContractor("null values", TypeContractorFactory.INTEGER)
            .addContractor("unique values", TypeContractorFactory.INTEGER)
            .addContractor("values", TypeContractorFactory.INTEGER)
            .build();
        
        ContractedDataset nonNumStats = new ContractedDataset(contract);
        
        for(String a: selection)
        {
            TypeContractor<?> ctrc = data
                .getContract()
                .getAttributeContract(a);

            if(!isNumeric(ctrc))
            {
                DataObject dobj = new DataObject();
            
                dobj.setString("attribute", a);
                dobj.setString("datatype", ctrc.name());
            
                List<Object> nonNulls = data
                    .select(o -> o.get(a) != null)
                    .stream()
                    .map(o -> o.get(a))
                    .collect(Collectors.toList());

                dobj.setInteger("null values", data.getSize() - nonNulls.size());
                dobj.setInteger("unique values", new HashSet<>(nonNulls).size());
                dobj.setInteger("values", data.getSize());
                
                nonNumStats.addDataObject(dobj);
            }        
        }
                
        return new TableContainer(
            nonNumStats,
            "Statistics for non-numerical data",
            idHandler.next(),
            ListOperations.list(
                "attribute",
                "datatype",
                "values",
                "unique values",
                "null values"
            ),
            false
        );
    }
}