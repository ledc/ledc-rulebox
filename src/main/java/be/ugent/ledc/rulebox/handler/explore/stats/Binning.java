package be.ugent.ledc.rulebox.handler.explore.stats;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.statistics.Statistics;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Binning
{
    SQRT(
        "sqrt",
        (data,a) -> (int)Math.ceil(Math.sqrt(data.getSize()))
    ),
    STURGE(
        "sturge",
        (data,a) -> 1 + (int)Math.ceil(Statistics.log2(data.getSize()))
    ),
    RICE(
        "rice",
        (data,a) -> (int) Math.ceil(2.0 * Math.cbrt(data.getSize()))
    ),
    FREEDMAN_DIACONIS(
        "freedman_diaconis",
        (data,a) -> (int)Math.ceil(
            (Statistics.range(data,a) * Math.cbrt(data.getSize()))
            /
            (2.0 * Statistics.interQuartileRange(data, a))
        )
    );
    
    private final String name;
    
    private final BiFunction<ContractedDataset, String, Integer> hiddenBinSizeFunction;
    
    private Binning(String name, BiFunction<ContractedDataset, String, Integer> hiddenBinSizeFunction)
    {
        this.name = name;
        this.hiddenBinSizeFunction = hiddenBinSizeFunction;
    }
    
    public int bins(ContractedDataset dataset, String a)
    {
        return hiddenBinSizeFunction.apply(dataset, a);
    }

    public String getName()
    {
        return name;
    }
    
    public static Binning parse(String name)
    {
        return Stream
            .of(values())
            .filter(b -> b.getName().equals(name))
            .findFirst()
            .orElseThrow(() -> new RuntimeException("Unknown binning type '" + name +"'"));
    }
    
    public static Set<String> names()
    {
        return Stream
            .of(values())
            .map(b -> b.getName())
            .collect(Collectors.toSet());
    }
    
}
