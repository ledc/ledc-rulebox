package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.algorithms.transform.acf.AutoCorrelationFunction;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.Line;
import be.ugent.ledc.rulebox.io.result.plot.LinePlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ACFHandler extends TimesHandler
{  
    
    public ACFHandler()
    {
        super("acf", "Plot the Auto Correlation Function");
    }
    
    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            
            //Initialize containers
            List<Container<?>> containers = new ArrayList<>();
            
            if(super.isPartitioned(args))
            {
                Map<String, Map<String, Signal>> pSignals = super.createMultiSignals(args);
                
                for(String label: pSignals.keySet())
                {
                    PlotContainer plotContainer = new PlotContainer(
                        "ACF analysis for " + label,
                        idHandler.next(),
                        1);
                    
                    System.out.println("Handling label " + label);
                    addPlots(
                        plotContainer, 
                        pSignals.get(label),
                        args);
                    
                    if(!plotContainer.isEmpty())
                        containers.add(plotContainer);
                }
            }
            else
            {
                PlotContainer plotContainer = new PlotContainer(
                    "ACF",
                    idHandler.next(),
                    1);

                addPlots(
                    plotContainer,
                    super.createSignals(args),
                    args);
                
                if(!plotContainer.isEmpty())
                    containers.add(plotContainer);
            }
            
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers,
                    createOutputFile(args)
                );
            }

        }
        catch (LedcException | MarshallException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }

    private void addPlots(PlotContainer plotContainer, Map<String, Signal> signals, String[] args) throws ChronosException, MarshallException
    {       
        for(String attribute: signals.keySet())
        {
            System.out.println("Computing ACF for " + attribute);

            //Get the next signal for the attribute
            Signal<Comparable<? super Comparable>, Number> signal = convert(signals.get(attribute));

            double avgGap = signal
                .interMeasureGap()
                .stream()
                .mapToLong(l->l)
                .summaryStatistics()
                .getAverage();

            System.out.println("Mean inter-measure distance (units): " + avgGap);

            List<Pair<Long, Double>> acfData = AutoCorrelationFunction
                .acf(signal)
                .entrySet()
                .stream()
                .map(e -> new Pair<>(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

            Line<Long, Double> acfLine = new Line<>("acf", acfData);

            LinePlot<Long, Double> plot = new LinePlot<>(
                SigmaContractorFactory.LONG,
                "ACF for " + attribute,
                acfLine
            );
            plot.setFixedUpperBound(1.0);
            plot.setFixedLowerBound(-1.0);
            plotContainer.addItem(plot);
        }
    }
}
