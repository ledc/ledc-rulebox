package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.Line;
import be.ugent.ledc.rulebox.io.result.plot.LinePlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class ChangeDetectionHandler extends TimesHandler
{

    public ChangeDetectionHandler(String command, String description)
    {
        super(command, description);
    }
    
    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            
            //Initialize containers
            List<Container<?>> containers = new ArrayList<>();
            
            if(super.isPartitioned(args))
            {
                Map<String, Map<String, Signal>> pSignals = super.createMultiSignals(args);
                
                for(String label: pSignals.keySet())
                {
                    PlotContainer plotContainer = new PlotContainer(
                        "Change points analysis for " + label,
                        idHandler.next(),
                        1);
                    
                    System.out.println("Handling label " + label);
                    addPlots(plotContainer, pSignals.get(label), args);
                    
                    if(!plotContainer.isEmpty())
                        containers.add(plotContainer);
                }
            }
            else
            {
                PlotContainer plotContainer = new PlotContainer(
                    "Change points analysis",
                    idHandler.next(),
                    1);

                addPlots(plotContainer, super.createSignals(args), args);
                
                if(!plotContainer.isEmpty())
                    containers.add(plotContainer);
            }            
            
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers,
                    createOutputFile(args)
                );
            }

        }
        catch (LedcException | MarshallException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    public abstract List<Comparable<? super Comparable>> findChanges(Signal<Comparable<? super Comparable>, Number> signal, String[] args) throws ChronosException, ParameterException;

    private void addPlots(PlotContainer plotContainer, Map<String, Signal> signals, String[] args) throws ChronosException, MarshallException, BinderPoolException, ParameterException, FileNotFoundException, DataReadException
    {
        for(String attribute: signals.keySet())
        {
            System.out.println("Applying change detection for " + attribute);

            //Get the next signal for the attribute
            Signal<Comparable<? super Comparable>, Comparable> pSignal = signals.get(attribute);

            Signal<Comparable<? super Comparable>, Number> nSignal = convert(pSignal);

            Line<Comparable<? super Comparable>, Number> line = new Line(
                "Original",
                toList(nSignal),
                true,
                false,
                false);

            LinePlot<Comparable<? super Comparable>, Number> plot = new LinePlot<>(
                super.getTimeContractor(args),
                "Change points for " + attribute,
                line
            );

            List<Comparable<? super Comparable>> changes = findChanges(nSignal, args);

            for(Comparable<? super Comparable> change: changes)
            {    
                line.mark(change, "change");
            }

            plotContainer.addItem(plot);
        }
    }
    
}
