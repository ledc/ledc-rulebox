package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.algorithms.changedetection.CusumChangeDetector;
import be.ugent.ledc.chronos.algorithms.transform.cusum.CusumLinearTransform;
import be.ugent.ledc.chronos.algorithms.transform.cusum.CusumNormalTransform;
import be.ugent.ledc.chronos.algorithms.transform.cusum.CusumPoissonTransform;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CusumDetectionHandler extends ChangeDetectionHandler
{
    private final Parameter<Integer> windowParameter = new OptionalParameter<>
    (
        "window",
        "--w",
        (s) -> Integer.valueOf(s),
        "Size of the window used for comparison of signal portions. Default value is 10.",
        10
    )
    .addVerifier(i -> i > 0, "Window size must be strictly positive.");
    
    private final Parameter<Double> multiplierParameter = new OptionalParameter<>
    (
        "multiplier",
        "--m",
        (s) -> Double.valueOf(s),
        "Multiplier applied to the IQR range. Default value is 2.",
        2.0
    )
    .addVerifier(i -> i > 0.0, "Window size must be strictly positive.");
    
    public final static String NORMAL     = "normal";
    public final static String POISSON    = "poisson";
    public final static String LINEAR     = "linear";
    
    public final static String[] MODELS = new String[]{NORMAL, POISSON, LINEAR};
    
    private final Parameter<String> modelParameter = new OptionalParameter<>
    (
        "model",
        "--mo",
        (s) -> s,
        "Underlying model that generated the data. Default model is normal.",
        "normal"
    )
    .addVerifier(m -> Stream
        .of(MODELS)
        .anyMatch(model -> model.equals(m)),
        "Unknown model. Available models are: " + Stream
            .of(MODELS)
            .collect(Collectors.joining(",")));
    
    
    
    public CusumDetectionHandler()
    {
        super("cusum", "Detect change points based on the CUSUM statistic.");
        
        addParameter(windowParameter);
        addParameter(modelParameter);
        addParameter(multiplierParameter);
    }

    @Override
    public List<Comparable<? super Comparable>> findChanges(Signal<Comparable<? super Comparable>, Number> signal, String[] args) throws ChronosException, ParameterException
    {
        String model      = ParameterTools.extractParameter(modelParameter, args);
        Integer window    = ParameterTools.extractParameter(windowParameter, args);
        Double multiplier = ParameterTools.extractParameter(multiplierParameter, args);
        
        switch(model)
        {
            case NORMAL:
                return new CusumChangeDetector<Comparable<? super Comparable>,Number>(
                    new CusumNormalTransform<>(window),
                    multiplier)
                .changes(signal);
            case POISSON:
                return new CusumChangeDetector<Comparable<? super Comparable>,Number>(
                    new CusumPoissonTransform(window),
                    multiplier)
                .changes(signal);
            case LINEAR:
                return new CusumChangeDetector<Comparable<? super Comparable>,Number>(
                    new CusumLinearTransform(window),
                    multiplier)
                .changes(signal);
        }
        

        return new ArrayList<>();
    }
    
}
