package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.algorithms.transform.fourier.FourierAnalysis;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.datastructures.ComplexNumber;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.Line;
import be.ugent.ledc.rulebox.io.result.plot.LinePlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FFTHandler extends TimesHandler
{  
    
    public FFTHandler()
    {
        super("fft", "Use the discrete fourier transform to detect potential cycles in the data");
    }
    
    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            
            //Initialize containers
            List<Container<?>> containers = new ArrayList<>();
            
            if(super.isPartitioned(args))
            {
                Map<String, Map<String, Signal>> pSignals = super.createMultiSignals(args);
                
                for(String label: pSignals.keySet())
                {
                    PlotContainer plotContainer = new PlotContainer(
                        "FFT analysis for " + label,
                        idHandler.next(),
                        1);
                    
                    System.out.println("Handling label " + label);
                    addPlots(
                        plotContainer, 
                        pSignals.get(label),
                        args);
                    
                    if(!plotContainer.isEmpty())
                        containers.add(plotContainer);
                }
            }
            else
            {
                PlotContainer plotContainer = new PlotContainer(
                    "FFT analysis",
                    idHandler.next(),
                    1);

                addPlots(
                    plotContainer,
                    super.createSignals(args),
                    args);
                
                if(!plotContainer.isEmpty())
                    containers.add(plotContainer);
            }
            
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers,
                    createOutputFile(args)
                );
            }

        }
        catch (LedcException | MarshallException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }

    private void addPlots(PlotContainer plotContainer, Map<String, Signal> signals, String[] args) throws ChronosException, MarshallException
    {
        OrdinalContractor<BigDecimal> c = SigmaContractorFactory.BIGDECIMAL_THREE_DECIMALS;
            
        for(String attribute: signals.keySet())
        {
            System.out.println("Applying FFT for " + attribute);

            //Get the next signal for the attribute
            Signal<Comparable<? super Comparable>, Number> signal = convert(signals.get(attribute));

            double avgGap = signal
                .interMeasureGap()
                .stream()
                .mapToLong(l->l)
                .summaryStatistics()
                .getAverage();

            System.out.println("Mean inter-measure distance (units): " + avgGap);

            ComplexNumber[] fft = FourierAnalysis.fastFourierTransform(signal);

            double s = ((double)signal.size())/((double)fft.length);

            List<Pair<BigDecimal, Double>> fftData = new ArrayList<>();

            for(int i=1; i<= fft.length-1; i++)
            {
                fftData.add(new Pair<>
                (
                    c.get(BigDecimal.valueOf(s * i)),
                    SigmaContractorFactory
                        .BIGDECIMAL_TWO_DECIMALS
                        .get(BigDecimal.valueOf(fft[i].abs()))
                        .doubleValue())
                );
            }        

            Line<BigDecimal, Double> fftLine = new Line<>("fft", fftData);

            LinePlot<BigDecimal, Double> plot = new LinePlot<>(
                c,
                "FFT for " + attribute,
                fftLine
            );
            plot.setFixedLowerBound(0.0);

            plotContainer.addItem(plot);
        }
    }
}
