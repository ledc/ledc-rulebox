package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.algorithms.changedetection.MovingAverageDetector;
import be.ugent.ledc.chronos.algorithms.transform.MovingAverageSpikeTransform;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import java.util.List;

public class MovingAverageChangeDetectorHandler extends ChangeDetectionHandler
{
    private final Parameter<Integer> smoothingWindowParameter = new OptionalParameter<>
    (
        "smoothing-window",
        "--sw",
        (s) -> Integer.parseInt(s),
        "Size of the window used for smoothing. Default value is 10.",
        10
    )
    .addVerifier(i -> i > 0, "Smoothing window must have a strict positive size.");
    
    private final Parameter<Integer> differenceWindowParameter = new OptionalParameter<>
    (
        "difference-window",
        "--dw",
        (s) -> Integer.parseInt(s),
        "Size of the window used to compute differences. Default value is 3.",
        3
    )
    .addVerifier(i -> i > 0, "Difference window must have a strict positive size.");
    
    private final Parameter<Double> medianMultiplierParameter = new OptionalParameter<>
    (
        "multiplier",
        "--m",
        (s) -> Double.parseDouble(s),
        "Multiplier applied to the median difference to compute a threshold for differences. Default value is 2.",
        2.0
    )
    .addVerifier(i -> i > 0.0, "Difference window must have a strict positive size.");
    
    public MovingAverageChangeDetectorHandler()
    {
        super("mac", "Detect change points based on differences in the data after smootging with a moving average.");
        
        addParameter(smoothingWindowParameter);
        addParameter(differenceWindowParameter);
        addParameter(medianMultiplierParameter);
    }

    @Override
    public List<Comparable<? super Comparable>> findChanges(Signal<Comparable<? super Comparable>, Number> signal, String[] args) throws ChronosException, ParameterException
    {
        return new MovingAverageDetector<Comparable<? super Comparable>, Number>
        (
            new MovingAverageSpikeTransform<>
            (
                ParameterTools.extractParameter(smoothingWindowParameter, args),
                ParameterTools.extractParameter(differenceWindowParameter, args)
            ),
            ParameterTools.extractParameter(medianMultiplierParameter, args)
        )
        .changes(signal);
    }
}
