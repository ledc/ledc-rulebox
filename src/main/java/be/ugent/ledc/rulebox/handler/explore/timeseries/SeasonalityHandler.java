package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.algorithms.transform.seasonality.decomposition.Decomposition;
import be.ugent.ledc.chronos.algorithms.transform.seasonality.decomposition.SimpleDecomposer;
import be.ugent.ledc.chronos.algorithms.transform.smoothing.BasicSmootherWeightFunction;
import be.ugent.ledc.chronos.algorithms.transform.smoothing.SmootherWeightFunction;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.core.statistics.Statistics;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.Line;
import be.ugent.ledc.rulebox.io.result.plot.LinePlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SeasonalityHandler extends TimesHandler
{
    private final Parameter<Integer> cycleParameter = new Parameter<>
    (
        "cycle",
        "--c",
        true,
        (s) -> Integer.valueOf(s),
        "Size of the seasonality cycle size in number of measures."
    )
    .addVerifier(i -> i > 0, "Seasonality cycle must be strictly positive.");

    private final Flag multiplicativeFlag = new Flag
    (
        "multiplicative",
        "--m",
        "When activated, seasonality decomposition uses a multiplicative model instead of the default additive one."
    );
    
    private final Flag markOutliersFlag = new Flag
    (
        "mark-outliers",
        "--mo",
        "When activated, outliers are marked in the output. Outliers are points for which the residual signal diverts strongly from the median of the residual."
    );
    
    private final Flag isolateCycleFlag = new Flag
    (
        "isolate-cycle",
        "--ic",
        "When activated, the cyclic pattern is shown for one cycle instead of the entire signal."
    );
    
    private final Parameter<Double> iqrMultiplierParameter = new OptionalParameter<>
    (
        "iqr-multiplier",
        "--iqrm",
        s -> Double.valueOf(s),
        "When marking outliers, this multiplier is applied to the IQR to determine which points are outliers. Default value is 3.",
        3.0
    ).addVerifier(d -> d > 0, "Multiplier must be strict positive");

    private final Parameter<String> smoothingParameter = new OptionalParameter<>
    (
        "smoothing",
        "--s",
        s -> s,
        "Smoothing function to be used. Options are: "
        + Stream.of(SMOOTHERS).collect(Collectors.joining(",","","."))
        + "Default value is " + UNIFORM_SMOOTHING,
        UNIFORM_SMOOTHING
    ).addVerifier(
        s -> Stream.of(SMOOTHERS).anyMatch(sm -> sm.equals(s)),
        "Value for smoothing must be one of : "
        + Stream.of(SMOOTHERS).collect(Collectors.joining(",")));
    
    public static final String LINEAR_SMOOTHING = "linear";
    public static final String UNIFORM_SMOOTHING = "uniform";
    public static final String TRICUBE_SMOOTHING = "tricube";
    
    public static final String[] SMOOTHERS = new String[]
    {
        LINEAR_SMOOTHING,
        UNIFORM_SMOOTHING,
        TRICUBE_SMOOTHING
    };
    
    public SeasonalityHandler()
    {
        super(
            "seasonality",
            "Provides a seasonality decomposition (additive by default) in time series data."
        );
        
        addParameter(cycleParameter);
        addParameter(iqrMultiplierParameter);
        addParameter(smoothingParameter);
        
        addFlag(multiplicativeFlag);
        addFlag(markOutliersFlag);
        addFlag(isolateCycleFlag);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            //Initialize containers
            List<Container<?>> containers = new ArrayList<>();
            
            Integer cycle = ParameterTools.extractParameter(cycleParameter, args);
            boolean multi = ParameterTools.extractFlag(multiplicativeFlag, args);
            String smooth = ParameterTools.extractParameter(smoothingParameter, args);
            
            SmootherWeightFunction swf = null;
                
            switch(smooth)
            {
                case LINEAR_SMOOTHING:
                    swf = BasicSmootherWeightFunction.LINEAR;
                    break;
                case UNIFORM_SMOOTHING:
                    swf = BasicSmootherWeightFunction.UNIFORM;
                    break;
                case TRICUBE_SMOOTHING:
                    swf = BasicSmootherWeightFunction.TRICUBE;
                    break;
                default:
                    super.error("Unknown smoothing function " + smooth);
            }
            
            SimpleDecomposer<Comparable<? super Comparable>,Number> decomposer = new SimpleDecomposer<>
            (
                swf,
                multi
                    ? SimpleDecomposer.MULTIPLICATIVE
                    : SimpleDecomposer.ADDITIVE
            );
            
            if(super.isPartitioned(args))
            {
                Map<String, Map<String, Signal>> pSignals = super.createMultiSignals(args);
                
                for(String label: pSignals.keySet())
                {
                    PlotContainer plotContainer = new PlotContainer(
                        "Seasonality decomposition for " + label,
                        idHandler.next(),
                        3);
                    
                    System.out.println("Handling label " + label);
                    addPlots(
                        plotContainer, 
                        pSignals.get(label),
                        decomposer,
                        cycle,
                        args);
                    
                    if(!plotContainer.isEmpty())
                        containers.add(plotContainer);
                }
            }
            else
            {
                PlotContainer plotContainer = new PlotContainer(
                    "Seasonality decomposition",
                    idHandler.next(),
                    3);

                addPlots(
                    plotContainer,
                    super.createSignals(args),
                    decomposer,
                    cycle,
                    args);
                
                if(!plotContainer.isEmpty())
                    containers.add(plotContainer);
            }
            
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers,
                    createOutputFile(args)
                );
            }
        }
        catch (LedcException | MarshallException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    private List<Comparable<? super Comparable>> locateOutliers(Signal<Comparable<? super Comparable>, Number> noise, double iqrMultiplier)
    {
        System.out.println("Searching outliers...");
        
        List<Double> noiseData = noise
            .entrySet()
            .stream()
            .filter(e -> e.getValue() != null)
            .map(e -> e.getValue().doubleValue()).collect(Collectors.toList());
        
        HashMap<String, Double> quartiles = Statistics.quartiles(noiseData);
        
        if(quartiles.isEmpty()
            || quartiles.get("Q3") == null
            || quartiles.get("Q1") == null
            || Double.compare(quartiles.get("Q3") - quartiles.get("Q1"), 0.0) == 0
        )
            return new ArrayList<>();
        
        double q1 = quartiles.get("Q1");
        double q3 = quartiles.get("Q3");
        
        //Compute IQR
        Double iqr = q3 - q1;
        
        System.out.println("Noise Median: " + quartiles.get("Q2"));
        System.out.println("Noise IQR: " + iqr);
        System.out.println("Accepted noise lower bound: " + (q1 - iqrMultiplier * iqr));
        System.out.println("Accepted noise higher bound: " + (q3 + iqrMultiplier * iqr));
        
        List<Comparable<? super Comparable>> outliers = noise
            .entrySet()
            .stream()
            .filter(e -> e.getValue() != null)
            .filter(e ->
                (e.getValue().doubleValue() < q1 - iqrMultiplier * iqr) //Lower outliers
            ||  (e.getValue().doubleValue() > q3 + iqrMultiplier * iqr))//Higher outliers
            .map(e -> e.getKey())
            .collect(Collectors.toList());
        
        System.out.println("Found " + outliers.size() + " outliers...");
        
        return outliers;
    }

    private void addPlots(PlotContainer plotContainer, Map<String, Signal> signals, SimpleDecomposer<Comparable<? super Comparable>,Number> decomposer, int cycle, String[] args) throws MarshallException, BinderPoolException, ParameterException, FileNotFoundException, DataReadException, ChronosException
    {
        for(String attribute: signals.keySet())
        {
            System.out.println("Creating seasonality decomposition for " + attribute);

            Signal<Comparable<? super Comparable>, Number> pSignal = convert(signals.get(attribute));

            double avgGap = pSignal
                .interMeasureGap()
                .stream()
                .mapToLong(l->l)
                .summaryStatistics()
                .getAverage();

            System.out.println("Mean inter-measure distance (units): " + avgGap);

            Decomposition decomposition = decomposer.decompose(
                pSignal,
                cycle);

            List<Comparable<? super Comparable>> outliers = ParameterTools.extractFlag(markOutliersFlag, args)
                ? locateOutliers(
                    decomposition.getNoise(),
                    ParameterTools.extractParameter(iqrMultiplierParameter, args)
                )
                : new ArrayList<>();

            Line<Comparable<? super Comparable>, Number> original = new Line<>(
                "Original",
                toList(pSignal));

            Line<Comparable<? super Comparable>, Number> trend = new Line<>(
                "Trend",
                toList(round(decomposition.getTrend())));

            trend.setEmphasize(true);
            trend.setSymbols(false);

            for(Comparable<? super Comparable> outlier: outliers)
            {
                original.mark(outlier, "outlier");
            }

            LinePlot<Comparable<? super Comparable>, Number> trendPlot = new LinePlot<>
            (
                super.getTimeContractor(args),
                "Trend plot for " +  attribute,
                original,
                trend
            );

            Signal<Comparable<? super Comparable>, Number> seas = round(decomposition.getSeasonal());

            if(ParameterTools.extractFlag(isolateCycleFlag, args))
            {
                seas = seas.subSignal(
                    seas.start(),
                    seas.jumpRight(seas.start(), cycle)
                );
                
                System.out.println(seas.indexSet().size());
            }
            
            double maxSeas = seas
                .entrySet()
                .stream()
                .map(e -> e.getValue())
                .mapToDouble(n -> n.doubleValue())
                .max()
                .getAsDouble();

            double minSeas = seas
                .entrySet()
                .stream()
                .map(e -> e.getValue())
                .mapToDouble(n -> n.doubleValue())
                .min()
                .getAsDouble();

            Line<Comparable<? super Comparable>, Number> seasLine = new Line<>(
                "Seasonality",
                toList(seas)
            );

            seasLine.setSymbols(false);

            LinePlot<Comparable<? super Comparable>, Number> seasonPlot = new LinePlot<>
            (
                super.getTimeContractor(args),
                "Seasonality plot for " + attribute,
                seasLine
            );

            seasonPlot.setFixedLowerBound(2 * minSeas);
            seasonPlot.setFixedUpperBound(2 * maxSeas);

            Line<Comparable<? super Comparable>, Number> residualLine = new Line<>(
                "Residual",
                toList(round(decomposition.getNoise()))
            );

            for(Comparable<? super Comparable> outlier: outliers)
            {
                residualLine.mark(outlier, "outlier");
            }

            residualLine.setSymbols(false);

            LinePlot<Comparable<? super Comparable>, Number> noisePlot = new LinePlot<>
            (
                super.getTimeContractor(args),
                "Residual plot for " + attribute,
                residualLine
            );

            //Add plots
            plotContainer.addItem(trendPlot);
            plotContainer.addItem(seasonPlot);
            plotContainer.addItem(noisePlot);

        }
    }
    
}
