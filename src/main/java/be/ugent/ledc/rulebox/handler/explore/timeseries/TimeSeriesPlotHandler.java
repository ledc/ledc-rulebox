package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.Line;
import be.ugent.ledc.rulebox.io.result.plot.LinePlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TimeSeriesPlotHandler extends TimesHandler
{
    public static final String NAME = "plot";
      
    private final Flag noInterpolationFlag = new Flag
    (
        "no-interpolation",
        "--ni",
        "When activated, no interpolation is applied in plots."
    );
    
    private final Flag forceZeroFlag = new Flag
    (
        "force-zero",
        "--fz",
        "When activated, Y-axis is forced to start at zero."
    );
    
    private final Parameter<Integer> columnsParameter = new OptionalParameter<>(
        "columns",
        "--c",
        (s) -> Integer.valueOf(s),
        "Number of plots that go in one row (default is 2).",
        2
    );
        
    public TimeSeriesPlotHandler()
    {
        super(
            NAME,
            "Visually inspect time series data present in a dataset."
        );
        
        addFlag(noInterpolationFlag);
        addFlag(forceZeroFlag);
        addParameter(columnsParameter);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);
            
            //Initialize containers
            List<Container<?>> containers = new ArrayList<>();
            
            Integer columns = ParameterTools.extractParameter(columnsParameter, args);
            
            if(super.isPartitioned(args))
            {
                Map<String, Map<String, Signal>> pSignals = super.createMultiSignals(args);
                
                for(String label: pSignals.keySet())
                {
                    PlotContainer plotContainer = new PlotContainer(
                        "Time series plots for " + label,
                        idHandler.next(),
                        columns);
                    
                    addPlots(plotContainer, pSignals.get(label), args);
                    
                    if(!plotContainer.isEmpty())
                        containers.add(plotContainer);
                }
            }
            else
            {
                PlotContainer plotContainer = new PlotContainer(
                    "Time series plots",
                    idHandler.next(),
                    columns);

                addPlots(plotContainer, super.createSignals(args), args);
                
                if(!plotContainer.isEmpty())
                    containers.add(plotContainer);
            }
            
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers,
                    createOutputFile(args)
                );
            }
        }
        catch (ParameterException |  BinderPoolException | DataReadException | MarshallException | ChronosException ex)
        {
            System.err.println(ex.getMessage());
        }
        catch (IOException  ex)
        {
            Logger.getLogger(TimeSeriesPlotHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addPlots(PlotContainer plotContainer, Map<String, Signal> signals, String[] args) throws ChronosException, BinderPoolException, IOException, ParameterException, FileNotFoundException, DataReadException, MarshallException
    {
        for(String attribute: signals.keySet())
        {   
            Signal<Comparable<? super Comparable>, Comparable> signal = signals.get(attribute);

            Line<Comparable<? super Comparable>, Number> line = new Line<>
            (
                attribute,
                toList(convert(signal))
            );

            LinePlot<Comparable<? super Comparable>, Number> linePlot = new LinePlot<>
            (
                getTimeContractor(args),
                "Series plot " + attribute,
                line
            );

            if(ParameterTools.extractFlag(noInterpolationFlag, args))
                line.setInterpolate(false);

            if(ParameterTools.extractFlag(forceZeroFlag, args))
                linePlot.setFixedLowerBound(0);

            plotContainer.addItem(linePlot);
        }
    }
}
