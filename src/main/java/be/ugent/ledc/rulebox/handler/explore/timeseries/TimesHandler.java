package be.ugent.ledc.rulebox.handler.explore.timeseries;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.chronos.datastructures.TemporalDataset;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderBox;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.handler.util.IdHandler;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class TimesHandler extends DataHandler
{
    private final Parameter<Set<String>> onlyOnParameter = new Parameter<>(
        "only",
        "--o",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Analyse only for these attributes (comma-separated list)"
    );
    
    private final Parameter<Set<String>> excludeParameter = new Parameter<>(
        "exclude",
        "--e",
        false,    
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Exclude these attributes from analysis (comma-separated list)"
    );

    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (file) -> file.getName().endsWith(".html") || file.getName().endsWith(".xml"),
            "Outputfile must be an .html or .xml file");
    
    protected final IdHandler idHandler = new IdHandler();
    
    public TimesHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(onlyOnParameter);
        addParameter(excludeParameter);
        addParameter(outputFileParameter);
    }
    
    protected File createOutputFile(String[] args) throws ParameterException
    {
        return ParameterTools.extractParameter(outputFileParameter, args);
    }

    protected Map<String, Signal> createSignals(String[] args) throws ParameterException, BinderPoolException, FileNotFoundException, DataReadException, ChronosException
    {        
        String bName = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        if(!BinderPool.getInstance().exists(bName))
            throw new BinderPoolException("Unknown data binder name '"
                + bName
                + "'");
        
        BinderBox<?> bb = BinderPool.getInstance().getBinderBox(bName);
        
        if(!bb.isTemporal())
            throw new BinderPoolException("Binder '"+ bName+ "' is not temporal");

        //Read dataset
        System.out.println("Reading data...");
        ContractedDataset data = BinderPool
            .getInstance()
            .data(ParameterTools.extractParameter(getBinderNameParameter(), args));

        Set<String> selection = data
            .getContract()
            .getAttributes();
        
        //Clean
        selection.remove(bb.getTemporalInfo().getTimeAttribute());
        if(bb.isTemporalPartitioned())
            selection.remove(bb.getTemporalInfo().getPartitionAttribute());

        Set<String> onlyOn  = ParameterTools.extractParameter(onlyOnParameter, args);
        Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);

        if(onlyOn != null)
            selection = SetOperations.intersect(selection, onlyOn);
        if(exclude != null)
            selection = SetOperations.difference(selection, exclude);

        return createSignals(selection, data, bb);
    }
    
    protected Map<String, Map<String, Signal>> createMultiSignals(String[] args) throws ParameterException, BinderPoolException, FileNotFoundException, DataReadException, ChronosException
    {        
        String bName = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        if(!BinderPool.getInstance().exists(bName))
            throw new BinderPoolException("Unknown data binder name '"
                + bName
                + "'");
        
        BinderBox<?> bb = BinderPool.getInstance().getBinderBox(bName);
        
        if(!bb.isTemporal())
            throw new BinderPoolException("Binder '"+ bName+ "' is not temporal");

        //Read dataset
        System.out.println("Reading data...");
        ContractedDataset data = BinderPool
            .getInstance()
            .data(ParameterTools.extractParameter(getBinderNameParameter(), args));

        Set<String> selection = data
            .getContract()
            .getAttributes();

        Set<String> onlyOn  = ParameterTools.extractParameter(onlyOnParameter, args);
        Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);

        if(onlyOn != null)
            selection = SetOperations.intersect(selection, onlyOn);
        if(exclude != null)
            selection = SetOperations.difference(selection, exclude);

        return createPartitionSignals(selection, data, bb);
    }
    
    protected boolean isPartitioned(String[] args) throws ParameterException, BinderPoolException, FileNotFoundException, DataReadException, ChronosException
    {        
        String bName = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        if(!BinderPool.getInstance().exists(bName))
            throw new BinderPoolException("Unknown data binder name '"
                + bName
                + "'");
        
        BinderBox<?> bb = BinderPool.getInstance().getBinderBox(bName);
        
        if(!bb.isTemporal())
            throw new BinderPoolException("Binder '"+ bName+ "' is not temporal");

        return bb.isTemporalPartitioned();

    }
    
    protected OrdinalContractor getTimeContractor(String[] args) throws ParameterException, BinderPoolException, FileNotFoundException, DataReadException, ChronosException
    {        
        String bName = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        if(!BinderPool.getInstance().exists(bName))
            throw new BinderPoolException("Unknown data binder name '"
                + bName
                + "'");
        
        BinderBox<?> bb = BinderPool.getInstance().getBinderBox(bName);
        
        if(!bb.isTemporal())
            throw new BinderPoolException("Binder '"+ bName+ "' is not temporal");

        return bb.getTemporalInfo().getContractor();

    }
    
    protected List<Pair<Comparable<? super Comparable>, Number>> toList(Signal<Comparable<? super Comparable>, Number> pSignal)
    {
        List<Pair<Comparable<? super Comparable>, Number>> pairList = new ArrayList<>();
        
        for(Comparable<? super Comparable> x: pSignal.indexSet())
        {
            if(x != null)
            {
                pairList.add(new Pair<>(x, (Number)pSignal.get(x)));
            }
        }
        
        return pairList;
    }
    
    protected Signal<Comparable<? super Comparable>, Number> convert(Signal<Comparable<? super Comparable>, Comparable> signal) throws ChronosException
    {
        Signal<Comparable<? super Comparable>, Number> converted = new Signal<>(signal.getIndexContractor());
        
        for(Comparable<? super Comparable> x: signal.indexSet())
        {
            if(x != null)
            {
                converted.put(x, (Number)signal.get(x));
            }
        }
        
        return converted;
    }
    
    protected int decimals(List<Double> values)
    {
        double min = values
            .stream()
            .filter(v -> v != null)
            .mapToDouble(e -> e)
            .min()
            .getAsDouble();
        
        double max = values
            .stream()
            .filter(v -> v != null)
            .mapToDouble(e -> e)
            .max()
            .getAsDouble();
        
        return (int) (max == min
            ? 2 + (-Math.log10(max))
            : 2 + (-Math.log10(max - min)));
    }
    
    protected Signal<Comparable<? super Comparable>, Double> round(Signal<Comparable<? super Comparable>, Double> pSignal) throws ChronosException
    {
        int decimals = decimals(pSignal
            .entrySet()
            .stream()
            .map(e -> e.getValue())
            .collect(Collectors.toList())
        );
        
        Signal<Comparable<? super Comparable>, Double> rounded = new Signal<>(pSignal.getIndexContractor());
        
        for(Comparable<? super Comparable> index: pSignal.indexSet())
        {
            rounded.put(
                index,
                pSignal.get(index) == null
                    ? null
                    : BigDecimal
                        .valueOf(pSignal.get(index))
                        .setScale(decimals, BigDecimal.ROUND_HALF_DOWN)
                        .doubleValue()
            );
        }
        
        return rounded;
    }

    private Map<String, Signal> createSignals(Set<String> selection, ContractedDataset data, BinderBox<?> bb) throws ChronosException, DataReadException, FileNotFoundException
    {
        Map<String, Signal> signals = new HashMap<>();
        
        TemporalDataset temporalData = bb.temporalData();
        
        for(String attribute: selection)
        {
            if(!isNumeric(data.getContract().getAttributeContract(attribute)))
            {
                System.out.println("Ignoring attribute '" + attribute + "': non-numeric data");
                continue;
            }

            signals.put(attribute, temporalData.getAttributeSignal(attribute));
        }
        
        return signals;
    }

    private Map<String, Map<String, Signal>> createPartitionSignals(Set<String> selection, ContractedDataset data, BinderBox<?> bb) throws ChronosException, DataReadException, FileNotFoundException
    {
        Map<String, Map<String, Signal>> signals = new HashMap<>();
        
        Map<String, TemporalDataset<Comparable<? super Comparable>>> tpData = bb.temporalPartitionedData();
        
        for(String attribute: selection)
        {
            if(!isNumeric(data.getContract().getAttributeContract(attribute)))
            {
                System.out.println("Ignoring attribute '" + attribute + "': non-numeric data");
                continue;
            }
            
            for(String label: tpData.keySet())
            {
                if(!signals.containsKey(label))
                    signals.put(label, new HashMap<>());
                
                signals
                    .get(label)
                    .put(
                        attribute,
                        tpData
                            .get(label)
                            .getAttributeSignal(attribute)
                    );
            }
        
        }
        return signals;
    
    }
    
}
