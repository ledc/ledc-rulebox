package be.ugent.ledc.rulebox.handler.reasoning;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.jdbc.agents.PersistentDatatype;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.reasoning.CandidateKeyGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.ConstraintHandler;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import java.io.File;
import java.io.FileNotFoundException;

public class CandidateKeyHandler extends ConstraintHandler
{   
    public static final String NAME = "candkeys";
            
    public CandidateKeyHandler()
    {
        super(NAME, "finds all candidate keys");
    }
    
    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            File file = ParameterTools.extractParameter(getConstraintFileParameter(), args);
            
            Rubix rubix = RubixReader.readRubix(file);
            
            RuleSet<Dataset,FD> fds = rubix.getAllFunctionalDependencies();
  
            TableSchema tableSchema = new TableSchema();
            tableSchema.setName("relation");

            for(String attribute: rubix.allAttributes())
            {
                tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
            }
            
            CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
            
            System.out.println("Relation attributes: " + rubix.allAttributes());
            System.out.println("Candidate keys:");
            ckg.generate(fds).forEach(System.out::println);
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
