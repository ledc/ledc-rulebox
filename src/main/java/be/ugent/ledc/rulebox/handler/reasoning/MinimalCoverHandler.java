package be.ugent.ledc.rulebox.handler.reasoning;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.ConstraintHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import be.ugent.ledc.rulebox.io.rubix.RubixWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.stream.Collectors;

public class MinimalCoverHandler extends ConstraintHandler
{
    public static final String NAME = "mincover";
    
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (f) -> f.getName().toLowerCase().endsWith(".rbx"),
            "Output file extension must be .rbx"
        );
    
    private final Flag noReductionFlag = new Flag(
        "no-reduction",
        "--nr",
        "Prevents the minimal cover generator from reducing the left hand side of FDs."
    );
    
    private final Flag keepRedundantFlag = new Flag(
        "keep-redundant",
        "--kr",
        "Prevents the minimal cover generator from removing redundant FDs."
    );
            
    public MinimalCoverHandler()
    {
        super(NAME, "produces a minimal cover of FDs");

        addParameter(outputFileParameter);
        
        addFlag(noReductionFlag);
        addFlag(keepRedundantFlag);
    }
    
    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            File file = ParameterTools.extractParameter(getConstraintFileParameter(), args);
            
            System.out.println("Reading constraints");
            Rubix rubix = RubixReader.readRubix(file);
            
            RuleSet<Dataset,FD> fds = rubix.getAllFunctionalDependencies();
  
            System.out.println("Transforming FDs");
            
            if(ParameterTools.extractFlag(keepRedundantFlag, args))
                System.out.println("Warning: you chose to keep redundant FDs. The transformed set might not be a minimal cover.");
            
            if(ParameterTools.extractFlag(noReductionFlag, args))
                System.out.println("Warning: you chose to not reduce FDs. The transformed set might not be a minimal cover.");
            
            RuleSet<Dataset,SimpleFD> transformedFDs = new MinimalCoverGenerator
            (
                !ParameterTools.extractFlag(keepRedundantFlag, args),
                !ParameterTools.extractFlag(noReductionFlag, args)
            ).generate(fds);
            
            //Write the result
            System.out.println("Writing rubix file");
            Rubix modifiedRubix = new Rubix.RubixBuilder()
                .withSigmaRules(rubix.getAllSigmaRules())
                .withFunctionalDependencies(
                    new RuleSet<>(transformedFDs
                        .stream()
                        .map(sfd -> new FD(sfd.getLeftHandSide(),sfd.getRightHandSide()))
                        .collect(Collectors.toSet())))
                .withPropertyAssertions(rubix.getAllPropertyAssertions())
                .withInclusionDependencies(rubix.getAllInclusionDependencies())
                .withSigmaCostFunctions(rubix.getCostFunctions())
                .withMatcher(rubix.getMatcher())
                .build();
                
            
            RubixWriter.writeRubix(
                modifiedRubix,
                ParameterTools.extractParameter(outputFileParameter, args));
            
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
