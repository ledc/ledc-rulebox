package be.ugent.ledc.rulebox.handler.reasoning;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.ConstraintHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import be.ugent.ledc.rulebox.io.rubix.RubixWriter;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFWrapper;
import java.io.File;
import java.io.FileNotFoundException;

public class SufficientSetHandler extends ConstraintHandler
{
    public static final String NAME = "fcf";

    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (f) -> f.getName().toLowerCase().endsWith(".rbx"),
            "Output file extension must be .rbx"
        );
    
    
    public SufficientSetHandler()
    {
        super(NAME, "Applies the FCF algorithm to compile a sufficient set of selection rules.");
        addParameter(outputFileParameter);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            File file = ParameterTools.extractParameter(getConstraintFileParameter(), args);
            
            System.out.println("Reading constraints");
            Rubix rubix = RubixReader.readRubix(file);
            
            System.out.println("Computing sufficient set with FCF");
            SufficientSigmaRuleset sufficientRules = generate(rubix);
            
            //Write the result
            System.out.println("Writing rubix file");
            Rubix modifiedRubix = new Rubix.RubixBuilder()
                .withSigmaRules(sufficientRules)
                .withFunctionalDependencies(rubix.getAllFunctionalDependencies())
                .withPropertyAssertions(rubix.getAllPropertyAssertions())
                .withInclusionDependencies(rubix.getAllInclusionDependencies())
                .withSigmaCostFunctions(rubix.getCostFunctions())
                .withMatcher(rubix.getMatcher())
                .build();
            
            RubixWriter.writeRubix(
                modifiedRubix,
                ParameterTools.extractParameter(outputFileParameter, args));
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    public SufficientSigmaRuleset generate(Rubix rubix)
    {
        //Build the sufficient set, not silent
        return new FCFWrapper(false).build(rubix.getAllSigmaRules());
    }

}
