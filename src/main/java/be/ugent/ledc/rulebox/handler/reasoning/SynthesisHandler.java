package be.ugent.ledc.rulebox.handler.reasoning;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.jdbc.agents.PersistentDatatype;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.normalization.ThreeNFSynthesis;
import be.ugent.ledc.fundy.algorithms.reasoning.CandidateKeyGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.ConstraintHandler;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Map;
import java.util.stream.Collectors;

public class SynthesisHandler extends ConstraintHandler
{   
    public SynthesisHandler()
    {
        super("3nf", "decomposes a relation to 3NF, given a set of FDs");
    }
    
    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            File file = ParameterTools.extractParameter(getConstraintFileParameter(), args);
            
            System.out.println("Reading constraints");
            Rubix rubix = RubixReader.readRubix(file);
            
            System.out.println("Computing 3NF synthesis");

            TableSchema tableSchema = new TableSchema();
            tableSchema.setName("relation");

            for(String attribute: rubix.allAttributes())
            {
                tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
            }
            
            //Decomposition
            Map<TableSchema, RuleSet<Dataset,FD>> decomposition = new ThreeNFSynthesis()
                .decompose(tableSchema, rubix.getAllFunctionalDependencies());
            
            PrintStream out = System.out;
                
            out.println("3NF synthesis:");
            
            for(TableSchema ts: decomposition.keySet())
            {
                out.println("Sub-relation: "
                    + ts.getName()
                    + ts.getAttributeNames().stream().collect(Collectors.joining(",", "[", "]"))
                );
                
                out.println("Candidate keys: " + new CandidateKeyGenerator(ts)
                    .generate(decomposition.get(ts)));
                
                out.println("FDs:" + (decomposition.get(ts).isEmpty() ? "none" : ""));
                
                for(FD fd: decomposition.get(ts))
                {
                    out.println(fd);
                }
                out.println("");
            }
        }
        catch (LedcException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}
