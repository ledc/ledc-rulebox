package be.ugent.ledc.rulebox.handler.repair;

import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.assertion.AbstractAssertor;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.core.RepairException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AssertionsRepairHandler extends RepairHandler
{
    public AssertionsRepairHandler()
    {
        super("assertions", "repairs assertions by applying standardization to attributes.");
    }

    @Override
    public Dataset repair(ContractedDataset data, Set<String> attributes, Rubix rubix, String[] args) throws RepairException, ParameterException, DependencyGraphException, PropertyParseException
    {
        List<DataObject> repairObjects = new ArrayList<>();
        
        String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        List<AbstractAssertor> repairable = rubix
            .getPropertyAssertions(binder)
            .getRules()
            .stream()
            .filter(aa -> aa.canClean())
            .collect(Collectors.toList());
        
        rubix
        .getPropertyAssertions(binder)
        .getRules()
        .stream()
        .filter(aa -> !aa.canClean())
        .map(aa -> "Warning: cannot repair "
                    + aa.toString()
                    + ". No convertor found.")
        .forEach(System.out::println);
        
        for(DataObject o: data)
        {
            //Copy
            DataObject r = new DataObject().concat(o);
            
            for(AbstractAssertor aa: repairable)
            {
                try
                {
                    r = aa.clean(r);
                }
                catch(PiException ex)
                {
                    throw new RepairException(ex);
                }
            }
            
            repairObjects.add(r);
        }
        
        return new SimpleDataset(repairObjects);
        
    }
}
