package be.ugent.ledc.rulebox.handler.repair;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.indy.algorithms.repair.IndyRepair;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.DamerauDistanceCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.DistanceCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunctionWrapper;
import be.ugent.ledc.sigma.repair.cost.models.IterableCostModel;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class INDRepairHandler extends RepairHandler
{
    private final Flag streamingFlag = new Flag
    (
        "stream-reference-data",
        "--srd",
        "When possible, stream data from the reference source"
    );
    
    private final Flag keepConstantCostFlag = new Flag
    (
        "keep-constant-cost",
        "--kcc",
        "By default, constant cost functions will be overwritten with distance-based cost functions. Use this flag to keep constant cost functions."
    );
    
    public INDRepairHandler()
    {
        super("ind", "Applies a lowest-cost search for repairs of inclusion dependencies. "
        + "This repair engine allows non-constant cost models to be specified in the rubix file.");
        
        addFlag(streamingFlag);
        addFlag(keepConstantCostFlag);
    }

    @Override
    public Dataset repair(ContractedDataset data, Set<String> attributes, Rubix rubix, String[] args) throws LedcException
    {
        try
        {
            //Build ruleset of INDS
            INDRuleset indRuleset = rubix
                .getINDRuleset(
                    ParameterTools.extractParameter(getBinderNameParameter(), args),
                    ParameterTools.extractFlag(streamingFlag, args)
                )
                .leftProject(attributes);
            
            return new IndyRepair(
                indRuleset,     //Pass the ruleset
                createCostModel(//Pass the cost model
                    ParameterTools.extractFlag(keepConstantCostFlag, args),
                    indRuleset,
                    rubix,
                    ParameterTools.extractParameter(getBinderNameParameter(), args),
                    ParameterTools.extractFlag(streamingFlag, args)
                )
            )  
            .repair(data);  //Repair
        }
        catch(BinderPoolException | IOException | DataReadException | INDException ex)
        {
            throw new RepairException(ex);
        }
    }
    
    public IterableCostModel createCostModel(boolean keep, INDRuleset rules, Rubix rubix, String binder, boolean streaming) throws LedcException, FileNotFoundException
    {   
        Map<String, SigmaContractor<?>> contractors = rubix
            .getINDRuleset(binder, streaming)
            .getLeftContractors();
        
        Map<String, IterableCostFunction> mapping = rubix
                .getCostFunctions()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(e -> e.getKey(),
                        e -> new IterableCostFunctionWrapper(e.getValue() instanceof ConstantCostFunction
                            ? keep
                                ? e.getValue()
                                : contractors
                                    .get(e.getKey())
                                    .name().equals(SigmaContractorFactory.STRING.name())
                                        ? new DamerauDistanceCostFunction()
                                        : new DistanceCostFunction((OrdinalContractor<?>)contractors.get(e.getKey()))
                            : e.getValue())
                ));
        
        return new IterableCostModel(mapping);
    }
    
}
