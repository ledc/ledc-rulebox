package be.ugent.ledc.rulebox.handler.repair;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.indy.algorithms.verification.ReferenceRegistry;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.sigma.datastructures.fd.PartialKey;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.repair.ParkerRepair;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.sigma.repair.bounding.EnumeratedBounding;
import be.ugent.ledc.sigma.repair.bounding.EnumeratedOffsetBounding;
import be.ugent.ledc.sigma.repair.bounding.RangedBounding;
import be.ugent.ledc.sigma.repair.cost.models.ParkerModel;
import be.ugent.ledc.sigma.repair.selection.FrequencyRepairSelection;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFWrapper;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParkerRepairHandler extends RepairHandler
{    
    private final Parameter<Set<String>> partialKeyParameter = new OptionalParameter<>(
        "partial-key",
        "--pk",
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "The set of attributes to be considered as the partial key. "
        + "FDs with LHS equal to (or super set of) this partial key will be repaired.",
        new HashSet<>()
    );
    
    private final Flag forceBoundingFlag = new Flag(
        "force-bounding",
        "--fb",
        "A bounding strategy is applied to all attributes, not just the ones without domain constraints."
    );
       
    private final Flag frequencySelectionFlag = new Flag(
        "frequency-selection",
        "--fs",
        "When activated, repair selection from the minimal cost repairs is based on frequency."
    );
    
    private final Flag rangedBoundingFlag = new Flag(
        "ranged-bounding",
        "--rb",
        "Attributes with ordinal datatype and no domain constraints are bounded to a range between the observed minimal and maximal value."
    );
    
    private final Flag fcfFlag = new Flag(
        "fcf",
        "--fcf",
        "Applies the FCF algorithm prior to repair. By default, it is assumed that selection rules in the rubix file are sufficient.");
    
    private final Parameter<Integer> boundingOffsetParameter = new OptionalParameter<>(
        "bounding-offset",
        "--bo",
        (s) -> Integer.parseInt(s),
        "Applies an offset to increase the search range for repairs when applying a bounding strategy. "
            + "Default offset is 0.",
        0
    );
    
    private final Parameter<Integer> boundingCapParameter = new OptionalParameter<>(
        "bounding-cap",
        "--bc",
        (s) -> Integer.parseInt(s),
        "When using ranged bounding, this parameter limits the number of permitted values. "
        + "When values is surpassed, uniform jumps across the entire range are used. "
        + "Default cap is 500.",
        500
    );
    
    public ParkerRepairHandler()
    {
        super("parker", "Applies the Parker repair engine to resolve violations of selection rules, inclusion dependencies and "
        + "a partial key simultaneously. "
        + "This repair engine allows non-constant cost models to be specified in the rubix file.");
        
        addParameter(partialKeyParameter);
        addParameter(boundingOffsetParameter);
        addParameter(boundingCapParameter);
        addFlag(forceBoundingFlag);
        addFlag(rangedBoundingFlag);
        addFlag(frequencySelectionFlag);
        addFlag(fcfFlag);
    }

    @Override
    public Dataset repair(ContractedDataset data, Set<String> attributes, Rubix rubix, String[] args) throws LedcException
    {   
        //Extract parameter vamies
        Set<String> partialKey = ParameterTools.extractParameter(partialKeyParameter, args);
        
        String binderName = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        boolean applyFcf = ParameterTools.extractFlag(fcfFlag, args);
        
        try
        {
            //Get selection rules
            SigmaRuleset selectionRules = rubix
                .getSigmaRules(binderName)
                .project(attributes);
            
            //Get ind ruleset
            INDRuleset inds = rubix
                .getINDRuleset(binderName, true)
                .leftProject(attributes);
            
            //Case 1: there are no inds
            if(inds.isEmpty())
            {
                //Compose a sufficient set
                SufficientSigmaRuleset sufficientSet = applyFcf
                    ? new FCFWrapper().build(selectionRules)
                    : SufficientSigmaRuleset.create(selectionRules);
                
                return applyParker(
                    data,
                    sufficientSet,
                    rubix,
                    partialKey,
                    args);
            }
            //Case 2: all INDs have constant conditions or not all variable conditions are on the partial key
            else if(inds
                .stream()
                .allMatch(ind -> ind.hasConstantCondition()
                    || ind
                    .getCondition()
                    .getVariableAtoms()
                    .stream()
                    .anyMatch(atom -> !partialKey.contains(atom.getRightAttribute()))
                )    
            )
            {
                SigmaRuleset merged = selectionRules;
                
                INDRuleset filteredINDs = inds.filter(ind -> ind.hasConstantCondition());
                
                if(filteredINDs.size() < inds.size())
                {
                    System.out.println("Warning: INDs conditioned on attributes other than "
                    + "the partial key are ignored during Parker repair...");
                }
                
                for(IND ind: filteredINDs)
                {
                    //Turn the ind into selection rules
                    SigmaRuleset indAsSelectionRules = ReferenceRegistry
                        .getInstance()
                        .getReferenceManager(ind)
                        .buildSigmaRuleset(
                            new DataObject(),
                            ind,
                            data.getContract());
                    
                    //Merge
                    merged = merged.merge(indAsSelectionRules);
                }
                
                //Compose a sufficient set
                SufficientSigmaRuleset sufficientSet = new FCFWrapper().build(merged);

                return applyParker(
                    data,
                    sufficientSet,
                    rubix,
                    partialKey,
                    args);
            }
            //Case 3: some INDs have variable conditions, all on the partial key
            else
            {
                INDRuleset filteredINDs = inds.filter(ind -> ind
                    .getCondition()
                    .getVariableAtoms()
                    .stream()
                    .allMatch(atom -> partialKey.contains(atom.getRightAttribute()))
                );
                
                if(filteredINDs.size() < inds.size())
                {
                    System.out.println("Warning: INDs conditioned on attributes other than "
                    + "the partial key are ignored during Parker repair...");
                }

                //Prepare a clean dataset
                Dataset cleaned = new ContractedDataset(data.getContract());

                ContractedDataset rowsWithNullsInKey = new ContractedDataset(data.getContract());
                
                data
                    .stream()
                    .filter(o -> partialKey.stream().anyMatch(a -> o.get(a) == null))
                    .forEach(rowsWithNullsInKey::addDataObject);
                
                if(rowsWithNullsInKey.getSize() > 0)
                {
                    SufficientSigmaRuleset sufficientSet = applyFcf
                    ? new FCFWrapper().build(selectionRules)
                    : SufficientSigmaRuleset.create(selectionRules);
                
                    cleaned = applyParker(
                        rowsWithNullsInKey,
                        sufficientSet,
                        rubix,
                        new HashSet<>(),
                        args);
                }
                
                Map<DataObject, List<DataObject>> classes = data
                    .stream()
                    .filter(o -> partialKey.stream().noneMatch(a -> o.get(a) == null))
                    .collect(Collectors.groupingBy(o -> o.project(partialKey)));
                
                for(Map.Entry<DataObject, List<DataObject>> nextCase: classes.entrySet())
                {
                    ContractedDataset localDataset = new ContractedDataset(data.getContract());
                    
                    //Make a dataset
                    nextCase
                        .getValue()
                        .stream()
                        .forEach(localDataset::addDataObject);
                    
                    SigmaRuleset merged = selectionRules;
                
                    for(IND ind: inds)
                    {
                        //Turn the ind into selection rules
                        SigmaRuleset indAsSelectionRules = ReferenceRegistry
                            .getInstance()
                            .getReferenceManager(ind)
                            .buildSigmaRuleset(
                                nextCase.getKey(),
                                ind,
                                data.getContract());

                        //Merge
                        merged = merged.merge(indAsSelectionRules);
                    }

                    //Compose a sufficient set
                    SufficientSigmaRuleset sufficientSet = new FCFWrapper().build(merged);
                    
                    //Apply parker and add the cleaned objects
                    applyParker(
                        localDataset,
                        sufficientSet,
                        rubix,
                        partialKey,
                        args)
                    .stream()
                    .forEach(cleaned::addDataObject);   
                }
                
                //Finally, return the cleaned dataset
                return cleaned;
            }
            

        }
        catch(BinderPoolException | IOException | DataReadException | INDException ex)
        {
            throw new RepairException(ex);
        }
    }
    
    private Dataset applyParker(ContractedDataset data, SufficientSigmaRuleset sufficientSet, Rubix rubix, Set<String> partialKey, String[] args) throws ParameterException, RepairException
    {
        return new ParkerRepair(
            createParkerModel( //Parker model to be used
                sufficientSet,
                rubix,
                partialKey,
                args),
            ParameterTools.extractFlag(frequencySelectionFlag, args) //Selection method
                ? new FrequencyRepairSelection(
                    data
                        .select(o -> sufficientSet.isSatisfied(o))
                        .project(sufficientSet
                            .stream()
                            .flatMap(rule -> rule
                                .getInvolvedAttributes()
                                .stream())
                            .collect(Collectors.toSet())
                        ),
                    sufficientSet)
                : new RandomRepairSelection() 
        )
        .repair(data);
    }

    public ParkerModel createParkerModel(SufficientSigmaRuleset rules, Rubix rubix, Set<String> pKey, String[] args) throws ParameterException, RepairException
    {
        String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        PartialKey partialKey = new PartialKey(
            pKey,
            rubix
                .getFunctionalDependencies(binder)
                .stream()
                .filter(fd -> pKey.containsAll(fd.getLeftHandSide()))
                .flatMap(fd -> fd.getRightHandSide().stream())
                .collect(Collectors.toSet())
        );
        
        //Extract relevant parameters
        Integer boundingOffset      = ParameterTools.extractParameter(boundingOffsetParameter, args);
        Integer boundingCap         = ParameterTools.extractParameter(boundingCapParameter, args);
        boolean useRangedBounding   = ParameterTools.extractFlag(rangedBoundingFlag, args);
        boolean forceBounding       = ParameterTools.extractFlag(forceBoundingFlag, args);
        
        return new ParkerModel<>(
            partialKey,
            rubix.getCostFunctions(),
            rules,
            useRangedBounding
                ? new RangedBounding(boundingOffset, boundingCap)
                : boundingOffset == 0
                    ? new EnumeratedBounding()
                    : new EnumeratedOffsetBounding(boundingOffset),
            forceBounding
        );
    } 
}
