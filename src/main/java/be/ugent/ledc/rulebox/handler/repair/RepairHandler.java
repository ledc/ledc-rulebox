package be.ugent.ledc.rulebox.handler.repair;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.rulebox.Config;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.DataConstraintHandler;
import be.ugent.ledc.rulebox.handler.DataModifier;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import be.ugent.ledc.sigma.repair.RepairEngine;
import be.ugent.ledc.core.RepairException;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class RepairHandler extends DataConstraintHandler implements DataModifier
{
    public static final String RULEBOX_KEY = "#ledc_rulebox_id";
    
    private final Parameter<Set<String>> onlyOnParameter = new Parameter<>(
        "only",
        "--o",
        false,
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Only repair for these attributes (comma-separated list)."
    );

    private final Parameter<Set<String>> excludeParameter = new Parameter<>(
        "exclude",
        "--e",
        false,
        (s) -> Stream
                .of(s.split(","))
                .map(String::trim)
                .collect(Collectors.toSet()),
        "Don't repair for these attributes (comma-separated list)."
    );
    
    private final Parameter<Integer> verbosityParameter = new OptionalParameter<>(
        "verbosity",
        "--v",
        (s) -> Integer.valueOf(s),
        "Verbose output of the repair proces. Higher values will provide more output. Default values is 0",
        0
    );
    
    public RepairHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(onlyOnParameter);
        addParameter(excludeParameter);
        addParameter(repairBinderParameter);
        addParameter(verbosityParameter);
        addFlag(softRepairFlag);
    }
    
    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);

            Config.loadRegistry();

            System.out.println("Reading constraints");
            Rubix rubix = RubixReader
                .readRubix(ParameterTools.extractParameter(getConstraintFileParameter(), args));

            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
            
            int id = 0;
            for(DataObject o: data)
            {
                o.setInteger(RULEBOX_KEY, id++);
            }

            Set<String> selection = data
                .getContract()
                .getAttributes();

            Set<String> onlyOn = ParameterTools.extractParameter(onlyOnParameter, args);
            Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);

            if (onlyOn != null)
            {
                selection = SetOperations.intersect(selection, onlyOn);
            }
            if (exclude != null)
            {
                selection = SetOperations.difference(selection, exclude);
            }
              
            RepairEngine.VERBOSITY = ParameterTools.extractParameter(verbosityParameter, args);
            
            System.out.println("Applying data repair algorithm to selected attributes " + selection);
            Dataset repairedData = repair(
                data,
                selection,
                rubix,
                args);
            
            repairedData.sort(DataObject.getProjectionComparator(RULEBOX_KEY));
                    
            for(DataObject o: data)
            {
                o.drop(RULEBOX_KEY);
            }
            
            System.out.println("Writing repair...");
            String repairBinderName = ParameterTools.extractParameter(repairBinderParameter, args);
            
            updateRows(
                data,
                ParameterTools.extractParameter(getBinderNameParameter(), args),
                ContractedDataset.create(repairedData.project(data.getContract().getAttributes()), data.getContract()),
                repairBinderName,
                ParameterTools.extractFlag(softRepairFlag,
                args)
            );
            
        } 
        catch (LedcException | SQLException | URISyntaxException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    public abstract Dataset repair(ContractedDataset data, Set<String> attributes, Rubix rubix, String[] args) throws LedcException;
}
