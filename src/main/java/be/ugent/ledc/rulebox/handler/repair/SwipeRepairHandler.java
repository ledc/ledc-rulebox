package be.ugent.ledc.rulebox.handler.repair;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.algorithms.repair.PartitionGenerator;
import be.ugent.ledc.fundy.algorithms.repair.Swipe;
import be.ugent.ledc.fundy.algorithms.repair.SwipePartition;
import be.ugent.ledc.fundy.algorithms.repair.priority.FixedPriority;
import be.ugent.ledc.fundy.algorithms.repair.priority.PriorityManager;
import be.ugent.ledc.fundy.algorithms.repair.priority.ReliabilityPriority;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.fundy.algorithms.repair.cost.SwipeCostModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SwipeRepairHandler extends RepairHandler
{
    private final Flag tieBreakingFlag = new Flag(
        "tie-breaking",
        "--tb",
        "When enabled, the Swipe algorithm will poll for input in case any ties occur in the generated preference order for attributes.");
    
    public SwipeRepairHandler()
    {
        super("swipe", "Applies the Swipe repair engine to resolve FD violations. "
                + "This repair engine uses repair functions to be specified in the rubix file.");
        
        addFlag(tieBreakingFlag);
    }

    @Override
    public Dataset repair(ContractedDataset data, Set<String> attributes, Rubix rubix, String[] args) throws RepairException
    {
        try
        {
            String binder = ParameterTools.extractParameter(getBinderNameParameter(), args);
            
            PriorityManager pm = new ReliabilityPriority();
            
            if(ParameterTools.extractFlag(tieBreakingFlag, args))
            {
                SwipePartition part = PartitionGenerator.generate(new MinimalCoverGenerator()
                    .generate(rubix
                            .getFunctionalDependencies(binder)
                    ));
           
                List<String> reliability = new ArrayList<>();
                
                for(Set<String> repairClass: part)
                {
                    if(repairClass.size() > 1)
                    {
                        //Let the user provide input on how to sort attributes in this class
                        List<String> tieBreaker = breakTies(repairClass);
                        
                        //Update the attribute order accordingly
                        reliability.addAll(tieBreaker);
                    }
                }
                
                pm = new FixedPriority(reliability);
            }
            
            //Step 2: build a repair agent
            SwipeCostModel scm = new SwipeCostModel(rubix.getCostFunctions());

            return new Swipe
            (
                rubix
                    .getFunctionalDependencies(binder)
                    .project(attributes),
                scm,
                pm //Pass the priority manager
            )
            .repair(data);
        }
        catch (ParameterException ex)
        {
            throw new RepairException(ex);
        }

    }

    private List<String> breakTies(Set<String> repairClass)
    {
        System.out.println("Swipe repair class needs an ordering:" + repairClass);
        System.out.println("Please provide an order of highest reliability first as a comma-separated list of attributes.");
        System.out.println("You can omit attributes: they will be considered as less reliable");
        
        while(true)
        {
            String input = (System.console() == null)
                ? new Scanner(System.in).nextLine()
                : System.console().readLine();
            
            //Parse
            List<String> order = Stream
                .of(input.split("\\s*,\\s*"))
                .map(s -> s.trim())
                .collect(Collectors.toList());
            
            if(order.stream().anyMatch(oa -> !repairClass.contains(oa)))
            {
                System.err.println("Attributes are not all part of the given repair class. Please try again");
            }
            else
            {
                return order;
            }
            
        }
    }
}
