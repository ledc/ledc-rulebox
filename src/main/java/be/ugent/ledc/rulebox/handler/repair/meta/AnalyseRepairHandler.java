package be.ugent.ledc.rulebox.handler.repair.meta;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import be.ugent.ledc.rulebox.Config;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.LeafHandler;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.explore.stats.Binning;
import be.ugent.ledc.rulebox.handler.util.IdHandler;
import be.ugent.ledc.rulebox.handler.util.PlotUtil;
import be.ugent.ledc.rulebox.io.result.Container;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.rulebox.io.result.MixedContainter;
import be.ugent.ledc.rulebox.io.result.OutputFactory;
import be.ugent.ledc.rulebox.io.result.plot.BoxPlot;
import be.ugent.ledc.rulebox.io.result.plot.HeatMap;
import be.ugent.ledc.rulebox.io.result.plot.HistogramPlot;
import be.ugent.ledc.rulebox.io.result.plot.PlotContainer;
import be.ugent.ledc.rulebox.io.result.table.TableContainer;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class AnalyseRepairHandler extends LeafHandler
{
    private final Parameter<String> sourceBinderParameter = ParameterFactory
        .createBinderNameParameter(true);
    
    private final Parameter<String> repairBinderParameter = 
        new Parameter<>(
            "repair-binder",
            "--r",
            true,
            (s) -> s,
            "Name of the repair binder. Use 'binder list' for an overview."
        )
        .addVerifier(n -> BinderPool.getInstance().exists(n), "Unknown data binder name.");
    
    private final Parameter<Set<String>> onlyOnParameter = new Parameter<>(
        "only",
        "--o",
        false,
        (s) -> Stream
            .of(s.split(","))
            .map(String::trim)
            .collect(Collectors.toSet()),
        "Apply repair only for these attributes (comma-separated list)"
    );

    private final Parameter<Set<String>> excludeParameter = new Parameter<>(
        "exclude",
        "--e",
        false,
        (s) -> Stream
            .of(s.split(","))
            .map(String::trim)
            .collect(Collectors.toSet()),
        "Don't apply repair for these attributes (comma-separated list)"
    );
    
    private final Parameter<Set<String>> rowKeyParameter = new Parameter<>(
        "row-key",
        "--rk",
        true,
        (s) -> Stream
            .of(s.split(","))
            .map(String::trim)
            .collect(Collectors.toSet()),
        "Identifier attributes for rows (comma-separated list) to match repair rows with."
    )
    .addVerifier((rk) -> !rk.isEmpty(), "Row key cannot be empty");
    
    private final Parameter<File> outputFileParameter = ParameterFactory
        .createOutputFileParameter(true)
        .addVerifier(
            (file) -> file.getName().endsWith(".html")
                   || file.getName().endsWith(".xml"),
            "Outputfile must be an .html or .xml file");
    
    private final IdHandler idHandler = new IdHandler();
    
    public AnalyseRepairHandler()
    {
        super("analyse", "Provides a comparative analysis of a repair and the original data.");
        
        addParameter(sourceBinderParameter);
        addParameter(onlyOnParameter);
        addParameter(excludeParameter);
        addParameter(repairBinderParameter);
        addParameter(rowKeyParameter);
        addParameter(outputFileParameter);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            //Get parameter values
            String sourceBinderName = ParameterTools.extractParameter(sourceBinderParameter, args);
            String repairBinderName = ParameterTools.extractParameter(repairBinderParameter, args);
            
            Set<String> onlyOn = ParameterTools.extractParameter(onlyOnParameter, args);
            Set<String> exclude = ParameterTools.extractParameter(excludeParameter, args);
            
            Set<String> rowKey = ParameterTools.extractParameter(rowKeyParameter, args);

            //Fetch repair
            ContractedDataset repair = BinderPool
                .getInstance()
                .data(repairBinderName);
            
            if(!repair.getContract().getAttributes().containsAll(rowKey))
            {
                error("Repair data does not contain row key " + rowKey);
                return;
            }
            
            //Check if row key has null values in repair
            if(repair.select(o -> rowKey.stream().anyMatch(a -> o.get(a) == null)).getSize() > 0)
            {
                error("Repair has null values for row key " + rowKey);
                return;
            }
            
            //Check if row key is unique in repair
            if(repair.getSize() != repair.project(rowKey).distinct().getSize())
            {
                error("Row key " + rowKey + " has duplicate values for repair");
                return;
            }

            
            //Fetch data
            ContractedDataset source = BinderPool
                .getInstance()
                .data(sourceBinderName);
            
            if(!source.getContract().getAttributes().containsAll(rowKey))
            {
                error("Source data does not contain row key " + rowKey);
                return;
            }
            
            //Check if row key has null values in source
            if(source.select(o -> rowKey.stream().anyMatch(a -> o.get(a) == null)).getSize() > 0)
            {
                error("Source data has null values for row key " + rowKey);
                return;
            }
            
            //Check if row key is unique in source
            if(source.getSize() != source.project(rowKey).distinct().getSize())
            {
                error("Row key " + rowKey + " has duplicate values for source data");
                return;
            }
            
            //Initialize selection with repair attributes
            Set<String> selection = repair
                .getContract()
                .getAttributes();
            
            //Keep only attributes that are also in the source
            selection.retainAll(source.getContract().getAttributes());

            //Apply requested selection criteria
            if (onlyOn != null)
            {
                selection = SetOperations.intersect(selection, onlyOn);
            }
            if (exclude != null)
            {
                selection = SetOperations.difference(selection, exclude);
            }
            
            //Remove row key attributes
            selection.removeAll(rowKey);
            
            //Check if row key attributes have compatible contracts
            for(String a: rowKey)
            {
                TypeContractor<?> sourceContract = source
                    .getContract()
                    .getAttributeContract(a);
                
                TypeContractor<?> repairContract = repair
                    .getContract()
                    .getAttributeContract(a);
                
                if(!sourceContract.equals(repairContract))
                {
                    error("Row key attribute "
                        + a
                        + " has different contracts."
                        + "\nSource contract: " + sourceContract.name()
                        + "\nRepair contract: " + repairContract.name()
                    );
                    return;
                }
            }
            
            //A list in which containers are collected
            List<Container<?>> containers = new ArrayList<>();
            
            Map<DataObject, List<DataObject>> sourceMap = source
                .stream()
                .collect(Collectors.groupingBy(o -> o.project(rowKey)));
            
            Map<DataObject, List<DataObject>> repairMap = repair
                .stream()
                .collect(Collectors.groupingBy(o -> o.project(rowKey)));
            
            //Compute overlap: keys that appear in both source and repair
            Set<DataObject> overlap = repairMap
                .keySet()
                .stream()
                .filter(sourceMap::containsKey)
                .collect(Collectors.toSet());
            
            //Compute number of inserts
            long inserts = repairMap
                .keySet()
                .stream()
                .filter(o -> !overlap.contains(o))
                .count();
            
            //Compute number of deletes
            long deletes = sourceMap
                .keySet()
                .stream()
                .filter(o -> !overlap.contains(o))
                .count();
            
            ContractedDataset generalStats = new ContractedDataset(new Contract
                .ContractBuilder()
                .addContractor("Descriptor", TypeContractorFactory.STRING)
                .addContractor("Value", TypeContractorFactory.STRING)
                .build());
            
            generalStats
                .addDataObject(
                    new DataObject(true)
                        .setString("Descriptor", "Deletes")
                        .setString("Value", Long.toString(deletes))
                );
            
            generalStats
                .addDataObject(
                    new DataObject(true)
                        .setString("Descriptor", "Inserts")
                        .setString("Value", Long.toString(inserts))
                );
            
            generalStats
                .addDataObject(
                    new DataObject(true)
                        .setString("Descriptor", "Overlap")
                        .setString("Value", Integer.toString(overlap.size()))
                );
            
            for(String a: selection)
            {
                System.out.println("Inspection of attribute '" + a + "'");
                
                Map<Couple<?>, Integer> changeFlow = new HashMap<>();

                int changes = 0;
                
                for(DataObject key: overlap)
                {
                    Object sValue = sourceMap.get(key).get(0).get(a);
                    Object rValue = repairMap.get(key).get(0).get(a);
                    
                    if(!Objects.equals(sValue, rValue))
                    {
                        //Increase change count
                        changes++;
                        
                        //Update change flow mapping
                        changeFlow.merge(
                            new Couple<>(sValue, rValue),
                            1,
                            Integer::sum
                        );    
                    }
                }
                
                if(changes > 0)
                {
                    generalStats
                    .addDataObject(
                        new DataObject(true)
                            .setString("Descriptor", "Changes in " + a)
                            .setString("Value", Integer.toString(changes))
                    );
                }
                
                if(changes > 0 || deletes > 0 || inserts > 0)
                {
                    //Prepare a container to hold all details about a
                    MixedContainter attributeContainer = new MixedContainter(
                        "Analysis for " + a,
                        idHandler.next());
                    
                    
                    
                    //Is the attribute numeric?
                    boolean numeric = 
                        DataHandler.isNumeric(source.getContract().getAttributeContract(a))
                    &&  DataHandler.isNumeric(repair.getContract().getAttributeContract(a));
                    
                    PlotContainer distributionContainer = new PlotContainer(
                        "Distribution comparison",
                        idHandler.next(),
                        numeric ? 4 : 2);
                    
                    Histogram<?> sourceHistogram = PlotUtil.buildHistogram(
                        source,
                        a,
                        Binning.STURGE.bins(source, a));
                    
                    Histogram<?> repairHistogram = PlotUtil.buildHistogram(
                        repair,
                        a,
                        Binning.STURGE.bins(repair, a));
                    
                    long max = Math.max(
                        sourceHistogram.maxCount(),
                        repairHistogram.maxCount()
                    );
                    
                    boolean catInSource = source
                        .getContract()
                        .getAttributeContract(a)
                        .name()
                        .equals(SigmaContractorFactory.STRING.name());
                    
                    boolean catInRepair = repair
                        .getContract()
                        .getAttributeContract(a)
                        .name()
                        .equals(SigmaContractorFactory.STRING.name());
                    
                    if(catInSource && sourceHistogram.numberOfBins() > 50)
                    {
                        System.out.println("Skipping source histogram: too many values");
                    }
                    else
                    {
                        distributionContainer
                            .addItem(
                                new HistogramPlot(
                                    sourceHistogram,
                                    "Source",
                                    catInSource,
                                    false,
                                    max)
                            );
                    }
                    
                    if(catInRepair && repairHistogram.numberOfBins() > 50)
                    {
                        System.out.println("Skipping repair histogram: too many values");
                    }
                    else
                    {
                        distributionContainer
                            .addItem(
                                new HistogramPlot(
                                    repairHistogram,
                                    "Repair",
                                    catInRepair,
                                    false,
                                    max)
                            );
                    }
                    
                    if(numeric)
                    {
                        BoxPlot sPlot = PlotUtil.buildBoxPlot(source, a, "Source");
                        BoxPlot rPlot = PlotUtil.buildBoxPlot(repair, a, "Repair");
                        
                        sPlot
                            .defaultSettings()
                            .withHeight(400)
                            .withWidth(400);
                        
                        rPlot
                            .defaultSettings()
                            .withHeight(400)
                            .withWidth(400);
                        
                        distributionContainer.addItem(sPlot);
                        distributionContainer.addItem(rPlot);
                    }

                    if(!distributionContainer.isEmpty())
                    {
                        //Add the distribution container
                        attributeContainer.addItem(distributionContainer);
                    }
                    
                    //Distributions of deletes and inserts
                    if(deletes > 0 || inserts > 0)
                    {
                        PlotContainer delInsContainer = new PlotContainer(
                            "Changes on tuple level",
                            idHandler.next(),
                            2);
                        
                        if(deletes > 0)
                        {
                            ContractedDataset deletedAttributeData = new ContractedDataset(new Contract
                                .ContractBuilder()
                                .addContractor(a, source.getContract().getAttributeContract(a))
                                .build());
                            
                            //For each object in the source data that is not
                            //in the overlap, add it to dataset with deleted information
                            sourceMap
                            .keySet()
                            .stream()
                            .filter(o -> !overlap.contains(o))
                            .map(o -> sourceMap.get(o).get(0).project(a))
                            .collect(Collectors.toList())
                            .forEach(deletedAttributeData::addDataObject);
                            
                            delInsContainer.addItem(new HistogramPlot(
                                PlotUtil.buildHistogram(
                                    deletedAttributeData,
                                    a,
                                    Binning.STURGE.bins(source, a)
                                ),
                                "Deletes",
                                deletedAttributeData
                                    .getContract()
                                    .getAttributeContract(a)
                                    .name()
                                    .equals(SigmaContractorFactory.STRING.name()),
                                false)
                            );
                        }
                        
                        if(inserts > 0)
                        {
                            ContractedDataset insertsInAttributeData = new ContractedDataset(new Contract
                                .ContractBuilder()
                                .addContractor(a, repair.getContract().getAttributeContract(a))
                                .build());
                            
                            //For each object in the source data that is not
                            //in the overlap, add it to dataset with deleted information
                            repairMap
                            .keySet()
                            .stream()
                            .filter(o -> !overlap.contains(o))
                            .map(o -> repairMap.get(o).get(0).project(a))
                            .collect(Collectors.toList())
                            .forEach(insertsInAttributeData::addDataObject);
                            
                            new HistogramPlot(
                                PlotUtil.buildHistogram(
                                    insertsInAttributeData,
                                    a,
                                    Binning.STURGE.bins(source, a)
                                ),
                                "Inserts",
                                insertsInAttributeData
                                    .getContract()
                                    .getAttributeContract(a)
                                    .name()
                                    .equals(SigmaContractorFactory.STRING.name()),
                                false);
                        }
                        
                        //Add the distribution container
                        attributeContainer.addItem(delInsContainer);
                    }
                    
                    if(changes > 0)
                    {
                        //Compose heatMap
                        List<Object> distinctValuesSorted = changeFlow
                            .keySet()
                            .stream()
                            .flatMap(c -> Stream.of(c.getFirst(), c.getSecond()))
                            .distinct()
                            .collect(Collectors.toList());
                        
                        if(distinctValuesSorted.size() > 50)
                        {
                            System.out.println("Omitting heatmap: too many values...");
                        }
                        else
                        {
                            int[][] heatMapMatrix = new int[distinctValuesSorted.size()][distinctValuesSorted.size()];

                            for(Couple<?> change: changeFlow.keySet())
                            {
                                heatMapMatrix
                                    [distinctValuesSorted.indexOf(change.getFirst())]
                                    [distinctValuesSorted.indexOf(change.getSecond())]
                                    = changeFlow.get(change);
                            }

                            PlotContainer changeFlowContainer = new PlotContainer(
                                "Change analysis",
                                idHandler.next(),
                                2);

                            changeFlowContainer.addItem(new HeatMap(
                                heatMapMatrix,
                                IntStream
                                    .range(0, distinctValuesSorted.size())
                                    .boxed()
                                    .collect(Collectors.toMap(                                   
                                        Function.identity(),
                                        i -> distinctValuesSorted.get(i) == null 
                                            ? Config.NULL_SYMBOL   
                                            : distinctValuesSorted.get(i).toString())),
                                "Change heatmap"));

                            attributeContainer.addItem(changeFlowContainer);
                        }
                    }
                    
                    if(!attributeContainer.isEmpty())
                        containers.add(attributeContainer);
                }
                
            }
            
            containers.add(0, new TableContainer(
                generalStats,
                "General overview",
                idHandler.next(),
                ListOperations.list("Descriptor", "Value")));
            
            if(!containers.isEmpty())
            {
                OutputFactory.dump(
                    containers,
                    ParameterTools.extractParameter(outputFileParameter, args)
                );
            }
            
        }
        catch(LedcException | FileNotFoundException | MarshallException ex)
        {
            error(ex.getMessage());
        }
    }
    
    
}
