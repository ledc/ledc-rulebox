package be.ugent.ledc.rulebox.handler.repair.selection;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.repair.ConditionalSequentialRepair;
import be.ugent.ledc.sigma.repair.NullBehavior;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;

public class ConditionalSequentialRepairHandler extends ConstantCostRepairHandler
{
    public ConditionalSequentialRepairHandler()
    {
        super("condsequential", "Applies Fellegi-Holt sequential repair for violations of selection rules, but selects "
        + "a minimal cover by the likelihood that the resulting row will not be highly infrequent. "
        + "A constant cost model can be specified in the rubix file.");
    }

    @Override
    public Dataset repair(String[] args, ContractedDataset data, SufficientSigmaRuleset rules, ConstantCostModel model, NullBehavior nb) throws RepairException
    {
        return new ConditionalSequentialRepair(
            rules,
            model,
            nb,
            data
                .select(o -> rules.isSatisfied(o))
                .project(rules.getContractors().keySet())

        )
        .repair(data);
    }
}
