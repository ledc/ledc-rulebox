package be.ugent.ledc.rulebox.handler.repair.selection;

import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;
import java.util.stream.Collectors;

public abstract class ConstantCostRepairHandler extends SelectionRepairHandler<ConstantCostModel>
{
    public ConstantCostRepairHandler(String command, String description)
    {
        super(command, description);
    }
    
    @Override
    public ConstantCostModel createCostModel(String [] args, SufficientSigmaRuleset rules, Rubix rubix)
    {
        if(rubix
            .getCostFunctions()
            .values()
            .stream()
            .anyMatch(cf -> !(cf instanceof ConstantCostFunction))
        )
        {
            System.err.println("Cannot repair selection rules. "
                + "Cause: the requested repair task requires a constant cost model.");
            System.exit(-1);
        }
        
        return new ConstantCostModel(rubix
            .getCostFunctions()
            .entrySet()
            .stream()
            .collect(Collectors.toMap(
                e -> e.getKey(),
                e -> (ConstantCostFunction)e.getValue()
            ))    
        );
    }
}
