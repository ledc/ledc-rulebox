package be.ugent.ledc.rulebox.handler.repair.selection;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.reasoning.SufficientSetHandler;
import be.ugent.ledc.rulebox.handler.repair.RepairHandler;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.repair.NullBehavior;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.CostModel;
import java.util.Set;

public abstract class SelectionRepairHandler<C extends CostModel> extends RepairHandler
{
    private final Flag fcfFlag = new Flag(
        "fcf",
        "--fcf",
        "Applies the FCF algorithm prior to repair. By default, it is assumed that selection rules in the rubix file are sufficient.");
    
    private final Parameter<Integer> nullRepairParameter = new OptionalParameter<>(
        "nulls",
        "--n",
        (s) -> Integer.parseInt(s),
        "Indicates whether null values should not be repaired (0), repaired only for attributes that have domain information (1) or repaired only for attributes involved in interaction rules (2). Default value is 0.",
        0
    )
    .addVerifier(i -> i>=0 && i<=2, "Parameters 'nulls' should have a value between 0 and 2, bounds inclusive.");
    
    public SelectionRepairHandler(String command, String description)
    {
        super(command, description);
        
        addFlag(fcfFlag);
        addParameter(nullRepairParameter);
    }
    
    @Override
    public Dataset repair(ContractedDataset data, Set<String> attributes, Rubix rubix, String[] args) throws RepairException, ParameterException
    {
        //Producde sufficient set
        SufficientSigmaRuleset suffRules = createSufficientSet(args, rubix)
               .project(attributes);
        
        //Apply repair
        return repair(
            args,
            data, 
            suffRules,
            createCostModel(args, suffRules, rubix),
            createNullBehavior(args)
        );
    }
      
    public SufficientSigmaRuleset createSufficientSet(String [] args, Rubix rubix) throws ParameterException
    {
        String binderName = ParameterTools.extractParameter(getBinderNameParameter(), args);
        
        return ParameterTools.extractFlag(fcfFlag, args)
            ? new SufficientSetHandler().generate(rubix)
            : SufficientSigmaRuleset.create(rubix.getSigmaRules(binderName));
    }
    
    public NullBehavior createNullBehavior(String [] args) throws ParameterException
    {
        Integer n = ParameterTools.extractParameter(nullRepairParameter, args);
        
        switch(n)
        {
            case 0:
                return NullBehavior.NO_REPAIR;
            case 1:
                return NullBehavior.DOMAIN_REPAIR;
            default:
                return NullBehavior.CONSTRAINED_REPAIR;
        }                
    }
    
    public abstract Dataset repair(String [] args, ContractedDataset data, SufficientSigmaRuleset rules, C model, NullBehavior nb) throws ParameterException, RepairException;
    
    public abstract C createCostModel(String [] args, SufficientSigmaRuleset rules, Rubix rubix) throws ParameterException,RepairException;
}
