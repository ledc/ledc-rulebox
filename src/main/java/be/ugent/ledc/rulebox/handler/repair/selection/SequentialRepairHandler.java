package be.ugent.ledc.rulebox.handler.repair.selection;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.repair.NullBehavior;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.sigma.repair.SequentialRepair;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;

public class SequentialRepairHandler extends ConstantCostRepairHandler
{

    public SequentialRepairHandler()
    {
        super("sequential", "Applies Fellegi-Holt sequential repair for violations of sigma rules "
                + "with random selection of a minimal cover. "
                + "A constant cost model can be specified in the rubix file");
    }

    @Override
    public Dataset repair(String[] args, ContractedDataset data, SufficientSigmaRuleset rules,ConstantCostModel model, NullBehavior nb) throws RepairException
    {
        return new SequentialRepair(
            rules,
            model,
            nb,
            data
                .select(o -> rules.isSatisfied(o))
                .project(rules.getContractors().keySet())
        )
        .repair(data);
    }
    
}
