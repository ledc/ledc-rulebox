package be.ugent.ledc.rulebox.handler.server;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.rulebox.Config;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.rulebox.handler.ParameterFactory;
import be.ugent.ledc.rulebox.io.rubix.RubixReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.json.JSONObject;
import org.json.JSONTokener;

public class FetchHandler extends ServerHandler
{
    private final Parameter<File> constraintFileParameter = ParameterFactory.createConstraintFileParameter(true);
    
    private final Flag overwriteFlag = new Flag(
        "overwrite",
        "--o",
        "Overwrites any information about properties already available in the local registry"
    );
    
    public FetchHandler()
    {
        super("fetch", "Connects to a ledc server and downloads all available components for properties listed in a rubix file to the local registry.");
        
        addParameter(constraintFileParameter);
        addFlag(overwriteFlag);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {    
            File rubixFile  = ParameterTools.extractParameter(constraintFileParameter, args);
            
            boolean overwrite   = ParameterTools.extractFlag(overwriteFlag, args);
            
            System.out.println("Reading constraints");
            Rubix rubix = RubixReader.readRubix(rubixFile);
            
            Set<Property> requiredProperties = rubix
                .getAllPropertyAssertions()
                .getRules()
                .stream()
                .map(a -> a.getProperty())
                .collect(Collectors.toSet());
            
            for(Property p: requiredProperties)
            {
                System.out.println("Handling property " + p);

                if(!overwrite && verify(p))
                {
                    System.out.println("Skipping property: information already exists locally. "
                        + "Re-run with the --o flag to overwrite.");
                }
                else
                {
                    File o = createFile(p);
                    fetchProperty(p, o);
                }
            }
        }
        catch (LedcException | IOException ex)
        {
            System.err.println(ex.getMessage());
        }
        catch (URISyntaxException ex)
        {
            System.err.println("Malformed URI syntax: " + ex.getMessage());
        }
    }

    public FetchHandler(String command, String description)
    {
        super(command, description);
    }

    public static File createFile(Property p)
    {
        return new File(Config
            .LOCAL_REGISTRY
            .concat(File.separator)
            .concat(p.getCanonicalName()
                .replaceAll(":?\\*", "")
            .replaceAll("\\.|/|#|:", "_"))
            .concat(".json"));
    }
    
    private void fetchProperty(Property p, File outputFile) throws URISyntaxException, MalformedURLException, IOException
    {
        Map <String,String> params = new HashMap<>();
        params.put("property", p.getCanonicalName());

        URL url = buildUrl("/fetch", params);
            
        System.out.println("Sending request to ledc server");
        System.out.println("-> " + url);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200)
        {
            throw new RuntimeException("Request to ledc server failed. Response code: " + conn.getResponseCode());
        }

        JSONObject pRegistry = new JSONObject(new JSONTokener((conn.getInputStream())));

        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(outputFile),
                        StandardCharsets.UTF_8)
        ))
        {
            writer.write(pRegistry.toString(4));
            writer.flush();
        }        
        
    }   

    private boolean verify(Property p) throws LedcException, URISyntaxException, FileNotFoundException
    {
        Config.loadRegistry();
        
        return Registry
            .getInstance()
            .hasValidationScheme(p)
        ||
           Registry
            .getInstance()
            .hasScanners(p);

    }
}
