package be.ugent.ledc.rulebox.handler.server;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.rulebox.handler.util.StringUtil;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ListHandler extends ServerHandler
{
    private final Parameter<String> filterParameter = new OptionalParameter<>(
        "filter",
        "--f",
        (s)->s,
        "A text pattern that the property URL must satisfy. Filters by string containment, or regex match when prefixed with /",
        "/.*"
    );
    
    public ListHandler()
    {
        super("list", "Lists all properties this server knows off and the operations it can do for those properties.");
        
        addParameter(filterParameter);
    }

    @Override
    public void handle(String[] args)
    {
        HttpURLConnection conn = null;
        try
        {
            super.handle(args);
            
            String filter = ParameterTools.extractParameter(filterParameter, args);
            
            String pattern = filter.startsWith("/")
                ? filter.substring(1)
                :  ".*".concat(filter).concat(".*");
            
            Map <String,String> params = new HashMap<>();
            params.put("pattern", pattern);
            
            URL url = buildUrl("/list", params);
            
            System.out.println("Sending request to ledc server");
            System.out.println("-> " + url);
            
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            
            if (conn.getResponseCode() != 200)
            {
                throw new RuntimeException("Request to ledc server failed. Response code: " + conn.getResponseCode());
            }
            
            JSONObject topObject = new JSONObject(
                new JSONTokener(
                    (conn.getInputStream())));
            
            JSONObject pInfo = topObject.getJSONObject("propertyInfo");
            
            System.out.println("Found " + pInfo.keySet().size() + " properties.");
            
            if(pInfo.keySet().size() > 0)
            {
                System.out.println("Listing properties with 4 bit mask");
                System.out.println("  bit 1 (s): a scanner is available");
                System.out.println("  bit 2 (m): a measure is available");
                System.out.println("  bit 3 (s): a standardizer is available");
                System.out.println("  bit 4 (c): a convertor is available");
                System.out.println("");
                
                int w = StringUtil.padSize(pInfo.keySet());
                
                //Show output
                pInfo
                    .keySet()
                    .stream()
                    .sorted()
                    .map(p -> StringUtil.pad(p, w) + " -> " + pInfo.getString(p))
                    .forEach(System.out::println);
                
            }
        }
        catch (LedcException | ProtocolException ex)
        {
            System.err.println(ex.getMessage());
        }
        catch (URISyntaxException ex)
        {
            System.err.println("Malformed URI syntax: " + ex.getMessage());
        }
        catch (IOException ex)
        {
            Logger.getLogger(ListHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            //Close connection
            if(conn != null)
                conn.disconnect();
        }

    }

}
