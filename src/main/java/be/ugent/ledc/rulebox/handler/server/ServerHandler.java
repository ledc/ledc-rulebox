package be.ugent.ledc.rulebox.handler.server;

import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.rulebox.handler.LeafHandler;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.stream.Collectors;

public class ServerHandler extends LeafHandler
{
    private final Parameter<String> hostParameter = new Parameter<>(
        "host",
        "--h",
        true,
        (s) -> s,
        "The location of the ledc server.");
    
    private final Flag httpFlag = new Flag(
        "http",
        "--http",
        "Attempts to connect to the server over an HTTP connection instead of HTTPS."
    );
        
    private String host;
    
    private boolean http;
    
    public ServerHandler(String command, String description)
    {
        super(command, description);
        addParameter(hostParameter);
    }

    @Override
    public void handle(String[] args)
    {
        super.handle(args);
        
        try
        {
            host = ParameterTools.extractParameter(hostParameter, args);
            
            if(host.startsWith("https://"))
                host = host.substring(8);
            if(host.startsWith("http://"))
                host = host.substring(7);
            
            http = ParameterTools.extractFlag(httpFlag, args);
        }
        catch (ParameterException ex)
        {
            System.err.println(ex.getMessage());
        }
        
    }

    public URL buildUrl(String command, Map<String,String> parameters) throws URISyntaxException, MalformedURLException
    {
        return new URI(
            http ? "http" : "https",
            host,
            command,
            parameters
                .entrySet()
                .stream()
                .map(e -> e.getKey().concat("=").concat(e.getValue()))
                .collect(Collectors.joining("&")),
            null
        ).toURL();
    }
    
    
}
