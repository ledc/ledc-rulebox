package be.ugent.ledc.rulebox.handler.transform;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import java.io.FileNotFoundException;
import java.sql.SQLException;

public abstract class AddAttributeHandler<T> extends AttributeTransformHandler
{
    private final Parameter<String> originalAttributeNameParameter = new Parameter<>(
        "original-attribute",
        "--o",
        true,
        s->s,
        "Input attribute to generate the new attribute.");
     
    private final Flag removeOriginalFlag = new Flag(
        "remove-original",
        "--ro",
        "When activated, the original attribute is removed.");
     
    public AddAttributeHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(originalAttributeNameParameter);
        addFlag(removeOriginalFlag);
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);

            //Read dataset
            System.out.println("Reading data...");
            ContractedDataset data = BinderPool
                .getInstance()
                .data(ParameterTools.extractParameter(getBinderNameParameter(), args));
            
            ContractedDataset extended = extend(
                data,
                ParameterTools.extractParameter(originalAttributeNameParameter, args),
                getAttribute(args),
                args
            );
            
            addAttribute(
                extended,
                getAttribute(args),
                ParameterTools.extractParameter(originalAttributeNameParameter, args),
                ParameterTools.extractParameter(repairBinderParameter, args),
                ParameterTools.extractFlag(softRepairFlag, args),
                ParameterTools.extractFlag(removeOriginalFlag, args)
            );
        }
        catch(LedcException | SQLException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    public abstract ContractedDataset extend(ContractedDataset data, String originalAttribute, String newAttribute, String[] args) throws ParameterException, DataWriteException;
}
