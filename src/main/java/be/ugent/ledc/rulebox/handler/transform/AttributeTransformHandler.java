package be.ugent.ledc.rulebox.handler.transform;

import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;

public abstract class AttributeTransformHandler extends TransformHandler
{
    private final Parameter<String> attributeNameParameter = new Parameter<>(
        "attribute",
        "--a",
        true,
        s->s,
        "Attribute that is the target of the transformation.");
    
    public AttributeTransformHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(attributeNameParameter);
    }
    
    public String getAttribute(String[] args) throws ParameterException
    {
        return ParameterTools.extractParameter(attributeNameParameter, args);
    }
}
