package be.ugent.ledc.rulebox.handler.transform;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.chronos.datastructures.TemporalDataset;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.rulebox.handler.binder.BinderBox;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.handler.explore.timeseries.CusumDetectionHandler;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CusumChangesTransformHandler extends AddAttributeHandler<Integer>
{
   private final Parameter<Integer> windowParameter = new OptionalParameter<>
    (
        "window",
        "--w",
        (s) -> Integer.valueOf(s),
        "Size of the window used for comparison of signal portions. Default value is 10.",
        10
    )
    .addVerifier(i -> i > 0, "Window size must be strictly positive.");
    
    private final Parameter<Double> multiplierParameter = new OptionalParameter<>
    (
        "multiplier",
        "--m",
        (s) -> Double.valueOf(s),
        "Multiplier applied to the IQR range. Default value is 2.",
        2.0
    )
    .addVerifier(i -> i > 0.0, "Window size must be strictly positive.");
    
    private final Parameter<String> modelParameter = new OptionalParameter<>
    (
        "model",
        "--mo",
        (s) -> s,
        "Underlying model that generated the data. Default model is normal.",
        "normal"
    )
    .addVerifier(m -> Stream
        .of(CusumDetectionHandler.MODELS)
        .anyMatch(model -> model.equals(m)),
        "Unknown model. Available models are: " + Stream
            .of(CusumDetectionHandler.MODELS)
            .collect(Collectors.joining(",")));
     
    public CusumChangesTransformHandler()
    {
        super(
            "changepoints",
            "Creates a new attribute that contains the changepoints of the "
                + "original attribute using the CUSUM detection method. "
                + "This transformation requires the binder to be a temporal dataset."
        );
        
        addParameter(windowParameter);
        addParameter(modelParameter);
        addParameter(multiplierParameter);
        
        getBinderNameParameter()
            .addVerifier(b -> BinderPool.getInstance().isTemporal(b),
                "This transformation requires a binder that is marked as a temporal dataset.");
    }

    @Override
    public ContractedDataset extend(ContractedDataset data, String originalAttribute, String newAttribute, String [] args) throws ParameterException, DataWriteException
    {
        try
        {
            ContractedDataset extendedDataset = new ContractedDataset(new Contract
                .ContractBuilder(data.getContract())
                .addContractor(newAttribute, SigmaContractorFactory.INTEGER)
                .build());
            
            if(originalAttribute == null
            || !data.getContract().getAttributes().contains(originalAttribute))
            {
                throw new DataWriteException("Attribute '"
                    + originalAttribute
                    + "' does not occur in the dataset.");                
            }

            //Transform
            System.out.println("Applying data transformation...");
            
            CusumDetectionHandler cdh = new CusumDetectionHandler();
            
            BinderBox<?> binderBox = BinderPool
                    .getInstance()
                    .getBinderBox(
                        ParameterTools.extractParameter(getBinderNameParameter(),
                        args));
            
            String tAttribute = binderBox
                .getTemporalInfo()
                .getTimeAttribute();
            
            if(binderBox.isTemporalPartitioned())
            {
                Map<String, TemporalDataset<Comparable<? super Comparable>>> dataMap = binderBox
                    .temporalPartitionedData();
                
                Map<String, List> changeMap = new HashMap<>();
                
                for(String partitionKey: dataMap.keySet())
                {
                    Signal attributeSignal = dataMap
                        .get(partitionKey)
                        .getAttributeSignal(originalAttribute);
                    
                    List changePoints = cdh.findChanges(attributeSignal, args);
                    
                    if(changePoints != null && !changePoints.isEmpty())
                    {
                        changeMap.put(partitionKey, changePoints);
                    }
                }
                
                String pAttribute = binderBox
                    .getTemporalInfo()
                    .getPartitionAttribute();
                        
                for(DataObject o: data)
                {
                    DataObject eObject = new DataObject(o);
                    
                    if(o.get(pAttribute) == null)
                    {
                        eObject.setInteger(newAttribute,0);
                        extendedDataset.addDataObject(eObject);
                        continue;
                    }
                    
                    String pValue = o.get(pAttribute).toString();
                    
                    if(changeMap.get(pValue) == null || changeMap.get(pValue).isEmpty())
                    {
                        eObject.setInteger(newAttribute,0);
                        extendedDataset.addDataObject(eObject);
                        continue;
                    }
                    
                    if(!changeMap.get(pValue).contains(o.get(tAttribute)))
                    {
                        eObject.setInteger(newAttribute,0);
                        extendedDataset.addDataObject(eObject);
                        continue;
                    }
                    
                    eObject.setInteger(newAttribute,1);
                    extendedDataset.addDataObject(eObject);
                }
            }
            else
            {
                TemporalDataset<?> temporalData = binderBox.temporalData();
                Signal attributeSignal = temporalData
                    .getAttributeSignal(originalAttribute);
                    
                List changePoints = cdh.findChanges(attributeSignal, args);
                
                for(DataObject o: data)
                {
                    DataObject eObject = new DataObject(o);

                    if(!changePoints.contains(o.get(tAttribute)))
                        eObject.setInteger(newAttribute,0);
                    else
                        eObject.setInteger(newAttribute,1);

                    extendedDataset.addDataObject(eObject);                    
                }
            }
            
            return extendedDataset;
        }
        catch (IOException
            | DataWriteException
            | DataReadException
            | ChronosException            
            | BinderPoolException ex)
        {
            System.err.println(ex.getMessage());
            return null;
        }

    }    
}

