package be.ugent.ledc.rulebox.handler.transform;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.measure.predicates.GrexPredicate;
import be.ugent.ledc.pi.measure.predicates.PatternPredicate;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.util.List;
import java.util.stream.Collectors;

public class GrexTransformHandler extends AddAttributeHandler<String>
{
    private final Parameter<String> patternParameter = new OptionalParameter<>(
        "pattern",
        "--p",
        (s) -> s,
        "Regex to match values against in the original attribute. "
            + "Only values matching this regex will lead to values in the new attribute.",
        ".*"
    );
    
    private final Parameter<String> transformationParameter = new Parameter<>(
        "transformation",
        "--t",
        true,
        (s) -> s,
        "Grex expression that describes the transformation from original values to new ones."
            + " Group variables are interpreted in the context of the pattern parameter."
    );
    
    private final Parameter<String> conditionParameter = new Parameter<>(
        "condition",
        "--c",
        false,
        (s) -> s,
        "Additional condition to select values for transformation, formulated as grex expressions. "
            + "Multiple conditions are allowed and will be combined conjunctively",
        true
    );
     
    public GrexTransformHandler()
    {
        super(
            "grex",
            "Creates a new attribute based on regex/grex expressions applied to an existing attribute"
        );
        
        addParameter(patternParameter);
        addParameter(transformationParameter);
        addParameter(conditionParameter);
    }

    @Override
    public ContractedDataset extend(ContractedDataset data, String originalAttribute, String newAttribute, String [] args) throws ParameterException, DataWriteException
    {
        try
        {
            String pattern = ParameterTools.extractParameter(patternParameter, args);
            String transform = ParameterTools.extractParameter(transformationParameter, args);
            
            ContractedDataset extendedDataset = new ContractedDataset(new Contract
                .ContractBuilder(data.getContract())
                .addContractor(newAttribute, SigmaContractorFactory.STRING)
                .build());
            
            if(originalAttribute == null
            || !data.getContract().getAttributes().contains(originalAttribute))
            {
                throw new DataWriteException("Attribute '"
                    + originalAttribute
                    + "' does not occur in the dataset.");                
            }
            
            PatternPredicate pp = new PatternPredicate(pattern);
            
            List<GrexPredicate> conditions = ParameterTools
                .extractMultiParameter(conditionParameter, args)
                .stream()
                .map(c -> new GrexPredicate(new Grex(c), pattern))
                .collect(Collectors.toList());
            
            //Transform
            System.out.println("Applying data transformation...");
            for(DataObject o: data)
            {
                if(o.get(originalAttribute) == null)
                    continue;
                
                String value = o
                    .project(originalAttribute)
                    .toString();
                
                //Check the basic regex pattern
                if(!pp.test(value))
                    continue;
                
                //Check any additional tests
                if(conditions.stream().allMatch(gp -> gp.test(value)))
                {
                    Grex tGrex = new Grex(transform);

                    DataObject eObject = new DataObject(o);
                    
                    eObject.setString(
                        newAttribute,
                        tGrex.resolveAsString(pattern, value)
                    );
                }
            }
            
            return extendedDataset;
        }
        catch (LedcException ex)
        {
            System.err.println(ex.getMessage());
            return null;
        }

    }    
}
