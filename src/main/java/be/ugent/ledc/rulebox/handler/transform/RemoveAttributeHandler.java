package be.ugent.ledc.rulebox.handler.transform;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.rulebox.handler.binder.BinderBox;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class RemoveAttributeHandler extends AttributeTransformHandler
{
    public RemoveAttributeHandler()
    {
        super("remove", "Removes the attribute from the dataset.");
    }

    @Override
    public void handle(String[] args)
    {
        try
        {
            super.handle(args);

            String repairBinder = ParameterTools.extractParameter(repairBinderParameter, args);

            if(BinderPool.getInstance().getBinderType(repairBinder).equals(BinderBox.TYPE_JDBC_TABLE))
            {
                removeAttribute(
                    getAttribute(args),
                    repairBinder,
                    ParameterTools.extractFlag(softRepairFlag, args));
            }
            else
            {
                //Read dataset
                System.out.println("Reading data...");
                ContractedDataset data = BinderPool
                    .getInstance()
                    .data(ParameterTools.extractParameter(getBinderNameParameter(), args));

                removeAttribute(
                    data,
                    getAttribute(args),
                    repairBinder,
                    ParameterTools.extractFlag(softRepairFlag, args));
            }
        }
        catch(LedcException | SQLException | FileNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }
    
    
}
