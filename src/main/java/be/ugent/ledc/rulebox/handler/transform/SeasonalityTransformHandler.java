package be.ugent.ledc.rulebox.handler.transform;

import be.ugent.ledc.chronos.ChronosException;
import be.ugent.ledc.chronos.algorithms.transform.seasonality.decomposition.Decomposition;
import be.ugent.ledc.chronos.algorithms.transform.seasonality.decomposition.SimpleDecomposer;
import be.ugent.ledc.chronos.algorithms.transform.smoothing.BasicSmootherWeightFunction;
import be.ugent.ledc.chronos.algorithms.transform.smoothing.SmootherWeightFunction;
import be.ugent.ledc.chronos.datastructures.Signal;
import be.ugent.ledc.chronos.datastructures.SignalOperations;
import be.ugent.ledc.chronos.datastructures.TemporalDataset;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.DataWriteException;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.config.parameter.OptionalParameter;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.ParameterException;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.rulebox.handler.binder.BinderBox;
import be.ugent.ledc.rulebox.handler.binder.BinderPool;
import be.ugent.ledc.rulebox.handler.binder.BinderPoolException;
import be.ugent.ledc.rulebox.handler.explore.timeseries.SeasonalityHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SeasonalityTransformHandler extends AddAttributeHandler<Double>
{
    private final Parameter<Integer> cycleParameter = new Parameter<>
    (
        "cycle",
        "--c",
        true,
        (s) -> Integer.valueOf(s),
        "Size of the seasonality cycle size in number of measures."
    )
    .addVerifier(i -> i > 0, "Seasonality cycle must be strictly positive.");
    
    private final Parameter<Double> iqrMultiplierParameter = new OptionalParameter<>
    (
        "iqr-multiplier",
        "--iqrm",
        s -> Double.valueOf(s),
        "When marking outliers, this multiplier is applied to the IQR to determine which points are outliers. Default value is 3.",
        3.0
    ).addVerifier(d -> d > 0, "Multiplier must be strict positive");

    private final Parameter<String> smoothingParameter = new OptionalParameter<>
    (
        "smoothing",
        "--s",
        s -> s,
        "Smoothing function to be used. Options are: "
        + Stream
            .of(SeasonalityHandler.SMOOTHERS)
            .collect(Collectors.joining(",","","."))
        + "Default value is " + SeasonalityHandler.UNIFORM_SMOOTHING,
        SeasonalityHandler.UNIFORM_SMOOTHING
    ).addVerifier(
        s -> Stream
            .of(SeasonalityHandler.SMOOTHERS)
            .anyMatch(sm -> sm.equals(s)),
        "Value for smoothing must be one of : "
        + Stream.of(SeasonalityHandler.SMOOTHERS)
            .collect(Collectors.joining(",")));
     
    private final Flag multiplicativeFlag = new Flag
    (
        "multiplicative",
        "--m",
        "When activated, seasonality decomposition uses a multiplicative model instead of the default additive one."
    );
    
    private final Parameter<Integer> keepParameter = new OptionalParameter<>
    (
        "keep",
        "--k",
        s -> Integer.valueOf(s),
        "3-bit integer to indicate which parts of the decomposition need to be preserved. "
            + "Bit 1: trend, Bit 2: seasonality, Bit 3: residual/noise. "
            + "Default vale is 1 (keep only the trend).",
        1
    ).addVerifier(
        i -> i > 0 && i <= 7,
        "Value of keep parameter needs to be a value between 1 and 7"
    );
        
    public SeasonalityTransformHandler()
    {
        super(
            "seasonality",
            "Creates a new attribute that contains a seasonality decomposition changepoints of the "
                + "original attribute. "
                + "This transformation requires the binder to be a temporal dataset."
        );
        
        addParameter(cycleParameter);
        addParameter(iqrMultiplierParameter);
        addParameter(smoothingParameter);
        addParameter(keepParameter);
        
        addFlag(multiplicativeFlag);
        
        getBinderNameParameter()
            .addVerifier(b -> BinderPool.getInstance().isTemporal(b),
                "This transformation requires a binder that is marked as a temporal dataset.");
    }

    @Override
    public ContractedDataset extend(ContractedDataset data, String originalAttribute, String newAttribute, String [] args) throws ParameterException, DataWriteException
    {
        try
        {
            ContractedDataset extendedDataset = new ContractedDataset(new Contract
                .ContractBuilder(data.getContract())
                .addContractor(newAttribute, TypeContractorFactory.DOUBLE)
                .build());
            
            if(originalAttribute == null
            || !data.getContract().getAttributes().contains(originalAttribute))
            {
                throw new DataWriteException("Attribute '"
                    + originalAttribute
                    + "' does not occur in the dataset.");                
            }

            //Transform
            System.out.println("Applying data transformation...");
            
            Integer cycle = ParameterTools.extractParameter(cycleParameter, args);
            Integer keep = ParameterTools.extractParameter(keepParameter, args);
            boolean multi = ParameterTools.extractFlag(multiplicativeFlag, args);
            String smooth = ParameterTools.extractParameter(smoothingParameter, args);
            
            SmootherWeightFunction swf = null;
                
            switch(smooth)
            {
                case SeasonalityHandler.LINEAR_SMOOTHING:
                    swf = BasicSmootherWeightFunction.LINEAR;
                    break;
                case SeasonalityHandler.UNIFORM_SMOOTHING:
                    swf = BasicSmootherWeightFunction.UNIFORM;
                    break;
                case SeasonalityHandler.TRICUBE_SMOOTHING:
                    swf = BasicSmootherWeightFunction.TRICUBE;
                    break;
                default:
                    super.error("Unknown smoothing function " + smooth);
            }
            
            SimpleDecomposer<Comparable<? super Comparable>,Number> decomposer = new SimpleDecomposer<>
            (
                swf,
                multi
                    ? SimpleDecomposer.MULTIPLICATIVE
                    : SimpleDecomposer.ADDITIVE
            );
            
            BinderBox<?> binderBox = BinderPool
                    .getInstance()
                    .getBinderBox(
                        ParameterTools.extractParameter(getBinderNameParameter(),
                        args));
            
            String tAttribute = binderBox
                .getTemporalInfo()
                .getTimeAttribute();
            
            if(binderBox.isTemporalPartitioned())
            {
                Map<String, TemporalDataset<Comparable<? super Comparable>>> dataMap = binderBox
                    .temporalPartitionedData();
                
                Map<String, Signal> decompositionMap = new HashMap<>();
                
                for(String partitionKey: dataMap.keySet())
                {
                    Signal attributeSignal = dataMap
                        .get(partitionKey)
                        .getAttributeSignal(originalAttribute);

                    Signal newAttributeSignal = keep == 7
                        ? attributeSignal //keep 7 means keep everything
                        : transform(
                            decomposer.decompose(attributeSignal, cycle),
                            keep,
                            multi);
                    
                    decompositionMap.put(partitionKey, newAttributeSignal);
                }
                
                String pAttribute = binderBox
                    .getTemporalInfo()
                    .getPartitionAttribute();
                        
                for(DataObject o: data)
                {
                    DataObject eObject = new DataObject(o);
                    
                    if(o.get(pAttribute) == null)
                    {
                        extendedDataset.addDataObject(eObject);
                        continue;
                    }
                    
                    String pValue = o.get(pAttribute).toString();
                    
                    if(decompositionMap.get(pValue) == null || decompositionMap.get(pValue).isEmpty())
                    {
                        extendedDataset.addDataObject(eObject);
                        continue;
                    }
                    
                    eObject.set(
                         newAttribute,
                         decompositionMap
                            .get(pValue)
                            .valueAt((Comparable) o.get(tAttribute))
                     );
                    
                    extendedDataset.addDataObject(eObject);
                }
            }
            else
            {
                TemporalDataset temporalData = binderBox.temporalData();
                
                Signal attributeSignal = temporalData
                    .getAttributeSignal(originalAttribute);
                  
                Signal newAttributeSignal = keep == 7
                    ? attributeSignal //keep 7 means keep everything
                    : transform(
                        decomposer.decompose(attributeSignal, cycle),
                        keep,
                        multi);  
                
                for(DataObject o: data)
                {
                    DataObject eObject = new DataObject(o);

                    eObject.set(
                        newAttribute,
                        newAttributeSignal.get((Comparable) o.get(tAttribute))
                    );

                    extendedDataset.addDataObject(eObject);                    
                }
            }
            
            return extendedDataset;
        }
        catch (IOException
            | DataWriteException
            | DataReadException
            | ChronosException            
            | BinderPoolException ex)
        {
            System.err.println(ex.getMessage());
            return null;
        }

    }    

    private Signal transform(Decomposition<Comparable<? super Comparable>> decomposition, Integer keep, boolean multiplicative) throws ChronosException
    {
        switch(keep)
        {
            case 1:
                return decomposition.getTrend();
            case 2:
                return decomposition.getSeasonal();
            case 4:
                return decomposition.getNoise();
            case 3: //011
                return SignalOperations.pointwiseAggregate(
                    decomposition.getTrend(),
                    decomposition.getSeasonal(),
                    (multiplicative ? (a,b)-> a*b : Double::sum));
            case 5: //101
                return SignalOperations.pointwiseAggregate(
                    decomposition.getTrend(),
                    decomposition.getNoise(),
                    (multiplicative ? (a,b)-> a*b : Double::sum));
            case 6: //110
                return SignalOperations.pointwiseAggregate(
                    decomposition.getSeasonal(),
                    decomposition.getNoise(),
                    (multiplicative ? (a,b)-> a*b : Double::sum));
            default:
                return null;
        }
    }
}
