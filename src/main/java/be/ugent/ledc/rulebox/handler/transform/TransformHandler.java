package be.ugent.ledc.rulebox.handler.transform;

import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.handler.DataModifier;

public abstract class TransformHandler extends DataHandler implements DataModifier
{
    public TransformHandler(String command, String description)
    {
        super(command, description);
        
        addParameter(repairBinderParameter);
        addFlag(softRepairFlag);
    }
}
