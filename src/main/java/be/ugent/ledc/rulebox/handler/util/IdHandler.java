package be.ugent.ledc.rulebox.handler.util;

public class IdHandler
{
    private int nextId = 0;

    public IdHandler(){}
    
    public String next()
    {
        return "" + (nextId++);
    }
    
}
