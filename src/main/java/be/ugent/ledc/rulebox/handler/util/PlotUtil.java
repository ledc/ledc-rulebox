package be.ugent.ledc.rulebox.handler.util;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.histogram.CategoricalHistogramBuilder;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import be.ugent.ledc.dino.outlierdetection.hbos.OrdinalFixedWidthHistogramBuilder;
import be.ugent.ledc.core.datastructures.histogram.RealFixedWidthHistogramBuilder;
import be.ugent.ledc.rulebox.handler.DataHandler;
import be.ugent.ledc.rulebox.io.result.plot.BoxPlot;
import be.ugent.ledc.rulebox.io.result.plot.HistogramPlot;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PlotUtil
{
    public static Histogram<?> buildHistogram(ContractedDataset data, String a, Integer bins)
    {
        TypeContractor<?> ac = data
            .getContract()
            .getAttributeContract(a);
                
        if(DataHandler.isInteger(ac))
        {           
            return new OrdinalFixedWidthHistogramBuilder()
                .buildHistogram(
                    data.project(a).asLong(a),
                    bins,
                    a,
                    SigmaContractorFactory.LONG);
        }
        else if (DataHandler.isNumeric(ac))
        {          
            return new RealFixedWidthHistogramBuilder()
                .buildHistogram(
                    data.project(a).asDouble(a),
                    bins,
                    a,
                    TypeContractorFactory.DOUBLE);
        }
        else if(ac.equals(TypeContractorFactory.DATE))
        {            
            return new OrdinalFixedWidthHistogramBuilder()
                .buildHistogram(
                    data,
                    bins,
                    a,
                    SigmaContractorFactory.DATE_DAYS);
        }
        else if(ac.equals(TypeContractorFactory.TIME))
        {            
            return new OrdinalFixedWidthHistogramBuilder()
                .buildHistogram(
                    data,
                    bins,
                    a,
                    SigmaContractorFactory.TIME_SECONDS);
        }
        else if(ac.equals(TypeContractorFactory.DATETIME))
        {            
            return new OrdinalFixedWidthHistogramBuilder()
                .buildHistogram(
                    data,
                    bins,
                    a,
                    SigmaContractorFactory.DATETIME_SECONDS);
        }
        else if(ac.equals(TypeContractorFactory.BOOLEAN))
        {            
            return new OrdinalFixedWidthHistogramBuilder()
                .buildHistogram(
                    data,
                    bins,
                    a,
                    SigmaContractorFactory.BOOLEAN);
        }
        else if(ac.equals(TypeContractorFactory.STRING))
        {            
            return new CategoricalHistogramBuilder()
                .buildHistogram(
                    data,
                    bins,
                    a,
                    TypeContractorFactory.STRING);
        }
        else
        {
            return null;
        }
    }
    
    public static HistogramPlot buildHistogramPlot(ContractedDataset data, String a, Integer bins)
    {
        return new HistogramPlot(
            PlotUtil.buildHistogram(
                data,
                a,
                bins),
            a,
            data
                .getContract()
                .getAttributeContract(a)
                .name()
                .equals(SigmaContractorFactory.STRING.name()),
            false
        );
    }
    
    public static BoxPlot buildBoxPlot(ContractedDataset data, String a, String caption)
    {
        List<Double> values = data
                .stream()
                .filter(o -> o.get(a) != null)
                .map(o -> ((Number)o.get(a)).doubleValue())
                .sorted()
                .collect(Collectors.toList());
            
            if(values.isEmpty())
                return null;
            
            int i1 = (int)((values.size()-1.0)/ 4.0);
            int i2 = (int)((values.size()-1.0)/ 2.0);
            int i3 = (int)((values.size()-1.0) * 3.0 / 4.0);
            
            double q1 = values.get(i1);
            double q2 = values.get(i2);
            double q3 = values.get(i3);
            
            double iqr = q3 - q1;
            
            Double min = null;
            Double max = null;
        
            List<Double> lowOutliers = new ArrayList<>();
            List<Double> highOutliers = new ArrayList<>();
            
            for(Double value: values)
            {
                if(value < q1 - 1.5 * iqr)
                {
                    if(lowOutliers.isEmpty() || !lowOutliers.get(lowOutliers.size()-1).equals(value))
                        lowOutliers.add(value);
                }
                else if(value <= q3 + 1.5 * iqr)
                { 
                    if(min == null)
                        min = value;
                    
                    max = value;
                }
                else
                {
                    if(highOutliers.isEmpty() || !highOutliers.get(highOutliers.size()-1).equals(value))
                        highOutliers.add(value);
                }
            }
            
            return new BoxPlot(
                min,
                q1,
                q2,
                q3,
                max,
                lowOutliers,
                highOutliers,
                caption);
    }
}
