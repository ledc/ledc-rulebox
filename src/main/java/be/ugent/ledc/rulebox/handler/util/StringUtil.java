package be.ugent.ledc.rulebox.handler.util;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StringUtil
{
    public static int padSize(Set<String> strings)
    {
        return strings
        .stream()
        .mapToInt(n -> n.length())
        .max()
        .getAsInt();
    }
    
    public static String pad(String name, int w)
    {
        return name.concat(
            IntStream
            .rangeClosed(0, w - name.length())
            .mapToObj(i -> " ").collect(Collectors.joining()));
    }
    
    public static String indent(String s, int i)
    {
        return IntStream
            .rangeClosed(1, i)
            .mapToObj(j -> "  ")
            .collect(Collectors.joining())
            .concat(s);
    }
}
