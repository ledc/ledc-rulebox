package be.ugent.ledc.rulebox.io;

import java.util.Scanner;

public class ConsoleUtil
{
    public static String readLine()
    {
        return System.console() == null
            ? new Scanner(System.in).nextLine()
            : System.console().readLine();
    }
    
    public static String readPassword()
    {
        return System.console() == null
            ? new Scanner(System.in).nextLine()
            : new String(System.console().readPassword());
    }
}
