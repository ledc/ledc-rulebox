package be.ugent.ledc.rulebox.io.result;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * A container bundles a list of items of the same type that should be visualized jointly.
 * @author abronsel
 * @param <I> 
 */
public abstract class Container<I> implements Marshaller
{
    private final List<I> items;
    
    private final String caption;
    
    private final String id;

    public Container(List<I> items, String caption, String id)
    {
        this.items = items;
        this.caption = caption;
        this.id = id;
    }
    
    public Container(String caption, String id)
    {
        this(new ArrayList<>(), caption, id);
    }

    public void addItem(I item)
    {
        this.items.add(item);
    }
    
    public void clear()
    {
        this.items.clear();
    }

    public String getCaption()
    {
        return caption;
    }
    
    protected List<I> getItems()
    {
        return items; 
    }
    
    public Element createContainerNode(Document document)
    {
        Element containerNode = document.createElement("container");
        
        containerNode.setAttribute("type", createContainerType());
        containerNode.setAttribute("id", id);
        
        Element containerMeta = document.createElement("metadata");
        
        addChild(document, containerMeta, "caption", caption);

        containerNode.appendChild(containerMeta);
        
        return containerNode;
    }
    
    public abstract String createContainerType();
    
    public boolean isEmpty()
    {
        return items.isEmpty();
    }
}
