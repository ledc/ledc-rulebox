package be.ugent.ledc.rulebox.io.result;

public class MarshallException extends Exception
{
    public MarshallException(String message)
    {
        super(message);
    }

    public MarshallException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MarshallException(Throwable cause)
    {
        super(cause);
    }
}
