package be.ugent.ledc.rulebox.io.result;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * This interface enforces an object to be serializable to XML.
 * @author abronsel
 */
public interface Marshaller
{
    public void marshall(Document document, Node parent);
    
    default void addChild(Document document, Node parent, String childName, String content)
    {
        Element child = document.createElement(childName);
        child.setTextContent(content);
        parent.appendChild(child);
    }
}
