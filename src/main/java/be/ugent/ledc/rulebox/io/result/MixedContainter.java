package be.ugent.ledc.rulebox.io.result;

import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class MixedContainter extends Container<Container<?>>
{
    public MixedContainter(List<Container<?>> items, String caption, String id)
    {
        super(items, caption, id);
    }

    public MixedContainter(String caption, String id)
    {
        super(caption, id);
    }

    @Override
    public String createContainerType()
    {
        return "mixed-container";
    }

    @Override
    public void marshall(Document document, Node parent)
    {
        //Create a node for the container
        Element containerNode = createContainerNode(document);
        
        for(Container<?> nestedContainer : getItems())
        {
            nestedContainer.marshall(document, containerNode);
        }
        
        //Append the container to the parent
        parent.appendChild(containerNode);
    }
    
}
