package be.ugent.ledc.rulebox.io.result;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OutputFactory
{
    private static final int INDENT_SIZE = 2;

    public static final String ROOT_ELEMENT = "Root";

    public static void dump(List<Container<?>> containers, File file) throws FileNotFoundException, MarshallException
    {
        marshall(
            containers, 
            new PrintWriter(file),
            file.getName().toLowerCase().endsWith(".html"));
    }

    public static void marshall(List<Container<?>> containers, Writer writer, boolean transform) throws MarshallException
    {
        // Check if object is null
        if (containers == null)
        {
            throw new MarshallException("Null can not be marshalled.");
        }

        // Create Document Builder Factory
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();

        try
        {
            // Instantiate Document Builder
            DocumentBuilder b = f.newDocumentBuilder();

            // Initialize the document
            Document document = b.newDocument();
            
            //Initialize a root node
            Element root = document.createElement("containers");
            
            for(Container<?> container: containers)
            {
                // Build the document
                container.marshall(document, root);
            }
            
            //Append root node
            document.appendChild(root);
            
            // Build a transformer factory
            TransformerFactory transformerFactory = TransformerFactory.newInstance();

            // Build a transformer
            Transformer transformer = transform
                ? transformerFactory
                    .newTransformer(
                        new StreamSource(OutputFactory
                            .class
                            .getClassLoader()
                            .getResourceAsStream("transformation.xml")))
                : transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            
            if(transform)
                transformer.setOutputProperty(OutputKeys.METHOD, "html");
            else
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            
            transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", 
                Integer.toString(INDENT_SIZE)
            );

            // Build a stream result
            StreamResult result = new StreamResult(writer);

            // Make a dom source of the document
            DOMSource source = new DOMSource(document);

            // Transform to the document
            transformer.transform(source, result);
        }
        catch (ParserConfigurationException | IllegalArgumentException | ClassCastException | TransformerException ex)
        {
            throw new MarshallException(ex);
        }
    }

}
