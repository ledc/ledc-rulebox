package be.ugent.ledc.rulebox.io.result.plot;

import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class BoxPlot extends Plot
{
    private final double min;
    private final double q1;
    private final double q2;
    private final double q3;
    private final double max;
    private final List<Double> lowOutliers;
    private final List<Double> highOutliers;

    public BoxPlot(double min, double q1, double q2, double q3, double max, List<Double> lowOutliers, List<Double> highOutliers, String caption)
    {
        super(caption);
        this.min = min;
        this.q1 = q1;
        this.q2 = q2;
        this.q3 = q3;
        this.max = max;
        this.lowOutliers = lowOutliers;
        this.highOutliers = highOutliers;
    }
        
    @Override
    public String plotType()
    {
        return "boxplot";
    }

    @Override
    public PlotSettings defaultSettings()
    {
        return new PlotSettings(
            600,
            600,
            25,
            25,
            50,
            50);
    }

    @Override
    public void marshall(Document document, Node parent)
    {
        Element plotNode = super.createPlotNode(document, defaultSettings());
        
        addChild(document, plotNode, "min", Double.toString(min));
        addChild(document, plotNode, "q1", Double.toString(q1));
        addChild(document, plotNode, "q2", Double.toString(q2));
        addChild(document, plotNode, "q3", Double.toString(q3));
        addChild(document, plotNode, "max", Double.toString(max));
        
        if(!lowOutliers.isEmpty())
        {
            Element lowOutlierNode = document.createElement("lower-outliers");
            
            for(Double outlier: lowOutliers)
            {
                addChild(document, lowOutlierNode, "outlier", Double.toString(outlier));
            }
            
            plotNode.appendChild(lowOutlierNode);
        }
        
        if(!highOutliers.isEmpty())
        {
            Element highOutlierNode = document.createElement("higher-outliers");
            
            for(Double outlier: highOutliers)
            {
                addChild(document, highOutlierNode, "outlier", Double.toString(outlier));
            }
            
            plotNode.appendChild(highOutlierNode);
        }
        
        //Add the plot
        parent.appendChild(plotNode);
    }
    
}
