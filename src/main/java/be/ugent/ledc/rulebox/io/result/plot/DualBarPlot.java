package be.ugent.ledc.rulebox.io.result.plot;

import be.ugent.ledc.core.datastructures.Couple;
import java.util.Map;
import java.util.Objects;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DualBarPlot<N> extends Plot
{
    private final Map<String, Couple<N>> dualBarData;
    
    private final boolean hoverLabels;
    
    private final String bar1;
    
    private final String bar2;

    public DualBarPlot(Map<String, Couple<N>> dualBarData, String caption, boolean hoverLabels, String bar1, String bar2)
    {
        super(caption);
        this.dualBarData = dualBarData;
        this.hoverLabels = hoverLabels;
        this.bar1 = bar1;
        this.bar2 = bar2;
    }

    public Map<String, Couple<N>> getDualBarData()
    {
        return dualBarData;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.dualBarData);
        hash = 23 * hash + (this.hoverLabels ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final DualBarPlot other = (DualBarPlot) obj;
        if (this.hoverLabels != other.hoverLabels)
        {
            return false;
        }
        if (!Objects.equals(this.dualBarData, other.dualBarData))
        {
            return false;
        }
        return true;
    }
    
    @Override
    public void marshall(Document document, Node parent)
    {
        //Number of bins
        int bins = dualBarData.size();
        
        int maxLabelChars = maxLabelChars(bins);
        
        //Check histogram labels
        int maxLabelSize = dualBarData
            .keySet()
            .stream()
            .mapToInt(binLabel -> binLabel.length())
            .max()
            .getAsInt();
        
        PlotSettings settings = defaultSettings();
        
        if(!hoverLabels)
        {
            if(maxLabelSize > maxLabelChars)
            {
                //Adjust lower margin and height
                int adjust = (int)((maxLabelSize - 4) * 5.5);

                settings.withBottomMargin(settings.getBottomMargin() + adjust);
                settings.withHeight(settings.getHeight() + adjust);
            }
        }
        
        Element plotNode = super.createPlotNode(document, settings);
        
        //Set vertical labelling
        plotNode.setAttribute("vertical-labels", Boolean.toString(!hoverLabels && maxLabelSize > maxLabelChars));
        
        //Set hover-over labels labelling
        plotNode.setAttribute("hover-labels", Boolean.toString(hoverLabels));
        
        for(String binLabel: dualBarData.keySet())
        {
            Element pointNode   = document.createElement("point");
            Element labelNode   = document.createElement("label");
            Element value1Node  = document.createElement("value");
            Element value2Node  = document.createElement("value");
            
            labelNode.setTextContent(binLabel);
            
            value1Node.setTextContent(dualBarData.get(binLabel).getFirst().toString());
            value1Node.setAttribute("bar", "1");
            value1Node.setAttribute("bar-label", bar1);
            value2Node.setTextContent(dualBarData.get(binLabel).getSecond().toString());
            value2Node.setAttribute("bar", "2");
            value2Node.setAttribute("bar-label", bar2);
            
            //Add point info
            pointNode.appendChild(labelNode);
            pointNode.appendChild(labelNode);
            pointNode.appendChild(value1Node);
            pointNode.appendChild(value2Node);
            
            //Add point
            plotNode.appendChild(pointNode);
        }
        
        //Add the plot
        parent.appendChild(plotNode);
        
    }   

    @Override
    public String plotType()
    {
        return "dual-bar";
    }

    private int maxLabelChars(int bins)
    {
        if(bins <= 5)
            return 10;
        
        if(bins <= 7)
            return 6;
        
        if(bins <= 8)
            return 5;
        
        if(bins <= 10)
            return 4;
        
        if(bins <= 15)
            return 3;
        
        if(bins <= 20)
            return 2;
        
        return 1;
    }

    @Override
    public PlotSettings defaultSettings()
    {
        return new PlotSettings(
            600,
            600,
            50,
            50,
            50,
            50);
    }
}
