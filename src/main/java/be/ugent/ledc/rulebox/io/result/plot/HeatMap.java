package be.ugent.ledc.rulebox.io.result.plot;

import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class HeatMap extends Plot
{
    private final int[][] data;
    
    private final Map<Integer, String> indexLabelMap;

    public HeatMap(int[][] data, Map<Integer, String> indexLabelMap, String caption)
    {
        super(caption);
        this.data = data;
        this.indexLabelMap = indexLabelMap;
    }
    
    @Override
    public String plotType()
    {
        return "heatmap";
    }

    @Override
    public PlotSettings defaultSettings()
    {
        return new PlotSettings(
            600,
            600,
            25,
            25,
            50,
            50);
    }

    @Override
    public void marshall(Document document, Node parent)
    {
        Element plotNode = super.createPlotNode(document, defaultSettings());
       
        for(Integer index: indexLabelMap.keySet())
        {
            Element labelElement = document.createElement("label");
            
            labelElement.setAttribute("index", Integer.toString(index));
            labelElement.setAttribute("value", indexLabelMap.get(index));
            
            plotNode.appendChild(labelElement);
        }
        
        plotNode.setAttribute("size", Integer.toString(data.length));
        
        int max = 0;
        
        for(int i=0; i<data.length; i++)
        {       
            
            
            

            for(int j=0; j<data[i].length; j++)
            {
                if(data[i][j] != 0)
                {
                    Element cellElement = document.createElement("cell");
                    cellElement.setAttribute("row", Integer.toString(i));
                    cellElement.setAttribute("column", Integer.toString(j));
                    cellElement.setAttribute("value", Double.toString(data[i][j]));
                    max = Math.max(max, data[i][j]);
                    plotNode.appendChild(cellElement);
                }   
            }
        }
        
        plotNode.setAttribute("maximum", Double.toString(max));
        
        parent.appendChild(plotNode);
    }
    
}
