package be.ugent.ledc.rulebox.io.result.plot;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import java.util.Objects;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class HistogramPlot extends Plot
{
    private final Histogram<?> histogram;
    
    private final boolean categorical;
    
    private final boolean hoverLabels;
    
    private final Long fixedCap;

    public HistogramPlot(Histogram<?> histogram, String caption, boolean categorical, boolean hoverLabels, Long fixedCap)
    {
        super(caption);
        this.histogram = histogram;
        this.categorical = categorical;
        this.hoverLabels = hoverLabels;
        
        //Make sure fixedCap is chosen sane (if specified)
        this.fixedCap = fixedCap != null
            ? Math.max(fixedCap, this.histogram.maxCount())
            : null;
    }
    
    public HistogramPlot(Histogram<?> histogram, String caption, boolean categorical, boolean hoverLabels)
    {
        this(histogram, caption, categorical, hoverLabels, null);
    }

    public Histogram<?> getHistogram()
    {
        return histogram;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.histogram);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final HistogramPlot other = (HistogramPlot) obj;
        if (!Objects.equals(this.histogram, other.histogram))
        {
            return false;
        }
        return true;
    }

    @Override
    public void marshall(Document document, Node parent)
    {
        //Number of bins
        int bins = histogram.numberOfBins();
        
        int maxLabelChars = maxLabelChars(bins);
        
        //Check histogram labels
        int maxLabelSize = histogram
            .stream()
            .mapToInt(bin -> bin
                .getLeftBound()
                .toString()
                .length())
            .max()
            .getAsInt();
        
        PlotSettings settings = defaultSettings();
        
        if(!hoverLabels)
        {
            if(maxLabelSize > maxLabelChars)
            {
                //Adjust lower margin and height
                int adjust = (int)((maxLabelSize - 4) * 5.5);

                settings.withBottomMargin(settings.getBottomMargin() + adjust);
                settings.withHeight(settings.getHeight() + adjust);
            }
        }
        
        Element plotNode = super.createPlotNode(document, settings);
        
        //Set categorical
        plotNode.setAttribute("categorical", Boolean.toString(categorical));
        
        //Set vertical labelling
        plotNode.setAttribute("vertical-labels", Boolean.toString(!hoverLabels && maxLabelSize > maxLabelChars));
        
        //Set hover-over labels labelling
        plotNode.setAttribute("hover-labels", Boolean.toString(hoverLabels));
        
        if(fixedCap != null)
            plotNode.setAttribute("fixed-cap", Long.toString(fixedCap));
        
        for(Interval bin: histogram)
        {
            Element pointNode       = document.createElement("point");
            Element lowerBoundNode  = document.createElement("lowerbound");
            Element upperBoundNode  = document.createElement("upperbound");
            Element valueNode       = document.createElement("value");
            
            lowerBoundNode.setTextContent(bin.getLeftBound().toString());
            upperBoundNode.setTextContent(bin.getRightBound().toString());
            valueNode.setTextContent(histogram.countFor(bin).toString());
            
            //Add point info
            pointNode.appendChild(lowerBoundNode);
            pointNode.appendChild(upperBoundNode);
            pointNode.appendChild(valueNode);
            
            //Add point
            plotNode.appendChild(pointNode);
        }
        
        //Add the plot
        parent.appendChild(plotNode);
        
    }   

    @Override
    public String plotType()
    {
        return "histogram";
    }

    private int maxLabelChars(int bins)
    {
        if(bins <= 5)
            return 10;
        
        if(bins <= 7)
            return 6;
        
        if(bins <= 8)
            return 5;
        
        if(bins <= 10)
            return 4;
        
        if(bins <= 15)
            return 3;
        
        if(bins <= 20)
            return 2;
        
        return 1;
    }

    @Override
    public PlotSettings defaultSettings()
    {
        return new PlotSettings(
            600,
            600,
            50,
            50,
            50,
            50);
    }
}
