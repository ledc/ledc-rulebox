package be.ugent.ledc.rulebox.io.result.plot;

import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Line<X extends Comparable<? super X>, Y extends Number>
{
    private final String label;
    
    private final List<Pair<X, Y>> data;
    
    private boolean interpolate;
    
    private boolean symbols;
    
    private boolean emphasize;
    
    private final Map<X, String> marks;

    public Line(String label, List<Pair<X, Y>> data, boolean interpolate, boolean symbols, boolean emphasize)
    {
        this.label = label;
        this.data = data;
        this.interpolate = interpolate;
        this.symbols = symbols;
        this.emphasize = emphasize;
        this.marks = new HashMap<>();
    }
    
    public Line(String label, List<Pair<X, Y>> data)
    {
        this(label, data, true, true, false);
    }

    public String getLabel()
    {
        return label;
    }

    public List<Pair<X, Y>> getData()
    {
        return data;
    }

    public boolean isInterpolate()
    {
        return interpolate;
    }

    public void setInterpolate(boolean interpolate)
    {
        this.interpolate = interpolate;
    }

    public boolean isSymbols()
    {
        return symbols;
    }

    public void setSymbols(boolean symbols)
    {
        this.symbols = symbols;
    }

    public boolean isEmphasize()
    {
        return emphasize;
    }

    public void setEmphasize(boolean emphasize)
    {
        this.emphasize = emphasize;
    }    
    
    public void mark(X x, String markType) throws MarshallException
    {
        //Sanity check
        if(data.stream().noneMatch(p -> p.getFirst().equals(x)))
            throw new MarshallException("Cannot add mark '" + markType + "' for unknown x-value '" + x + "'.");
        
        this.marks.put(x,markType);
    }
    
    public boolean hasMark(X x)
    {
        return this.marks.get(x) != null;
    }
    
    public String getMark(X x)
    {
        return this.marks.get(x);
    }
    
    public Stream<Pair<X,Y>> stream()
    {
        return this.data.stream();
    }
}
