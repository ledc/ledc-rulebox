package be.ugent.ledc.rulebox.io.result.plot;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.rulebox.io.result.MarshallException;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class LinePlot<X extends Comparable<? super X>, Y extends Number> extends Plot
{
    private final List<Line<X,Y>> data;
    
    private final OrdinalContractor<X> contractor;
    
    private int ticks = 5;

    private Y fixedLowerBound;
    
    private Y fixedUpperBound;

    public LinePlot(OrdinalContractor<X> contractor, String caption, List<Line<X,Y>> data) throws MarshallException
    {
        super(caption);
        this.data = data;
        this.contractor = contractor;
        
        //Sanity check
        if(data.size() > 4)
            throw new MarshallException("Maximally 4 lines allowed in line plot");
    }
    
    public LinePlot(OrdinalContractor<X> contractor, String caption, Line<X,Y> ... data) throws MarshallException
    {
        this(contractor, caption, ListOperations.list(data));
    }
    
    @Override
    public void marshall(Document document, Node parent)
    {       
        PlotSettings settings = defaultSettings();
               
        Element plotNode = super.createPlotNode(document, settings);

        if(fixedLowerBound != null)
            plotNode.setAttribute("fixed-lower-bound", fixedLowerBound.toString());
        
        if(fixedUpperBound != null)
            plotNode.setAttribute("fixed-upper-bound", fixedUpperBound.toString());
        
        X minIndex = data
        .stream()
        .flatMap(list -> list.stream())
        .filter(pair -> pair.getFirst()!= null)
        .map(pair -> pair.getFirst())
        .min(Comparator.naturalOrder())
        .get();
        
        X maxIndex = data
        .stream()
        .flatMap(list -> list.stream())
        .filter(pair -> pair.getFirst()!= null)
        .map(pair -> pair.getFirst())
        .max(Comparator.naturalOrder())
        .get();
        
        int indexRange = (int)(contractor
            .cardinality(
                new Interval<>(minIndex,maxIndex, false, false)
            ) - 1);
        
        Set<Integer> tickIndices = IntStream
            .rangeClosed(1, ticks - 1)
            .boxed()
            .map(i -> indexRange <= ticks
                    ? i
                    : i * (indexRange / ticks)
            )
            .collect(Collectors.toSet());

        tickIndices.add(0);
        tickIndices.add(indexRange);
        
        int labelw = 0;
        
        for(Integer tickIndex: tickIndices)
        {
            //Initialize line node
            Element tickNode = document.createElement("tick");
            
            String label = contractor
                .add(minIndex, tickIndex)
                .toString();
            
            tickNode.setAttribute("axis", "x");
            tickNode.setAttribute("index", Integer.toString(tickIndex));
            tickNode.setAttribute("label", label);
            
            labelw = Math.max(labelw, label.length());
            
            plotNode.appendChild(tickNode);
        }
        
        if(labelw > 20)
            plotNode.setAttribute("tickrotation", "true");
        
        for(Line<X,Y> line: data)
        {
            //Initialize line node
            Element lineNode = document.createElement("line");
            
            lineNode.setAttribute("label", line.getLabel());
            lineNode.setAttribute("interpolate", Boolean.toString(line.isInterpolate()));
            lineNode.setAttribute("symbols", Boolean.toString(line.isSymbols()));
            lineNode.setAttribute("emphasize", Boolean.toString(line.isEmphasize()));
            
            for(Pair<X, Y> point: line.getData())
            {
                //Initialize point node
                Element pointNode = document.createElement("point");
                
                X idx = point.getFirst();
                
                int index = (int)(contractor.cardinality(Interval.closed(minIndex,idx)) - 1);
                
                pointNode.setAttribute("x", idx.toString());
                pointNode.setAttribute("xindex", Integer.toString(index));
                
                if(point.getSecond() != null)
                    pointNode.setAttribute("y", point.getSecond().toString());
                
                //If this point needs marking, set mark indicator true
                if(line.hasMark(point.getFirst()))
                    pointNode.setAttribute("mark", line.getMark(point.getFirst()));
                
                lineNode.appendChild(pointNode);
            }
            
            //Add line to plot
            plotNode.appendChild(lineNode);
        }
        
        //Add the plot
        parent.appendChild(plotNode);
        
    }   

    @Override
    public String plotType()
    {
        return "lines";
    }

    public int getTicks()
    {
        return ticks;
    }

    public void setTicks(int ticks)
    {
        this.ticks = ticks;
    }

    public Y getFixedLowerBound()
    {
        return fixedLowerBound;
    }

    public Y getFixedUpperBound()
    {
        return fixedUpperBound;
    }

    public void setFixedUpperBound(Y fixedUpperBound)
    {
        this.fixedUpperBound = fixedUpperBound;
    }

    public void setFixedLowerBound(Y fixedLowerBound)
    {
        this.fixedLowerBound = fixedLowerBound;
    }
    
    @Override
    public PlotSettings defaultSettings()
    {
        return new PlotSettings(
            600,
            600,
            50,
            50,
            50,
            50);
    }
}
