package be.ugent.ledc.rulebox.io.result.plot;

import be.ugent.ledc.rulebox.io.result.Marshaller;
import java.util.Objects;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class Plot implements Marshaller
{
    private final String caption;

    public Plot(String caption)
    {
        this.caption = caption;
    }

    public String getCaption()
    {
        return caption;
    }
     
    public Element createPlotNode(Document document, PlotSettings settings)
    {
        Element plotNode = document.createElement("plot");
        
        plotNode.setAttribute("type", plotType());
        
        Element plotMeta = document.createElement("settings");
        
        addChild(document, plotMeta, "caption", caption);
        addChild(document, plotMeta, "width", Integer.toString(settings.getWidth()));
        addChild(document, plotMeta, "height", Integer.toString(settings.getHeight()));
        addChild(document, plotMeta, "leftmargin", Integer.toString(settings.getLeftMargin()));
        addChild(document, plotMeta, "rightmargin", Integer.toString(settings.getRightMargin()));
        addChild(document, plotMeta, "topmargin", Integer.toString(settings.getTopMargin()));
        addChild(document, plotMeta, "bottommargin", Integer.toString(settings.getBottomMargin()));
        
        plotNode.appendChild(plotMeta);
        
        return plotNode;
    }
    
    public abstract String plotType();
    
    public abstract PlotSettings defaultSettings();

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.caption);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Plot other = (Plot) obj;
        if (!Objects.equals(this.caption, other.caption))
        {
            return false;
        }
        return true;
    }
    
}
