package be.ugent.ledc.rulebox.io.result.plot;

import be.ugent.ledc.rulebox.io.result.Container;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class PlotContainer extends Container<Plot>
{
    private final int gridWidth;

    public PlotContainer(List<Plot> plots, String caption, String id, int gridWidth)
    {
        super(plots, caption, id);
        this.gridWidth = gridWidth;
    }
    
    public PlotContainer(String caption, String id, int gridWidth)
    {
        this(new ArrayList<>(), caption, id, gridWidth);
    }
    
    public PlotContainer(String caption, String id)
    {
        this(caption, id, 3);
    }

    public int getGridWidth()
    {
        return gridWidth;
    }

    @Override
    public void marshall(Document document, Node parent)
    {
        //Create a node for the container
        Element containerNode = createContainerNode(document);
        
        containerNode.setAttribute("grid-width", String.valueOf(gridWidth));
        
        List<Element> rows = new ArrayList<>();
        
        for(int i=0; i<getItems().size(); i++)
        {
            if(i%gridWidth == 0)
                rows.add(document.createElement("row"));
            
            getItems()
                .get(i)
                .marshall(document, rows.get(rows.size() - 1));
        }
        
        //Append each row to the container
        rows.forEach(containerNode::appendChild);
        
        //Append the container to the parent
        parent.appendChild(containerNode);
    }

    @Override
    public String createContainerType()
    {
        return "plot-container";
    }
}
