package be.ugent.ledc.rulebox.io.result.plot;

public class PlotSettings
{
    private int width;
    
    private int height;
    
    private int leftMargin;
    
    private int rightMargin;
    
    private int topMargin;
    
    private int bottomMargin;

    public PlotSettings(int width, int height, int leftMargin, int rightMargin, int topMargin, int bottomMargin)
    {
        this.width = width;
        this.height = height;
        this.leftMargin = leftMargin;
        this.rightMargin = rightMargin;
        this.topMargin = topMargin;
        this.bottomMargin = bottomMargin;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public int getLeftMargin()
    {
        return leftMargin;
    }

    public int getRightMargin()
    {
        return rightMargin;
    }

    public int getTopMargin()
    {
        return topMargin;
    }

    public int getBottomMargin()
    {
        return bottomMargin;
    }
    
    public PlotSettings withWidth(int width)
    {
        this.width = width;
        return this;
    }
    
    public PlotSettings withHeight(int height)
    {
        this.height = height;
        return this;
    }
    
    public PlotSettings withLeftMargin(int leftMargin)
    {
        this.leftMargin = leftMargin;
        return this;
    }
    
    public PlotSettings withRightMargin(int rightMargin)
    {
        this.rightMargin = rightMargin;
        return this;
    }
    
    public PlotSettings withTopMargin(int topMargin)
    {
        this.topMargin = topMargin;
        return this;
    }
    
    public PlotSettings withBottomMargin(int bottomMargin)
    {
        this.bottomMargin = bottomMargin;
        return this;
    }
}
