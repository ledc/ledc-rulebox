package be.ugent.ledc.rulebox.io.result.table;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.rulebox.Config;
import be.ugent.ledc.rulebox.io.result.Container;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TableContainer extends Container<ContractedDataset>
{
    private final ContractedDataset data;
    
    private final List<String> columnOrder;
    
    private final boolean stretch;

    public TableContainer(ContractedDataset data, String caption, String id, List<String> columnOrder, boolean stretch)
    {
        super(ListOperations.list(data), caption, id);
        this.data = data;
        this.columnOrder = columnOrder;
        
        //Clean order
        this.columnOrder.removeIf(a -> !data.getContract().getAttributes().contains(a));
        
        //Complete order
        data
            .getContract()
            .getAttributes()
            .stream()
            .filter(a -> !columnOrder.contains(a))
            .forEach(columnOrder::add);
        
        this.stretch = stretch;
    }
    
    public TableContainer(ContractedDataset data, String caption, String id, List<String> columnOrder)
    {
        this(data, caption, id, columnOrder, false);
    }
    
    public TableContainer(ContractedDataset data, String caption, String id)
    {
        this(data, caption, id, new ArrayList<>());
    }

    @Override
    public String createContainerType()
    {
        return "table-container";
    }

    @Override
    public void marshall(Document document, Node parent)
    {
        //Create a node for the container
        Element containerNode = createContainerNode(document);
        
        containerNode.setAttribute("stretch", Boolean.toString(stretch));
        
        //Add a node with the header
        containerNode.appendChild(createHeaderNode(document));
        
        for(DataObject o: data)
        {
            containerNode.appendChild(createTupleNode(document, o));
        }
        
        //Append the container to the parent
        parent.appendChild(containerNode);
    }

    private Node createHeaderNode(Document document)
    {
        Element headerNode = document.createElement("header");
        
        for(String a: columnOrder)
        {
            addChild(document, headerNode, "attribute", a);
        }
        
        return headerNode;
    }

    protected Node createTupleNode(Document document, DataObject o)
    {
        Element tupleNode = document.createElement("tuple");
        
        for(String a: columnOrder)
        {
            TypeContractor<?> contractor = data
            .getContract()
            .getAttributeContract(a);
            
            Element attribute = document.createElement("attribute");
            
            String value = o.get(a) == null
                ? Config.NULL_SYMBOL
                : contractor
                    .getFromDataObject(o, a)
                    .toString();
            
            attribute.setAttribute("name", a);
            attribute.setAttribute("value", value);
//            addChild(document, tupleNode, "attribute", a);
//            addChild(
//                document,
//                tupleNode,
//                "value",
//                
//            );

            tupleNode.appendChild(attribute);
        }
        
        return tupleNode;
    }
    
    @Override
    public boolean isEmpty()
    {
        return data == null || data.getSize() == 0;
    }
}
