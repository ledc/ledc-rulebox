package be.ugent.ledc.rulebox.io.result.table;

import java.util.Objects;
import java.util.Set;

public class Violation
{
    private final String violationType; 
    
    private final String violation; 
    
    private final Set<String> attributes;

    public Violation(String violationType, String violation, Set<String> attributes)
    {
        this.violationType = violationType;
        this.violation = violation;
        this.attributes = attributes;
    }

    public String getViolationType()
    {
        return violationType;
    }

    public String getViolation()
    {
        return violation;
    }

    public Set<String> getAttributes()
    {
        return attributes;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.violationType);
        hash = 97 * hash + Objects.hashCode(this.violation);
        hash = 97 * hash + Objects.hashCode(this.attributes);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Violation other = (Violation) obj;
        if (!Objects.equals(this.violationType, other.violationType))
        {
            return false;
        }
        if (!Objects.equals(this.violation, other.violation))
        {
            return false;
        }
        if (!Objects.equals(this.attributes, other.attributes))
        {
            return false;
        }
        return true;
    } 
}
