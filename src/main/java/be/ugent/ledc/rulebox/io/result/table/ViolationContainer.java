package be.ugent.ledc.rulebox.io.result.table;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ViolationContainer extends TableContainer
{
    private final Map<DataObject, Set<Violation>> violations;
    
    private final Map<DataObject, Long> objectCounts;

    public ViolationContainer(ContractedDataset data, Map<DataObject, Set<Violation>> violations, String caption, String id, Map<DataObject, Long> objectCounts)
    {
        super(data, caption, id, new ArrayList<>(), true);
        this.violations = violations;
        this.objectCounts = objectCounts;
    }
    
    public ViolationContainer(ContractedDataset data, Map<DataObject, Set<Violation>> violations, String caption, String id)
    {
        super(data, caption, id, new ArrayList<>(), true);
        this.violations = violations;
        this.objectCounts = new HashMap<>();
    }

    @Override
    protected Node createTupleNode(Document document, DataObject o)
    {
        Node tupleNode = super.createTupleNode(document, o);
        
        NodeList childNodes = tupleNode.getChildNodes();
        
        if(violations.get(o) == null || violations.get(o).isEmpty())
            return tupleNode;
        
        for(int i=0;i<childNodes.getLength(); i++)
        {
            Node child = childNodes.item(i);
            
            if(child.getNodeType() == Node.ELEMENT_NODE && child.getNodeName().equals("attribute"))
            {
                String a = ((Element)child).getAttribute("name");
                
                Set<Violation> selection = violations
                    .get(o)
                    .stream()
                    .filter(v ->v.getAttributes().contains(a))
                    .collect(Collectors.toSet());
                
                if(selection.isEmpty())
                    continue;
                
                Element violationsElement = document.createElement("violations");
                
                if(objectCounts.get(o) != null)
                    violationsElement.setAttribute("count", objectCounts.get(o).toString());
                
                for(Violation v: selection)
                {
                    Element vElement = document.createElement("violation");
                    
                    vElement.setAttribute("type", v.getViolationType());
                    vElement.setTextContent(v.getViolation());
                    
                    violationsElement.appendChild(vElement);
                }
                
                child.appendChild(violationsElement);
            }
        }
        
        return tupleNode;
    }
}
