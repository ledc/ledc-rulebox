package be.ugent.ledc.rulebox.io.rubix;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.DamerauDistanceCostFunction;
import be.ugent.ledc.core.cost.LevenshteinDistanceCostFunction;
import be.ugent.ledc.core.cost.SimpleErrorFunction;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.InterpretationException;
import be.ugent.ledc.rulebox.Config;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import static be.ugent.ledc.sigma.io.ConstraintIo.CONTRACT_PREFIX;
import static be.ugent.ledc.sigma.io.ConstraintIo.CONTRACT_SEPARATOR;
import be.ugent.ledc.core.cost.errormechanism.ErrorMechanism;
import be.ugent.ledc.core.cost.errormechanism.datetime.DatetimeErrors;
import be.ugent.ledc.core.cost.errormechanism.decimal.DecimalErrors;
import be.ugent.ledc.core.cost.errormechanism.integer.IntegerErrors;
import be.ugent.ledc.sigma.repair.cost.functions.DistanceCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.PreferenceCostFunction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CostFunctionParser
{
    public static final String CONSTANT     = "constant";
    public static final String DISTANCE     = "distance";
    public static final String ERROR        = "error";
    public static final String PREFERENCE   = "preference";
    public static final String LEVENSTEIN   = "levenstein";
    public static final String DAMERAU      = "damerau";
   
    private static final Set<String> TYPES = SetOperations.set(
        CONSTANT,
        DISTANCE,
        ERROR,
        PREFERENCE,
        LEVENSTEIN,
        DAMERAU
    );
    
    public static String build(CostFunction cf)
    {
        if(cf instanceof ConstantCostFunction constantCostFunction)
        {
            return buildConstantCost(constantCostFunction);
        }
        else if(cf instanceof DistanceCostFunction)
        {
            return DISTANCE;
        }
        else if(cf instanceof LevenshteinDistanceCostFunction)
        {
            return LEVENSTEIN;
        }
        else if(cf instanceof DamerauDistanceCostFunction)
        {
            return DAMERAU;
        }
        else if(cf instanceof SimpleErrorFunction simpleErrorFunction)
        {
            return buildErrorCost(simpleErrorFunction);
        }
        else if(cf instanceof PreferenceCostFunction preferenceCostFunction)
        {
            return buildPreferenceCost(preferenceCostFunction);
        }
        
        throw new RuntimeException("Cannot convert cost function to string: unsupported cost function.");

    }
    
    public static Map<String, CostFunction<?>> parseCostFunctions(List<String> lines, Map<String, SigmaContractor<?>> contractorMap, boolean padWithConstantFunctions) throws ParseException, InterpretationException
    {
        Map<String, CostFunction<?>> costFunctionMap = new HashMap<>();
        
        for(String line: lines)
        {
            if(!line.trim().startsWith(CONTRACT_PREFIX) || !line.contains(CONTRACT_SEPARATOR))
                throw new ParseException("Cost functions should be specified in the following format: "
                    + CONTRACT_PREFIX 
                    + "<ATTRIBUTE_NAME>"
                    + CONTRACT_SEPARATOR
                    + "<COST_FUNCTION_TYPE> <COST_FUNCTION_PARAMETERS> where <COST_FUNCTION_TYPE> is one of "
                    + TYPES.stream().collect(Collectors.joining(","))
                );
        
            //Parse the attribute name
            String attribute = line
                .substring(1, line.trim().indexOf(CONTRACT_SEPARATOR))
                .trim();

            if(contractorMap.get(attribute) == null)
                throw new ParseException("Cannot parse cost function for attribute '"
                    + attribute
                    + "'. "
                    + "Cause: no contractor specified in section "
                    + RubixFileConstants.SIGMA_SECTION
                    + " for this attribute."
                );
            
            //Parse the cost function definition
            String costFunctionDefinition = line
                .substring(line.indexOf(":") + 1)
                .trim();
            
            costFunctionMap
                .put(
                    attribute,
                    parseCostFunction(
                        costFunctionDefinition,
                        (SigmaContractor) contractorMap.get(attribute)
                    ));
        }
        
        if(padWithConstantFunctions)
        {
            for(String attribute: contractorMap.keySet())
            {
                //If an attribute has no cost function yet,
                //assign it a constant cost function with cost 1
                costFunctionMap.merge(
                    attribute,
                    new ConstantCostFunction(1),
                    (cf, ccf) -> cf);
            }
        }
        
        return costFunctionMap;
    }
    
    public static <T extends Comparable<? super T>> CostFunction parseCostFunction(String s, SigmaContractor<T> contractor) throws InterpretationException
    {
        //Case of a constant cost function
        String cPattern = CONSTANT + "\\s*\\(\\s*(\\d+)\\s*\\)";
        s = s.trim();
        if(s.matches(cPattern))
        {
            int cost = new Grex("@1")
                .resolveAsInteger(cPattern, s)
                .intValue();
            
            return new ConstantCostFunction<>(cost);
        }
        
        //Case of a distance-based cost function
        if(s.matches(DISTANCE))
        {
            return new DistanceCostFunction<>((OrdinalContractor<T>) contractor);
        }
        
        if(s.matches(LEVENSTEIN))
        {
            return new LevenshteinDistanceCostFunction();
        }
        
        if(s.matches(DAMERAU))
        {
            return new DamerauDistanceCostFunction();
        }
        
        //Case of a preference-based cost function
        String pPattern = PREFERENCE + "(\\!?)" + "\\s*\\((.+)\\)";
        
        if(s.matches(pPattern))
        {
            List<T> preferenceOrder = Stream.of(
                    new Grex("@2")
                        .resolveAsString(pPattern, s)
                        .split(",")
                )
                .map(v -> v.trim())
                .map(v -> !v.startsWith("'") ? "'" + v : v)
                .map(v -> !v.endsWith("'") ? v + "'" : v)
                .map(v -> contractor.parseConstant(v))
                .collect(Collectors.toList());
            
            //Check strong/weak bias
            boolean weakBias = !new Grex("@1 ! =").resolveAsBoolean(pPattern, s);
            
            return new PreferenceCostFunction<>(preferenceOrder, weakBias);
        }
        
        String ePattern = ERROR + "\\s*\\(((\\d+)\\s*,)?\\s*\\[(.+)\\]\\)";
        
        if(s.matches(ePattern))
        {
            String uexc = new Grex("@2").resolveAsString(ePattern, s);
            
            int unexplained = uexc == null || uexc.trim().isEmpty()
                ? 2
                : Integer.parseInt(uexc);
            
            List<String> errorMechNames = 
                Stream.of(new Grex("@3")
                    .resolveAsString(ePattern, s)
                    .split(","))
                    .map(v -> v.trim())
                    .collect(Collectors.toList());
            
            List<ErrorMechanism<T>> errorMechs = new ArrayList<>();
            
            Map<String, ErrorMechanism<?>> knownErrorMechs = collectErrorMechanisms();
            
            for(String errorMechName: errorMechNames)
            {
                if(!knownErrorMechs.containsKey(errorMechName))
                    throw new RuntimeException("Cannot parse error mechanism. Cause: unknown error mechanism '" + errorMechName + "'");
                else
                    errorMechs.add((ErrorMechanism<T>) knownErrorMechs.get(errorMechName));
            }
            
            return new SimpleErrorFunction<>(unexplained, errorMechs);
        }
        
        
        throw new RuntimeException("Cannot parse cost function from string: unknown cost function '" + s + "'");
    }
    
    private static String buildConstantCost(ConstantCostFunction cf)
    {
        return CONSTANT + "(" + cf.getFixedCost() + ")";
    }
    
    private static String buildErrorCost(SimpleErrorFunction<?> cf)
    {
        List<String> emStrings = new ArrayList<>();
        
        for(ErrorMechanism<?> em : cf.getErrorMechanisms())
        {
            String emString = buildErrorMechanism(em);
            
            if(emString == null)
                throw new RuntimeException("Cannot convert error cost function to string: unsupported error mechanism.");
            
            emStrings.add(emString);
            
        }
        
        return ERROR
            + "("
            + cf.getNonExplainedCost()
            + ","
            + emStrings
                .stream()
                .collect(Collectors.joining(",", "[", "]"))
            + ")";    
    }
    
    private static String buildPreferenceCost(PreferenceCostFunction cf)
    {
        return PREFERENCE
            + (cf.isWeakBias() ? "" : "!")
            + cf
            .getPreferenceOrder()
            .stream()
            .collect(Collectors.joining(",","(",")"));
    }
    
    private static String buildErrorMechanism(ErrorMechanism e)
    {
        for(DatetimeErrors dte: DatetimeErrors.values())
        {
            if(dte.getEmbeddedMechanism().equals(e))
            {
                return Config.COST_FUNCTION_SHORT_NAMES
                    ? dte.getShortName()
                    : dte.getName();
            }
        }
        
        for(IntegerErrors ie: IntegerErrors.values())
        {
            if(ie.getEmbeddedMechanism().equals(e))
            {
                return Config.COST_FUNCTION_SHORT_NAMES
                    ? ie.getShortName()
                    : ie.getName();
            }
        }
        
        for(DecimalErrors de: DecimalErrors.values())
        {
            if(de.getEmbeddedMechanism().equals(e))
            {
                return Config.COST_FUNCTION_SHORT_NAMES
                    ? de.getShortName()
                    : de.getName();
            }
        }
        
        return null;
    }
    
    private static Map<String, ErrorMechanism<?>> collectErrorMechanisms()
    {
        Map<String, ErrorMechanism<?>> errorMap = new HashMap<>();
        
        for(DatetimeErrors dte: DatetimeErrors.values())
        {
            add(errorMap, dte.getName(), dte.getEmbeddedMechanism());
            add(errorMap, dte.getShortName(), dte.getEmbeddedMechanism());
        }
        
        for(IntegerErrors ie: IntegerErrors.values())
        {
            add(errorMap, ie.getName(), ie.getEmbeddedMechanism());
            add(errorMap, ie.getShortName(), ie.getEmbeddedMechanism());
        }
        
        for(DecimalErrors de: DecimalErrors.values())
        {
            add(errorMap, de.getName(), de.getEmbeddedMechanism());
            add(errorMap, de.getShortName(), de.getEmbeddedMechanism());
        }
        
        return errorMap;
    }

    private static void add(Map<String, ErrorMechanism<?>> errorMap, String n, ErrorMechanism<?> err)
    {
        if(errorMap.containsKey(n))
            throw new RuntimeException("Internal inconsistency with error mechanisms detected. Name '" + n + "' is not unique.");
        
        errorMap.put(n, err);
    }
}
