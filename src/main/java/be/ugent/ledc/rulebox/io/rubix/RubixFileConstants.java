package be.ugent.ledc.rulebox.io.rubix;

public class RubixFileConstants
{
    public static final String COMMENT_PREFIX           = "--";
    
    //Constraint related
    public static final String CONTRACT                 = "#datatypes";
    public static final String SIGMA_SECTION            = "#selection-rules";
    public static final String PROPERTY_SECTION         = "#property-assertions";
    public static final String FD_SECTION               = "#functional-dependencies";
    public static final String IND_SECTION              = "#inclusion-dependencies";
    public static final String TRANSITION_SECTION       = "#transition-rules";
    public static final String LINKAGE_SECTION          = "#linkage-rules";
    
    
    //Repair related
    public static final String COST_MODEL_SECTION       = "#repair-cost-models";
}
