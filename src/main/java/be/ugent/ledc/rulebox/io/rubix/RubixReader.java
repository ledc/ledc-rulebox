package be.ugent.ledc.rulebox.io.rubix;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.io.FDParser;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.indy.io.INDParser;
import be.ugent.ledc.match.matcher.dataobject.DataObjectMatcher;
import be.ugent.ledc.match.matcher.io.LinkageParser;
import be.ugent.ledc.pi.assertion.AbstractAssertor;
import be.ugent.ledc.pi.assertion.AssertionParser;
import be.ugent.ledc.pi.assertion.PropertyAssertions;
import be.ugent.ledc.rulebox.Rubix;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class RubixReader
{    
    private static final Set<String> SECTIONS =
        SetOperations.set(
            RubixFileConstants.CONTRACT,
            RubixFileConstants.SIGMA_SECTION,
            RubixFileConstants.FD_SECTION,
            RubixFileConstants.PROPERTY_SECTION,
            RubixFileConstants.COST_MODEL_SECTION,
            RubixFileConstants.IND_SECTION,
            RubixFileConstants.LINKAGE_SECTION
        );
    
    public static Rubix readRubix(File rubixFile) throws FileNotFoundException, LedcException
    {
        return readRubix(rubixFile, StandardCharsets.UTF_8);
    }
    
    public static Rubix readRubix(File rubixFile, Charset charset) throws FileNotFoundException, LedcException
    {
        Map<String, List<String>> sectionMap = parse(rubixFile, charset);
        
        //Parse contractos
        Map<String, SigmaContractor<?>> contractors = SigmaRuleParser.parseContractors(
            sectionMap.get(RubixFileConstants.CONTRACT)
        );
        
        //Validate: each contractor that contains a separator, must start with an existing binder
        for(String a: contractors.keySet())
        {
            if(!Rubix.valid(a))
                throw new ParseException("Invalid attribute name in section "
                + RubixFileConstants.CONTRACT
                + ": " + a + ". Prefix is not an existing binder.");
        }
        
        //Parse sigma rules
        SigmaRuleset sigmaRules = SigmaRuleParser.parseSigmaRuleSet(
            sectionMap.get(RubixFileConstants.SIGMA_SECTION),
            contractors
        );
        
        //Parse cost functions
        Map<String, CostFunction<?>> sigmaCostFunctions = CostFunctionParser
            .parseCostFunctions(
                sectionMap.get(RubixFileConstants.COST_MODEL_SECTION),
                sigmaRules.getContractors(),
                true);
        
        //Parse functional dependencies
        RuleSet<Dataset,FD> fdRules = FDParser.parseRuleset(
            sectionMap.get(RubixFileConstants.FD_SECTION)
        );
        
        //Validate
        for(FD fd: fdRules)
        {
            Rubix.validate(fd);
        }

        //Parse property assertions
        PropertyAssertions assertions = AssertionParser.parse(
            sectionMap.get(RubixFileConstants.PROPERTY_SECTION)
        );
        
        //Validate
        for(AbstractAssertor aa: assertions.getRules())
        {
            Rubix.validate(aa);
        }
        
        //Parse inclusion dependencies
        INDRuleset inds = INDParser.parseInclusionDependencies(
            sectionMap.get(RubixFileConstants.IND_SECTION),
            contractors);
        
        //Validate
        for(IND ind: inds)
        {
            Rubix.validate(ind);
        }
        
        DataObjectMatcher matcher = LinkageParser.parseObjectMatcher(sectionMap.get(RubixFileConstants.LINKAGE_SECTION));

        
        return new Rubix.RubixBuilder()
            .withSigmaRules(sigmaRules)
            .withFunctionalDependencies(fdRules)
            .withPropertyAssertions(assertions)
            .withInclusionDependencies(inds)
            .withSigmaCostFunctions(sigmaCostFunctions)
            .withMatcher(matcher)
            .build();
        
    }
    
    private static Map<String, List<String>> parse(File rbxFile, Charset charset) throws ParseException, FileNotFoundException
    {
        Scanner scanner = new Scanner(rbxFile, charset.name());
        
        String activeSection = null;
        
        //Initialize a mapping from section names to lines found in that section
        Map<String, List<String>> sectionMap = SECTIONS
            .stream()
            .collect(Collectors.toMap(
                sec -> sec,
                sec -> new ArrayList<String>())
            );
        
        while (scanner.hasNextLine())
        {
            //Read the line, ignore leading and trailing spaces
            String line = scanner.nextLine().trim();

            //Comments are ignored
            if (line.startsWith(RubixFileConstants.COMMENT_PREFIX) || line.isEmpty())
                continue;
            
            if(SECTIONS.stream().anyMatch(s -> line.equals(s)))
            {
                //Swith to another section
                activeSection = line;
            }
            else
            {
                if(activeSection == null)
                    throw new ParseException("Could not parse file "
                        + rbxFile.getName()
                        + ". No active section specified.");
                else
                    sectionMap
                        .get(activeSection)
                        .add(line);
            }
        }

        return sectionMap;
    }
}
