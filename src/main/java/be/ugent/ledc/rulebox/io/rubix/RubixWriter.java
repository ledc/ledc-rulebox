package be.ugent.ledc.rulebox.io.rubix;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.match.matcher.dataobject.DataObjectMatcher;
import be.ugent.ledc.match.matcher.dataobject.FellegiSunterMatcher;
import be.ugent.ledc.match.matcher.dataobject.MinimaxMatcher;
import be.ugent.ledc.match.matcher.dataobject.belief.ConditionalBelief;
import be.ugent.ledc.match.matcher.io.LinkageParser;
import be.ugent.ledc.rulebox.Rubix;
import static be.ugent.ledc.sigma.io.ConstraintIo.CONTRACT_PREFIX;
import static be.ugent.ledc.sigma.io.ConstraintIo.CONTRACT_SEPARATOR;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RubixWriter
{
    public static void writeRubix(Rubix rubix, File rubixFile) throws FileNotFoundException, LedcException
    {
        writeRubix(rubix, rubixFile, StandardCharsets.UTF_8);
    }
    
    public static void writeRubix(Rubix rubix, File rubixFile, Charset charset) throws FileNotFoundException, LedcException
    {
        try(
            PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(
                        new FileOutputStream(rubixFile),
                        charset))
        )
        {
            writer.println(RubixFileConstants.CONTRACT);
            
            //Write the contracts in format @<attribute>:<contractname>
            Stream.concat(
                rubix
                    .getAllSigmaRules()
                    .getContractors()
                    .entrySet()
                    .stream(),
                rubix
                    .getAllInclusionDependencies()
                    .stream()
                    .flatMap(ind -> ind
                        .getCondition()
                        .getAttributes()
                        .stream()
                        .collect(Collectors.toMap(
                            a -> a,
                            a -> ind.getContractor(a)
                        ))
                        .entrySet()
                        .stream()
                    )
            )
            .map(e -> CONTRACT_PREFIX
                .concat(e.getKey())
                .concat(CONTRACT_SEPARATOR)
                .concat(e.getValue().name()))
            .forEach(writer::println);

            if(!rubix.getAllSigmaRules().getRules().isEmpty())
            {
                //Write the sigma rules
                writer.println("");
                writer.println(RubixFileConstants.SIGMA_SECTION);
            
                writer.println("");
                writer.println(RubixFileConstants.COMMENT_PREFIX + "Domain constraints");
                writer.println("");
                rubix
                .getAllSigmaRules()
                .getRules()
                .stream()
                .filter(rule -> rule.getInvolvedAttributes().size() == 1)
                .map(rule -> rule.toString())
                .forEach(writer::println);

                writer.println("");
                writer.println(RubixFileConstants.COMMENT_PREFIX + "Interaction constraints");
                writer.println("");

                rubix
                .getAllSigmaRules()
                .getRules()
                .stream()
                .filter(rule -> rule.getInvolvedAttributes().size() > 1)
                .map(rule -> rule.toString())
                .forEach(writer::println);
            }
            
            if(!rubix.getAllInclusionDependencies().isEmpty())
            {
                //Write the sigma rules
                writer.println(RubixFileConstants.IND_SECTION);
           
                rubix
                .getAllInclusionDependencies()
                .stream()
                .map(rule -> rule.toString())
                .forEach(writer::println);
                    
            }
            
            //Write the cost functions that are not constant or have a constant cost differen from one
            if(rubix
                .getCostFunctions()
                .values()
                .stream()
                .anyMatch(cf -> !(cf instanceof ConstantCostFunction)
                        || ((ConstantCostFunction)cf).getFixedCost() != 1))
            {
                writer.println("");
                writer.println(RubixFileConstants.COST_MODEL_SECTION);
                writer.println("");

                rubix
                .getCostFunctions()
                .entrySet()
                .stream()
                .filter(e -> !(e.getValue() instanceof ConstantCostFunction)
                    || ((ConstantCostFunction)e.getValue()).getFixedCost() != 1)
                .map(e -> CONTRACT_PREFIX
                    .concat(e.getKey())
                    .concat(CONTRACT_SEPARATOR)
                    .concat(CostFunctionParser.build(e.getValue()))
                )
                .forEach(writer::println);
            }
            
            if(!rubix.getAllPropertyAssertions().getRules().isEmpty())
            {
                writer.println("");
                writer.println(RubixFileConstants.PROPERTY_SECTION);
                writer.println("");

                rubix
                .getAllPropertyAssertions()
                .getRules()
                .stream()
                .map(ass -> ass.toString())
                .forEach(writer::println);
            }
            
            if(!rubix.getAllFunctionalDependencies().isEmpty())
            {
                writer.println("");
                writer.println(RubixFileConstants.FD_SECTION);
                writer.println("");

                rubix
                .getAllFunctionalDependencies()
                .stream()
                .map(fd -> fd.toString())
                .forEach(writer::println);
            }
            
            if(rubix.getMatcher() != null)
            {
                writer.println("");
                writer.println(RubixFileConstants.LINKAGE_SECTION);
                writer.println("");
                
                DataObjectMatcher<?, ?> matcher = rubix.getMatcher();
                
                for(String a: matcher.getMatchAttributes())
                {
                    writer.println(
                        CONTRACT_PREFIX
                            .concat(a)
                            .concat(CONTRACT_SEPARATOR)
                            .concat(matcher.getAttributeMatcher(a).toString())
                    );
                }
                
                if(matcher instanceof FellegiSunterMatcher fs)
                {
                    writer.println("");
                    
                    //Write prior
                    writer.println(LinkageParser.PRIOR_WEIGHT
                        .concat(LinkageParser.SEPARATOR)
                        .concat(" " + fs.getPrior()));
                    
                    writer.println("");
                    
                    //Write partial match weights
                    for(String a: matcher.getMatchAttributes())
                    {
                        writer.println(
                            LinkageParser.PARTIAL_MATCH_WEIGHT
                            .concat(LinkageParser.SEPARATOR)
                            .concat(" ")
                            .concat(LinkageParser.ATTRIBUTE_PREFIX)
                            .concat(a)
                            .concat(fs.getBelief(a).toString())
                        );
                    }
                }
                else if(matcher instanceof MinimaxMatcher mm)
                {
                    ConditionalBelief condBelief = mm.getConditionalBelief();
                    
                    for(Set<String> set: condBelief.believableSets())
                    {
                        System.out.println(
                            LinkageParser.MINIMAX_RULE
                            + " "
                            + set.stream().collect(Collectors.joining(","))
                            + LinkageParser.SEPARATOR
                            + " "
                            + condBelief.getConditionalBelief(set)
                        );
                    }
                }
            }
            
            writer.flush();
        }
    }
}
