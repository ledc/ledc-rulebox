package be.ugent.ledc.rulebox.test;

import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.SimpleErrorFunction;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.pi.grex.InterpretationException;
import be.ugent.ledc.rulebox.io.rubix.CostFunctionParser;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.repair.cost.functions.DistanceCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.PreferenceCostFunction;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author abronsel
 */
public class CostFunctionParserTest
{

    @Test
    public void constantCostParseTest() throws InterpretationException
    {
        CostFunction<Integer> cf = CostFunctionParser
            .parseCostFunction(
                "constant ( 2 )",
                SigmaContractorFactory.INTEGER);
        
        Assert.assertTrue(cf instanceof ConstantCostFunction);
        Assert.assertEquals(0, cf.cost(1, 1, new DataObject()));
        Assert.assertEquals(2, cf.cost(1, 3, new DataObject()));
        Assert.assertEquals(2, cf.cost(null, 3, new DataObject()));
        Assert.assertEquals(Integer.MAX_VALUE, cf.cost(1, null, new DataObject()));
    }
    
    @Test
    public void distanceCostParseTest() throws InterpretationException
    {
        CostFunction<Integer> cf = CostFunctionParser
            .parseCostFunction(
                "distance ",
                SigmaContractorFactory.INTEGER);
        
        Assert.assertTrue(cf instanceof DistanceCostFunction);
        Assert.assertEquals(0, cf.cost(1, 1, new DataObject()));
        Assert.assertEquals(4, cf.cost(1, 5, new DataObject()));
        Assert.assertEquals(1, cf.cost(2, 1, new DataObject()));
        Assert.assertEquals(1, cf.cost(null, 3, new DataObject()));
        Assert.assertEquals(Integer.MAX_VALUE, cf.cost(1, null, new DataObject()));
    }
    
    @Test
    public void preferenceCostParseTest() throws InterpretationException
    {
        CostFunction<String> cf = CostFunctionParser
            .parseCostFunction(
                "preference (A, C, E,X)",
                SigmaContractorFactory.STRING);
        
        Assert.assertTrue(cf instanceof PreferenceCostFunction);
        Assert.assertEquals(0, cf.cost("A", "A", new DataObject()));
        Assert.assertEquals(2, cf.cost("A", "C", new DataObject()));
        Assert.assertEquals(5, cf.cost("A", "T", new DataObject()));
        Assert.assertEquals(1, cf.cost("C", "A", new DataObject()));
        Assert.assertEquals(1, cf.cost("X", "C", new DataObject()));
        Assert.assertEquals(1, cf.cost(null, "A", new DataObject()));
        Assert.assertEquals(Integer.MAX_VALUE, cf.cost("A", null, new DataObject()));
    }
    
    @Test
    public void preferenceCostStrongBiasParseTest() throws InterpretationException
    {
        CostFunction<String> cf = CostFunctionParser
            .parseCostFunction(
                "preference! (A, C, E,X)",
                SigmaContractorFactory.STRING);
        
        Assert.assertTrue(cf instanceof PreferenceCostFunction);
        Assert.assertEquals(0, cf.cost("A", "A", new DataObject()));
        Assert.assertEquals(4, cf.cost("A", "X", new DataObject()));
        Assert.assertEquals(3, cf.cost("A", "E", new DataObject()));
        Assert.assertEquals(2, cf.cost("A", "C", new DataObject()));
        Assert.assertEquals(3, cf.cost("X", "E", new DataObject()));
        Assert.assertEquals(2, cf.cost("X", "C", new DataObject()));
        Assert.assertEquals(1, cf.cost(null, "A", new DataObject()));
        Assert.assertEquals(Integer.MAX_VALUE, cf.cost("A", null, new DataObject()));
    }
    
    @Test
    public void errorCostIntegerParseTest() throws InterpretationException
    {
        CostFunction<Integer> cf = CostFunctionParser
            .parseCostFunction(
                "error (3, [sd, at,p])",
                SigmaContractorFactory.INTEGER);
        
        Assert.assertTrue(cf instanceof SimpleErrorFunction);
        Assert.assertEquals(0, cf.cost(150, 150, new DataObject()));
        Assert.assertEquals(1, cf.cost(150, 151, new DataObject()));
        Assert.assertEquals(1, cf.cost(150, 180, new DataObject()));
        Assert.assertEquals(1, cf.cost(150, 105, new DataObject()));
        Assert.assertEquals(3, cf.cost(150, 185, new DataObject()));

        Assert.assertEquals(Integer.MAX_VALUE, cf.cost(156, null, new DataObject()));
    }
}
